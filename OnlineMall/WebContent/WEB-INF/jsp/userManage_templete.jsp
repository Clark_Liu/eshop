<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<tr style="background:#2A7AD2 ; height:30px; color:#FFF">
	<th width="3%">&nbsp;</th>
	<th width="20%">会员名</th>
	<th width="20%">手机号</th>
	<th width="15%">email</th>
	<th width="15%">性别</th>
	<th width="20%">权限</th>
	<th align="center">操作</th>
</tr>   
<c:forEach  items="${list }" var="tmp" varStatus="loop">

<tr>
    <td align="left"></td>
    <td align="left">
  		<span class="memimg" style="cursor: pointer;">
 	         <img src="${tmp.headPhoto }" width="45" height="49">
        </span> 
     	<span class="mem_detail">
             <ul>
	             <li class="mem_font">${tmp.username }<i>(真实姓名：${tmp.truename })</i></li>
	             <li class="addtime">注册时间：<fmt:formatDate type="both" dateStyle="medium" timeStyle="medium" value="${tmp.addtime }" /></li>
             </ul>
         </span>
     </td>
     <td align="center">${tmp.cellphone }</td>
     <td class="lightred">${tmp.email }</td>
     <td class="lightred">${tmp.sex }</td>
  	 <td class="blue" align="center">${tmp.role }</td>
 	 <td align="center" class="blue xiahua">
  		<a href="/dispatcher/dispatch?dispatch_url=userManageCheck.jsp?id=${tmp.id }">查看</a> 
 	 </td>
</tr> 
</c:forEach>
<input type="hidden" id="currentPage" value="${currentPage }"/>