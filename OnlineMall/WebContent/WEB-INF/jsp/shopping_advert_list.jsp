<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>广告管理</title>
<link href="/css/template.css" rel="stylesheet" type="text/css">
<script src="/js/jquery-1.6.2.js"></script>
<script src="/js/jquery.shop.common.js"></script>
<script src="/js/jquery.poshytip.min.js"></script>
<script>
	function show() {
		var name = $("#advername").val();
		$.ajax({
			url : '/advert/show',
			type : 'post',
			async : true,
			data : {
				'name' : name,
				'pagenum' : 1
			},
			success : function(data) {
				//alert(111);
				fenye();
				$("#tbody").html(data);
			}
		});
	}
	function gotoPage(page) {
		alert("分页ajax="+page);
		var name = $("#advername").val();
		$.ajax({
			url : '/advert/fenye',
			type : 'post',
			data : {
				'name' : name,
				"pagenum" : page
			},
			async : true,
			success : function(data) {
				fenye();
				var tbody = $("#tbody");
				tbody.html(data);
			}
		});
	}
	function fenye() {
		$.ajax({
			url : '/advert/fenye1',
			type : 'post',
			async : true,
			data : {},
			success : function(data) {
				var div = $("#queryCondition");
				div.html(data);
			}
		});
	}
	jQuery(document).ready(function() {
		show();
		tipStyle();
	});
	//网站logo file样式
	jQuery(function() {
		var textButton = "<input type='text' name='textfield' id='textfield1' class='size13' /><input type='button' name='button' id='button1' value='' class='filebtn' />"
		jQuery(textButton).insertBefore("#brandLogo");
		jQuery("#brandLogo").change(function() {
			jQuery("#textfield1").val(jQuery("#brandLogo").val());
		})
	});
	//ajax修改是否显示的状态
	function ajax_updatestate(id, display, obj) {
		alert("修改是否显示");
		var va = $(obj).attr("va");
		alert(va);
		$.ajax({
			type : "Post",
			url : "/advert/updateSep",
			data : {
				"id" : id,
				"display" : display,
				'flg':va
			},
			success : function(data) {
				alert("进入success"+"##########"+va);
				if (va=="false") {
					$(obj).attr("src", "/images/yes.png");
					$(obj).attr("va","true");
				} else {
					alert("进入false");
					$(obj).attr("src", "/images/no.png");
					$(obj).attr("va","false");
				}
			}
		});
	}

	//ajax修改可编辑框中的品牌名、排序					
	function ajax_update(id, fieldName, obj) {
		// 	debugger
		// 	alert("111");
		var bname = $(obj).val();
		var secondval = $(obj).attr('cls');
		if (bname != secondval) {
			$.ajax({
				type : "Post",
				url : "/adver/updateBrandNameSeq",
				data : {
					"id" : id,
					"fieldName" : fieldName,
					"value" : bname
				},
				success : function(data) {
					// 				debugger
					// 				alert("222");
					if (bname == "") {
						if (data == "brandnamenull") {
							alert("分类名称不能为空!");
							$(obj).attr("value", secondval);
						}
						if (data == "seqnull") {
							alert("排序不能为空!");
							$(obj).attr("value", secondval);
						}
					} else if (data == "repeat") {
						alert("分类名称不能重复!");
						$(obj).attr("value", secondval);
					} else if (data == "geshi") {
						alert("排序格式必须为数字!");
						$(obj).attr("value", secondval);
					}
				}
			});
		}
	}
</script>
<style id="poshytip-css-tip-skyblue" type="text/css">
div.tip-skyblue {
	visibility: hidden;
	position: absolute;
	top: 0;
	left: 0;
}

div.tip-skyblue table,div.tip-skyblue td {
	margin: 0;
	font-family: inherit;
	font-size: inherit;
	font-weight: inherit;
	font-style: inherit;
	font-variant: inherit;
}

div.tip-skyblue td.tip-bg-image span {
	display: block;
	font: 1px/1px sans-serif;
	height: 10px;
	width: 10px;
	overflow: hidden;
}

div.tip-skyblue td.tip-right {
	background-position: 100% 0;
}

div.tip-skyblue td.tip-bottom {
	background-position: 100% 100%;
}

div.tip-skyblue td.tip-left {
	background-position: 0 100%;
}

div.tip-skyblue div.tip-inner {
	background-position: -10px -10px;
}

div.tip-skyblue div.tip-arrow {
	visibility: hidden;
	position: absolute;
	overflow: hidden;
	font: 1px/1px sans-serif;
}
</style>
</head>
<body style="">
	<div class="cont">
		<h1 class="seth1">广告管理</h1>
		<div class="settab">
			<span class="tab-one"></span> <span class="tabs"> <a
				href="/dispatcher/dispatch?dispatch_url=/shopping_advert_list.jsp"
				class="this">管理</a> | <a
				href="/advert/shopping_advert_edit.htm?flg=add">新增</a>
			</span> <span class="tab-two"></span>
		</div>
		<form name="queryForm" id="queryForm" action="" method="post">
			<input type="hidden" name="currentpage" id="currentpage" value="1">
			<div class="allmem_search">
				<ul>
					<li><span>广告名称</span> <span class="allmen size4"> <input
							type="text" name="advername" id="advername" value="">
					</span> <span class="btn_search"> <input name="" type="button"
							value="搜索" onclick="show();" style="cursor: pointer;">
					</span></li>
				</ul>
			</div>
		</form>
		<form name="ListForm" id="ListForm"
			action="http://localhost:8080/adver/showAdver" method="post">
			<div class="brandtable">
				<table width="100%" border="0" cellspacing="0" cellpadding="0"
					class="brand_table">
					<tbody>
						<tr style="background: #2A7AD2; height: 30px; color: #FFF">
							<td width="60" align="center">排序</td>
							<td width="250">广告名称</td>
							<td width="183" align="center">广告 图片标识</td>
							<td width="150" align="center">广告链接</td>
							<td width="121" align="center">推荐</td>
							<td width="217" align="center">操作</td>
						</tr>
					<tbody id="tbody"></tbody>
					</tbody>
				</table>
			</div>
		</form>
		<div class="fenye" align="right" id="queryCondition">
		</div>
</body>
</html>