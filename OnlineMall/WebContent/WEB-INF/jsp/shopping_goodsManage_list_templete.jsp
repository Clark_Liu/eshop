<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>



        <tr style="background:#2A7AD2; height:30px; color:#FFF">
          <td width="1%">&nbsp;</td>
          <td width="15%">商品名称</td>
          <td width="10%" align="center">商品图片</td>
          <td width="8%" align="center">原价</td>
          <td width="8%" align="center">售价</td>
          <td width="10%" align="center">品牌</td>
          <td width="15%" align="center">分类名</td>
          <td width="6%" align="center">商品状态</td>
          <td width="6%" align="center">特别推荐</td>
          <td align="center">操作</td>
        </tr>
<c:forEach items="${list }" var="tmp">
  <tr>
		  <input type="hidden" name="ids" value="${tmp.id }"/>
          <td>
          	<a href="http://localhost:8080/goods/updateSmallimg?id=26"><img src="/images/picture.png" title="点击进入小图编辑页"></a>
          </td>
          <td>
          		<input type="text" id="${tmp.id }" name="${tmp.id }" value="${tmp.goodsName }" cls="${tmp.goodsName }" title="可编辑" onblur="ajax_updategoodsname('${tmp.id }',this)">
          </td>
          <td align="center">  
          		<img src="${tmp.goodsPicture }" width="55px" height="60px">	</td>
          <td align="center">¥ 
          		${tmp.goodsOriginalprice }</td>
          <td align="center">¥ 
          		${tmp.goodsCurrentprice }	</td>
          <td align="center">${tmp.brandname }	</td>
          <td align="center">${tmp.firstclassname } &gt; ${tmp.secendclassname } &gt; ${tmp.thirdclassname }</td>
          <c:if test="${tmp.goodsStatus == true }">
	          <td align="center">上架</td>
          </c:if>
          <c:if test="${tmp.goodsStatus == false }">
	          <td align="center">下架</td>
          </c:if>
          <c:if test="${tmp.goodsRecommend == true }">
	          <td align="center">
	          		
	          		 <img src="/images/yes.png" width="21" isRecommend="yes" height="23" onclick="ajax_updategoodsrecommend('${tmp.id }',this)" style="cursor:pointer;" title="可编辑">
	          </td>          
          </c:if>
          <c:if test="${tmp.goodsRecommend == false }">
	          <td align="center">
	          		
	          		 <img src="/images/no.png" width="21" isRecommend="no" height="23" onclick="ajax_updategoodsrecommend('${tmp.id }',this)" style="cursor:pointer;" title="可编辑">
	          </td>          
          </c:if>
          <td class="ac8" align="center">
	          <a href="/dispatcher/dispatch?dispatch_url=shopping_goodsEditor.jsp?id=${tmp.id }">编辑</a> |
	          <a href="javascript:void(0);" onclick="ajax_deletegoodsone(this);">删除</a>	
          </td>
        </tr>
</c:forEach>