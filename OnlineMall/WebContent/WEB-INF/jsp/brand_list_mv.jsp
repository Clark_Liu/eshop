<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:forEach items="${brandlist }" var="tmp">
	<tr>
          <td align="center"><input name="checkbox" type="checkbox" id="checkbox" value="${tmp.id }"></td>
          <td><span class="pxnum size5"> <input type="text" name="${tmp.sequence}" id="${tmp.sequence}" value="${tmp.sequence}" onchange="ajax_update(${tmp.id},'sequence',this)" title="可编辑"></span></td>
          <td><span class="pxnum size5"> <input type="text" name="${tmp.firstWord}" id="${tmp.firstWord}" value="${tmp.firstWord}" onchange="ajax_update(${tmp.id},'first_word',this)" title="可编辑"></span></td>
          <td><span class="pxnum size8"><input name="${tmp.name}" type="text" id="${tmp.name}" onchange="ajax_update(${tmp.id},'name',this)" value="${tmp.name}" title="可编辑">
          </span></td>
          <td><span class="pxnum size8">${tmp.brandCategory }</span></td>
          <td><img src="/upload/brand/${tmp.brandlogoImg }" width="88px" height="44px"></td>
          <c:if test="${tmp.recommend }">
          <td align="center"><img onclick="ajax_update(${tmp.id},'recommend',this)" src="/images/true.png" width="25" height="21" border="0" style="cursor:pointer;" title="可编辑" va="true"> </td>          
          </c:if>
          <c:if test="${!tmp.recommend }">
          <td align="center"><img onclick="ajax_update(${tmp.id},'recommend',this)" src="/images/false.png" width="25" height="21" border="0" style="cursor:pointer;" title="可编辑" va="false"> </td>
          </c:if>
          <td class="ac8" align="left"><a href="/brand/goods_brand_edit.htm?id=${tmp.id }&flg=eidt">编辑</a>|<a href="http:/brand/delete?mulitId=${tmp.id }&amp;currentPage=1">删除</a></td>
    </tr>
    </c:forEach>
    <tr>
        <td align="center"><input type="checkbox" name="all" id="all" value="" onclick="selectAll(this)"/></td>
        <td colspan="2" class="bdel">
		<span class="sp1">全部</span><span class="sp2">
          <div class="shop_btn_del shopbtn">
            <input name="input" type="button"  value="删除" onclick="deleteall()">          </div>
          </span>
		  </td>
        <td colspan="4"></td>
      </tr>