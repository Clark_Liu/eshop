<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>广告添加</title>
<link href="/css/template.css" rel="stylesheet" type="text/css">
<script src="/js/jquery-1.6.2.js"></script>
<script src="/js/jquery.validate.min.js"></script>

<script type="text/javascript">

$(document).ready(function(){
	var id = '<%=request.getParameter("id")%>';
	
	$.ajax({
		type:"post",
		async:true,
		url:"/user/findSimple",
		data:{
			"id":id
		},
		success:function(data){
			var us = JSON.parse(data).u;
			$("#username").html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+us.username);
			$("#truename").html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+us.truename);
			$("#headPhoto").attr("src",us.headPhoto);
			if(us.sex){
				$("#sex").html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;男");
			}else{
				$("#sex").html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;女");
			}
			$("#cellPhone").html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+us.cellphone);
			$("#email").html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+us.email);
			
		}
	})
});

</script>
</head>

<body style="">
<form action="" method="post" enctype="multipart/form-data" name="theForm" id="theForm">
  <input name="id" id="id" type="hidden" value="9">
  <div class="cont">
    <h1 class="seth1">会员管理</h1>
    <div class="settab"> 
	<span class="tab-one"></span> <span class="tabs">
         <a href="/dispatcher/dispatch?dispatch_url=userManage.jsp">管理</a> | 
		 <a href="javascript:void(0);" onclick="JavaScript:window.location.reload()" class="this">查看</a> 
	</span> 
	<span class="tab-two"></span>	</div>
    <div class="setcont" id="base">
      <!--鼠标经过样式-->
      <ul class="set1">
        <li><strong class="orange fontsize20">*</strong>用户名</li>
        <li>
        	<span class="webname" id="username"></span>
        </li>        
      </ul>
      
      <ul class="set1">
        <li><strong class="orange fontsize20">*</strong>真实姓名</li>
        <li>
        	<span class="webname" id="truename"></span>
        </li>        
      </ul>
      
      <ul class="set1">      
	      <li><strong class="orange fontsize20">*</strong>用户头像</li>
	      <li><span>
		        	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img id="headPhoto" src="" width="45" height="49">
			  </span>
		  </li>      
	      <li><strong class="orange fontsize20">*</strong>性别</li>
		  <li><span id="sex"></span></li>
      </ul>
    
      <ul class="set1">
        <li><strong class="orange fontsize20">*</strong>手机号</li>
        <li>
        	<span class="webname" id="cellPhone">
        	</span> 
        </li>
      </ul>    
      
      <ul class="set1">
        <li><strong class="orange fontsize20">*</strong>email</li>
        <li>
	        <span class="webname" id="email">
	        </span>
        </li>		 
      </ul>
    </div>
  </div>
</form>


</body></html>