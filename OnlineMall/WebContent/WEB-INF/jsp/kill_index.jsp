<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!-- saved from url=(0059)http://y14u504114.imwork.net:12111/shopping/admin/index.htm -->
<html xmlns="http://www.w3.org/1999/xhtml" style="overflow: hidden;"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>童臻时代 - Powered by shopping</title>
<meta name="keywords" content="shopping1,shopping2">
<meta name="description" content="shopping1,shopping2">
<meta name="generator" content="shopping 1.3">
<meta name="author" content="www.shopping.com">
<meta name="copyright" content="shopping Inc. All Rights Reserved">
<link href="/css/template.css" rel="stylesheet" type="text/css">
<script src="/js/jquery-1.6.2.js"></script>
<script src="/js/jquery.shop.base.js"></script>
<script type="text/javascript">
//文档加载前判断是否登录
	var isLogin = <%=request.getSession().getAttribute("isLogin")%>;
	console.log(isLogin);
	if(isLogin != true){
		alert("请您先登录!");
		window.location.href="/dispatcher/dispatch?dispatch_url=Login.jsp";
	}

</script>
<script>
$(document).ready(function(){
    pagestyle();
	$(".webmap a").click(function(){
	    $(".webmap_box").fadeIn('normal');
	 });
	$(".close_map").click(function(){
	    $(".webmap_box").fadeOut('normal');
	});
	$("a[id^=complex_]").click(function(){
	  var suffix=$(this).attr("suffix");
	  if($("#"+suffix+"info").css("display")=="block"){
	      $("#"+suffix+"info").hide();
		  $("#"+suffix+"img").attr("src","http://y14u504114.imwork.net:12111/shopping/resources/style/system/manage/blue/images/spread.jpg");
	   }else{
	      $("#"+suffix+"info").show();
		  $("#"+suffix+"img").attr("src","../images/contract.jpg");	  
	   }
	});
	$(".webskin em a img").click(function(){
	      var webcss = $(this).attr("webcss");
			$.post("http://y14u504114.imwork.net:12111/shopping/admin/set_websiteCss.htm",{
						"webcss":webcss
						},function(data){
						window.location.href="http://y14u504114.imwork.net:12111/shopping/admin/index.htm";	
							},"text");
		});
});	
</script>
</head>
<body scroll="yes" style="">
<div class="main">

  <div class="top">
    <div class="login">您好,${sessionScope.username },您登录的身份是：${sessionScope.role } &nbsp;&nbsp;| <a href="/user/exit" target="_self">安全退出</a>|<a href="http://localhost:8080/index/homePage" target="_blank">商城首页</a></div>
  	<div class="logo"></div>
  </div>
  
  <div class="index" id="workspace">
    <div class="left" style="width: 10%;">
      <div class="lefttop"> </div>
	  <div class="left_ul">
	  	<div class="leftone">常用操作</div>
		<ul class="ulleft" id="common_operation">		  
          <li><a class="this" id="welcome_op" href="javascript:void(0);" onclick="openURL('url','/dispatcher/dispatch?dispatch_url=welcome.html','main_workspace','welcome_op')">欢迎页面</a> </li>         
          <li><a href="javascript:void(0);" id="user_list_op_q" onclick="openURL('url','/dispatcher/dispatch?dispatch_url=userManage.jsp','main_workspace','user_list_op_q')">会员管理</a></li>
          <li><a href="javascript:void(0);" id="set_site_op_q" onclick="openURL('url','/dispatcher/dispatch?dispatch_url=category.jsp','main_workspace','set_site_op_q')">分类管理</a></li>
          <li><a href="javascript:void(0);" id="city_site_op_q" onclick="openURL('url','/dispatcher/dispatch?dispatch_url=cityManage.jsp','main_workspace','city_site_op_q')" class="">城市管理</a></li>                            
          <li><a href="javascript:void(0);" id="brand_site_op_q" onclick="openURL('url','/dispatcher/dispatch?dispatch_url=\/goods_brand_list.jsp','main_workspace','brand_site_op_q')">品牌管理</a></li>
          <li><a href="javascript:void(0);" id="goods_type_op" onclick="openURL('url','/dispatcher/dispatch?dispatch_url=goods_type_list.jsp','main_workspace','goods_type_op')">类型管理</a></li>
          <li><a href="javascript:void(0);" id="goods_spec_op" onclick="openURL('url','/dispatcher/dispatch?dispatch_url=goods_spec_list.jsp','main_workspace','goods_spec_op')">规格管理</a></li>
	      <li><a href="javascript:void(0);" id="advert_set_op" onclick="openURL('url','/dispatcher/dispatch?dispatch_url=shopping_advert_list.jsp','main_workspace','advert_set_op')">广告管理</a></li>
          <li><a href="javascript:void(0);" id="goods_floor_op" onclick="openURL('url','/dispatcher/dispatch?dispatch_url=shopping_floor_list.jsp','main_workspace','goods_floor_op')">首页楼层</a></li>
          <li><a href="javascript:void(0);" id="spec_site_op_q" onclick="openURL('url','/dispatcher/dispatch?dispatch_url=goods_spec_list.html','main_workspace','spec_site_op_q')">规格管理</a></li>                                                                                      
          <li><a href="javascript:void(0);" id="goods_manage_op_q" onclick="openURL('url','/dispatcher/dispatch?dispatch_url=shopping_goodsManage.jsp','main_workspace','goods_manage_op_q')">商品管理</a></li>
          <li><a href="javascript:void(0);" id="floor_manage_op_q" onclick="openURL('url','/floor/showFloor','main_workspace','floor_manage_op_q')">楼层管理</a></li>        
          <li><a href="javascript:void(0);" id="order_list_op_q" onclick="openURL('url','/dispatcher/dispatch?dispatch_url=shopping_orderManage.jsp','main_workspace','order_list_op_q')">订单管理</a></li>        
        </ul>                         
	  </div>
    </div>
    <div class="content" style="width: 90%; height: 758px;">
	    <div class="navbar"><span class="webmap"><a href="javascript:void(0);">后台管理</a></span>
	      <div style="line-height:36px;">您的位置：控制台&gt;<span id="top_nav_info">会员管理</span></div>
	    </div>
      	<iframe id="main_workspace" name="main_workspace" src="/dispatcher/dispatch?dispatch_url=welcome.html" style="overflow: auto; height: 656px; width: 1077px;" frameborder="0" width="100%" height="100%" scrolling="yes" onload="window.parent"></iframe>
    </div>
  </div>
  
</div>

</body></html>