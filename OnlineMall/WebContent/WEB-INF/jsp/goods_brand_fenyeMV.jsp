<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
	   <a href="javascript:void(0);" onclick="gotoPage(1)">首页</a>
	   <c:if test="${currentPage!=1 }">
	   <a href="javascript:void(0);" onclick="gotoPage(${currentPage-1 })">上一页</a>
	   </c:if>
	    第<c:forEach  var="i" begin="1" end="${pagenum1 }">
	    <c:if test="${currentPage==i }">
	   <a class="this" href="javascript:void(0);" onclick="gotoPage(${i })">${i }</a>
	   </c:if>
	   	<c:if test="${currentPage!=i }">
	   <a  href="javascript:void(0);" onclick="gotoPage(${i })">${i }</a>
	   </c:if>
	   </c:forEach> 页
		<c:if test="${currentPage != pagenum1 }">
	   <a href="javascript:void(0);" onclick="gotoPage(${currentPage+1 })">下一页</a>
	   </c:if>
	   <a href="javascript:void(0);" onclick="gotoPage(${pagenum1 })">末页</a> 	  
       