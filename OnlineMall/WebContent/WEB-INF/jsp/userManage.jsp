<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE >
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>会员管理</title>
<style type="text/css">th{text-align:center}</style>
<link href="/css/template.css" rel="stylesheet" type="text/css">
<script src="/js/jquery-1.6.2.js"></script>
<script src="/js/jquery.shop.common.js"></script>
<script src="/js/jquery.poshytip.min.js"></script>
<script language="javascript" type="text/javascript" src="/js/WdatePicker.js"></script>
<link href="/css/WdatePicker.css" rel="stylesheet" type="text/css">

<script type="text/javascript">
$(document).ready(function(){
  $(".memimg").mouseover(function(){
     $(this).css("cursor","pointer");
	 $(".mem_detail[id^=user_detail_]").hide();
     var id=$(this).attr("id");
	 $("#user_detail_"+id).show();
  });
  $(".mem_detail[id^=user_detail_]").mouseleave(function(){
    $(this).hide();
  });
  
  
  
  
  
  if(<%=request.getParameter("currentPage")%> != null){
		currentPage = "<%=request.getParameter("currentPage")%>";
	}else{
		currentPage = 1;
	}
  $.ajax({
	  type:"post",
	  url:"/user/findAll",
	  async:true,
	  data:{
		  "currentPage":currentPage
	  },
	  success:function(data){
		  $("tbody").html(data);
			$.ajax({
				url:'/SkipPageUtils/skipPage',
				type:'post',
				async:true,
				data:{
					"currentPage":currentPage,
					"url":"userManage.jsp"
				},
				success:function(data){
					$("div .fenye").html(data);
				}
			}) 
	  }
  });
  
  
  
  
  
});
	
//清空查询框内的内容	
	function cleartext(){
	window.location.href="/dispatcher/dispatch/userManage.jsp";
	
	
	 /* 	var input = $('form#searchform').find('input');
	 	for(var i=0;i<input.length;i++){
	 	   	if(input[i].type == 'text'){
	 	   		input[i].value = '';	
	 	   	} 		
 		} */
	}
	
    
    function check(){
    	var username = $("#username").val();
    	$("#usernameHidden").val(username);
    	var tel = $("#tel").val();
    	$("#telHidden").val(tel);
    	var begintime = $("#begintime").val();
    	$("#begintimeHidden").val(begintime);
    	var endtime = $("#endtime").val();
    	$("#endtimeHidden").val(endtime);
    	$.ajax({
    		type:"post",
    		async:true,
    		url:"/user/search",
    		data:{
    			"username":username,
    			"tel":tel,
    			"begintime":begintime,
    			"endtime":endtime,
    			"currentPage":"1",
    		},
    		success:function(data){
    			 $("tbody").html(data);
    			 currentPage = $("#currentPage").val();
    			 $.ajax({
    					url:'/SkipPageUtils/skipPageSearch',
    					type:'post',
    					async:true,
    					data:{
    						"currentPage":currentPage,
    					},
    					success:function(data){
    						$("div .fenye").html(data);
    					}
    				}) 
    		}
    	})
    }
    
    
    
    function skipPage(currentPage){
    	var username = $("#usernameHidden").val();
    	var tel = $("#telHidden").val();
    	var begintime = $("#begintimeHidden").val();
    	var endtime = $("#endtimeHidden").val();
    	$.ajax({
    		type:"post",
    		async:true,
    		url:"/user/search",
    		data:{
    			"username":username,
    			"tel":tel,
    			"begintime":begintime,
    			"endtime":endtime,
    			"currentPage":currentPage,
    		},
    		success:function(data){
    			 $("tbody").html(data);
    			 currentPage = $("#currentPage").val();
    			 $.ajax({
    					url:'/SkipPageUtils/skipPageSearch',
    					type:'post',
    					async:true,
    					data:{
    						"currentPage":currentPage,
    					},
    					success:function(data){
    						$("div .fenye").html(data);
    					}
    				}) 
    		}
    	})
    }
    

</script>
<style id="poshytip-css-tip-skyblue" type="text/css">div.tip-skyblue{visibility:hidden;position:absolute;top:0;left:0;}div.tip-skyblue table, div.tip-skyblue td{margin:0;font-family:inherit;font-size:inherit;font-weight:inherit;font-style:inherit;font-variant:inherit;}div.tip-skyblue td.tip-bg-image span{display:block;font:1px/1px sans-serif;height:10px;width:10px;overflow:hidden;}div.tip-skyblue td.tip-right{background-position:100% 0;}div.tip-skyblue td.tip-bottom{background-position:100% 100%;}div.tip-skyblue td.tip-left{background-position:0 100%;}div.tip-skyblue div.tip-inner{background-position:-10px -10px;}div.tip-skyblue div.tip-arrow{visibility:hidden;position:absolute;overflow:hidden;font:1px/1px sans-serif;}</style></head>

<body style="">
  <div class="cont">
    <h1 class="seth1">查询会员信息</h1>
    <div class="settab"><span class="tab-one"></span> 
    	<span class="tabs">  <a href="javascript:void(0);" onclick="JavaScript:window.location.reload()" class="this">管理</a> </span>
    </div>
    <div class="allmem_search">
	<form action="/user/search" method="post" id="searchform">
      <ul>
        <li> <span class="allmen size4">
	        	会员名
	        	<input type="text" name="username" id="username" value="">	        	
	        	<input type="hidden" name="usernameHidden" id="usernameHidden" value="">	        	
       		</span>
       	
        &nbsp;
       	 <span class="allmen size4">
	        	手机号
	        	<input type="text" name="tel" id="tel" value="">	        	
	        	<input type="hidden" name="telHidden" id="telHidden" value="">	        	
       		</span>
       	
       	 &nbsp;
	   <span class="allmen size4">
				时&nbsp;&nbsp;&nbsp;间
				<input type="text" id="begintime" name="begintime" value="" class="Wdate" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'endtime\',{d:-1})}'})">
				<input type="hidden" id="begintimeHidden" name="begintimeHidden" value="" />
				至
				<input type="text" id="endtime" name="endtime" value="" class="Wdate" onclick="WdatePicker({minDate:'#F{$dp.$D(\'begintime\',{d:1})}'})">		
				<input type="hidden" id="endtimeHidden" name="endtimeHidden" value=""/>		
			</span>		
		
       	
<!--           <span class="btn_search"> -->
          &nbsp;&nbsp;			  <input id="button" type="button" onclick="check()" value="查询"  title="点击查询">
          <!-- &nbsp;&nbsp;&nbsp;&nbsp;<input type="button" value="清空" onclick="cleartext();"> -->
          &nbsp;&nbsp;&nbsp;&nbsp;<input type="button" value="清空" onclick="JavaScript:window.location.href='/dispatcher/dispatch?dispatch_url=userManage.jsp'">
<!--           </span>  -->
        </li>
      </ul>
	</form>
	
    </div>
    <div class="operation">
      <h3 align="center">会员信息</h3>
    </div>
	<form name="ListForm" id="ListForm" action="http://localhost:8080/user/userManage" method="post">
		<input type="hidden" id="">
    <div class="allmem_table">
      <table width="98%" border="0" cellspacing="0" cellpadding="0">
        <tbody>
				       
			
		

   	   </tbody>
      </table>
    </div>
    </form>
	<div class="fenye">
	</div>
     <li> &nbsp;</li>      	
</div>


</body>
</html>