<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="/css/template.css" rel="stylesheet" type="text/css">
<script src="/js/jquery-1.6.2.js"></script>
<script src="/js/jquery.shop.common.js"></script>
<script src="/js/jquery.poshytip.min.js"></script>
<script type="text/javascript">


$(function(){
	//生成表内容
	skipPage(1);
	
	
	//生成品牌的下拉列表
	$.ajax({
		url:"/brand/findAllSelected",
		type:"post",
		async:false,
		data:{},
		success:function(data){
			var list = JSON.parse(data).list;
			for(var i in list){
				var ipt = $("<option value="+list[i].id+">"+list[i].name+"</option>");
				$("#brandId").append(ipt);
			}
		}
	});
	
	//分类下拉列表
	$.ajax({
		url:'/goodsclass/findSelectClass',
		type:'post',
		async:true,
		data:{},
		success:function(data){
			var sel = $("#sortId");
			var list = JSON.parse(data).list;
			for(var index in list){
				var opt = $("<option value="+ list[index].id +" level="+ list[index].level+">"+ list[index].classname +"</option>");
				sel.append(opt);
				var list2 = list[index].childList;
				for(var index2 in list2){
					var opt2 = $("<option value="+ list2[index2].id +" level="+ list2[index2].level+">" + "&nbsp;&nbsp;&nbsp;&nbsp;"+list2[index2].classname +"</option>");
					sel.append(opt2);
					var list3 = list2[index2].childList;
					for(var index3 in list3){
						var opt3 = $("<option value="+ list3[index3].id +" level="+ list3[index3].level+">" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+list3[index3].classname +"</option>");
						sel.append(opt3);
					}
				}
			}
		}
	});
	
	
})


//展示页
function skipPage(currentPage){
	$.ajax({
		url:"/goods/findAll",
		type:"post",
		async:false,
		data:{
			currentPage:currentPage
		},
		success:function(data){
			$("#tb").empty();
			$("#tb").append(data);
			$("#fenye").empty();
			
			
			$.ajax({
				url:"/SkipPageUtils/skipPageSearch",
				type:"post",
				async:false,
				data:{
					currentPage:currentPage
				},
				success:function(data){
					$("#fenye").append(data);
				}
			})
		}
	});
}

//搜索页
function check(currentPage){
	var goodsname = $("#goodsname").val();
	$("#goodsnameIpt").val(goodsname);
	var brandId = $("#brandId").val();
	$("#brandIdIpt").val(brandId);
	var sortId = $("#sortId").val();
	$("#sortIdIpt").val(sortId);
	var sortName = $("#sortId option:selected").html();
	$("#sortNameIpt").val(sortName);
	var sortLevel = $("#sortId option:selected").attr("level");
	$("#sortLevelIpt").val(sortLevel);
	var recommend = $("#recommend").val();
	$("#recommend").val(recommend);
	
	$.ajax({
		url:"/goods/search",
		type:"post",
		async:false,
		data:{
			goodsname:goodsname,
			brandId:brandId,
			sortId:sortId,
			recommend:recommend,
			sortName:sortName,
			sortLevel:sortLevel,
			currentPage:currentPage
		},
		success:function(data){
			$("#tb").empty();
			$("#tb").append(data);
			$("#fenye").empty();
			
			$.ajax({
				url:"/SkipPageUtils/skipPageSearch2",
				type:"post",
				async:false,
				data:{
					currentPage:currentPage
				},
				success:function(data){
					$("#fenye").append(data);
				}
			})
		}
	})
}

function checkSkipPage(currentPage){
	var goodsname = $("#goodsnameIpt").val();
	var brandId = $("#brandIdIpt").val();
	var sortId = $("#sortIdIpt").val();
	var sortName = $("#sortNameIpt").val();
	var sortLevel = $("#sortLevelIpt").val();
	var recommend = $("#recommend").val();
	$.ajax({
		url:"/goods/search",
		type:"post",
		async:false,
		data:{
			goodsname:goodsname,
			brandId:brandId,
			sortId:sortId,
			recommend:recommend,
			sortName:sortName,
			sortLevel:sortLevel,
			currentPage:currentPage
		},
		success:function(data){
			$("#tb").empty();
			$("#tb").append(data);
			$("#fenye").empty();
			
			$.ajax({
				url:"/SkipPageUtils/skipPageSearch2",
				type:"post",
				async:false,
				data:{
					currentPage:currentPage
				},
				success:function(data){
					$("#fenye").append(data);
				}
			})
		}
	})
}


//ajax页面修改商品名
function ajax_updategoodsname(id, obj){
// 	debugger;
	var val = $(obj).val();
	var secondval = $(obj).attr('cls');
	if(val != secondval){
		$.ajax({
			type:"Post",
			url:"/goods/updateGoodsName",
			data:{"id":id,
				"value":val
				},
			success:function(data){
				if(data == "repeat"){
					alert("该商品名已存在！");
					$(obj).attr("value",secondval);
					return;
				}
				if(data == "namenull"){
					alert("修改的商品名不能为空！");
					$(obj).attr("value",secondval);
					return;
				}
				$(obj).attr("cls",val);
				
			}		
		}); 
	}
}
//ajax修改商品是否推荐状态
function ajax_updategoodsrecommend(id,obj){
	var val = $(obj).attr('isRecommend');
	$.ajax({
		type:"Post",
		url:"/goods/updateGoodsRecommend",
		data:{"id":id,
			"val":val
			},
		success:function(data){
			if(data == "no"){
				$(obj).attr("src","/images/no.png");
				$(obj).attr('isRecommend',"no");
			}else{
				$(obj).attr("src","/images/yes.png");
				$(obj).attr('isRecommend',"yes");
			}			
		}
	});	
}


//ajax删除一个商品
function ajax_deletegoodsone(obj){
	var model = $(obj).parent().parent();
	if(model.children("input[type='hidden']").length>0){
		var id = model.find("input[name='ids']")[0].value;
		if(!confirm("确定删除么")){
			return;
		}
		$.ajax({
			type:"Post",
			url:"/goods/deleteGoodsOne",
			data:{"id":id},
			success:function(data){
				if(data == "1"){
					window.location.reload();
					//model.remove();
				}
			}
		});
		return;
	}
}



</script>
<style id="poshytip-css-tip-skyblue" type="text/css">div.tip-skyblue{visibility:hidden;position:absolute;top:0;left:0;}div.tip-skyblue table, div.tip-skyblue td{margin:0;font-family:inherit;font-size:inherit;font-weight:inherit;font-style:inherit;font-variant:inherit;}div.tip-skyblue td.tip-bg-image span{display:block;font:1px/1px sans-serif;height:10px;width:10px;overflow:hidden;}div.tip-skyblue td.tip-right{background-position:100% 0;}div.tip-skyblue td.tip-bottom{background-position:100% 100%;}div.tip-skyblue td.tip-left{background-position:0 100%;}div.tip-skyblue div.tip-inner{background-position:-10px -10px;}div.tip-skyblue div.tip-arrow{visibility:hidden;position:absolute;overflow:hidden;font:1px/1px sans-serif;}</style></head>
<body style="">
<div class="cont">
  <h1 class="seth1">商品管理</h1>
  <div class="settab"> 
	<span class="tab-one"></span> <span class="tabs"> 
	<a href="/dispatcher/dispatch?dispatch_url=shopping_goodsManage.jsp" class="this">所有商品</a> |<a href="/dispatcher/dispatch?dispatch_url=shopping_goodsAdd.jsp">新增商品</a></span> 
	<span class="tab-two"></span>	</div>
	
	<form name="queryForm" id="queryForm" action="" method="post">
	<input type="hidden" id="currentpage" name="currentpage" value="1">
	
	<input type="hidden" id="goodsnameIpt" value=""/>
	<input type="hidden" id="brandIdIpt" value=""/>
	<input type="hidden" id="sortIdIpt" value=""/>
	<input type="hidden" id="sortNameIpt" value=""/>
	<input type="hidden" id="sortLevelIpt" value=""/>
	<input type="hidden" id="recommendIpt" value=""/>
	
    <div class="allmem_search">
      <ul>
        <li>        
          <span>商品名称</span> 
          <span class="allmen size4">
          	<input type="text" id="goodsname" name="goodsname" value="">
          </span> 
          
          <span>品牌名称</span> 
			<select name="brandId" id="brandId">                   	
      				
      					<option value="" selected="selected">请选择</option>
      				
      			
			</select>
			
          	<span>类别</span>
           		<select id="sortId" name="sortId" class="form-control m-b" style="height:30px;">
           			
           				<option value="" selected="selected">请选择</option>
           			
              					
           			
           		</select>
          
          <span>特别推荐</span>
        		<select id="recommend" name="recommend" class="form-control m-b" style="height:30px;">
        			
        				<option value="" selected="selected">请选择</option>
        				<option value="1">是</option>
        				<option value="0">否</option>
        			
        			
        			
        		</select>
          <span class="btn_search">
          	<input name="" id="search" type="button" value="搜索" onclick="check(1)" style="cursor:pointer;"/>
       	  </span> 
     	</li>
      </ul>
    </div>
  </form>
  
  <div class="operation">
    <h3>友情提示</h3>
    <ul>
      <li>上架商品，在商城前台所有访客均可查看，管理员和卖家都可以设置商品上架状态</li>
      <li>违规商品，管理员根据商城访客举报结合实际情况设定违规，违规商品前台不可显示，只能管理员能取消违规</li>
    </ul>
  </div>
  <form name="ListForm" id="ListForm" action="http://localhost:8080/goods/showGoods" method="post">
    <div class="brandtable">
      <table  width="100%" border="0" cellspacing="0" cellpadding="0" class="brand_table">
        <tbody id="tb">

        
    </tbody>
    </table>
    </div>
   </form> 
		<div class="fenye" id="fenye" align="right" id="queryCondition">
		</div>
</div>


</body></html>