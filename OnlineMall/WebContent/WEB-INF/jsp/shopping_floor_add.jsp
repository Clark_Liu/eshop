<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="/css/template.css" rel="stylesheet" type="text/css">
<script src="/js/jquery-1.6.2.js"></script>
<script src="/js/jquery.validate.min.js"></script>
<script>
function floor(){
	$.ajax({
		url:'/floor/floor_category',
		type:'post',
		async:false,
		data:{},
		success:function(data){
			var list=JSON.parse(data).floor_category;
			var set=$("#set");
	  		for(var i in list){
  				var opt = $("<option value="+ list[i].id +" level="+ list[i].level+">"+list[i].name+"</option>");
  				set.append(opt);set_opt
 				var list2 = list[i].childList;
 				for(var i2 in list2){
 					var opt2 = $("<option value="+ list2[i2].id +" level="+ list2[i2].level+">" + "&nbsp;&nbsp;&nbsp;&nbsp;"+list2[i2].name +"</option>");
 					set.append(opt2);
 				}
  			}
			
		}
	});
}
//设置默认状态以及判断初始显示状态
jQuery(document).ready(function(){
	floor();
	var index_pid=$("#index_add_pid").val();
	alert("pid="+index_pid);
	$("#goodsPrice").hide();
	if(index_pid!=null && index_pid!=""){
		var index_level=$("#index_add_level").val();
		$("[value="+index_pid+"]").prop("selected", 'selected');
		if(index_level==0){
			alert('新增下一级');
			//$("#goodsPrice").hide();
			$("#upload").hide();
			$("#set_show_mv").hide();
		}
		if(index_level==1){
			$("#set_show_mv").hide();
			$("#goodsPrice").show();
		}
	}
	var level=0;
	$("#set").change(function (){
		$(".set1").show();
		level = $('#set').find('option:selected').attr('level');
		var id=$("#set").val();
		$("#id").val(id);
		$("#level").val(level);
			if(level==0){
				alert('name');
				$("#goodsPrice").hide();
				$("#upload").hide();
				$("#set_show_mv").hide();				
			}
			if(level==1){
				$("#set_show_mv").hide();
			}
	});
	$("#sel_mv").change(function(){
		var val_mv=$("#sel_mv").val();
		$("#show_mv").val(val_mv);
	});

//表单验证 begin	
// jQuery("#theForm").validate({
//     rules:{
// 	  name:{
// 	    required :true,
// 	  },
// 	  brandLogo:{
// 	   required :true,
// 	    accept:"gif|jpg|png"
// 	  }	 	  
//     },
// 	messages:{
// 	  name:{required:"品牌名称不能为空",remote:"该品牌已经存在"}  ,
// 	  brandLogo:{required :"品牌图片不能为空",accept:"系统不允许的文件类型"}	}
//   });
  //表单验证 end
//设置显示的默认装态
//alert("设置开关样式");
var rec = jQuery("#display").val();
if(rec=="true"){
	jQuery("#displayOn").show();
	jQuery("#displayOff").hide();
}else{
	jQuery("#displayOn").hide();
	jQuery("#displayOff").show();
}
//改变系统提示的样式
  jQuery("span .w").mousemove(function(){
	var id=jQuery(this.parentNode).attr("id");
	if(id="nothis"){
	   jQuery(this.parentNode).attr("id","this");
	}
  }).mouseout(function(){
     var id=jQuery(this.parentNode).attr("id");
	 if(id="this"){
	   jQuery(this.parentNode).attr("id","nothis")
	 }
  });
});
//修改显示状态
function displayState(){
	//alert("进去修改显示状态");
	var state = jQuery("#display").val();
	if(state=="true"){
		jQuery("#display").val("false");
		jQuery("#displayOff").show();
		jQuery("#displayOn").hide();
	}else{
		jQuery("#display").val("true");
		jQuery("#displayOff").hide();
		jQuery("#displayOn").show();
	}
}
//提交
function saveForm(method){
		alert("提交");
	   jQuery("#theForm").submit();
}
</script>
<body style="">
	<div class="cont">
		<h1 class="seth1">楼层管理</h1>
		<div class="settab">
			<span class="tab-one"></span> <span class="tabs">
			<a href="/dispatcher/dispatch?dispatch_url=shopping_floor_list.jsp">管理</a> | 
			<a href="/dispatcher/dispatch?dispatch_url=shopping_floor_add.jsp" class="this">新增</a>
			</span>
		</div>
		<!-- 主页新增下一级用  begin-->
		<input type="hidden" name="index_add_pid" id="index_add_pid" value="${pid }"/>
		<input type="hidden" name="index_add_level" id="index_add_level" value="${level }"/>
	<!-- 主页新增下一级用  end-->
		<form  action="/floor/add" method="post" enctype="multipart/form-data" name="theForm" id="theForm">
			<input name="id" type="hidden" id="id" value="">
			<input id="level" type="hidden" name="level" value="0">
			<input id="show_mv" type="hidden" name="show_mv" value="1">
			<div class="setcont">
				<ul class="set1" id="name">
					<li><strong class="orange fontsize20">*</strong>楼层名称</li>
					<li><span class="webname">
					<input type="text" id="floorname" name="floorname" placeholder="请输入楼层名称" value="" size="40">
					</span></li>
				</ul>
				<ul class="set1">
					<li><span><strong class="orange fontsize20">*</strong>上级分类&nbsp;&nbsp;&nbsp;(如果选择上级楼层，那么新增的楼层类则为被选择上级楼层的子楼层)
					</span><span id="two_out" style="margin-left: 140px"></span></li>
					<li><span class="webnamesec sizese" id="one_stage" style="cursor: pointer;">
					<select name="pid" class="form-control m-b" id="set">
						<option value="0" level="0" id="set_opt">最上级</option>
					</select>
					</span></li>
				</ul>
				<ul class="set1" id="upload">
					<li><strong class="orange fontsize20">*</strong>图片上传</li>
					<li><span class="size13">
					<input name="textfield" type="text" id="textfield1">
					</span> <span class="filebtn">
					<input name="button" type="button" id="button1" value="浏览">
					</span> <span style="float: left;" class="file">
					<input type="file" name="file" id="adverLogo" size="30">
					</span> <span class="preview"></span> <span id="nothis"><strong
							class="q"></strong><strong class="w">最佳尺寸93*33，支持格式gif,jpg,png</strong><strong
							class="c"></strong></span></li>
				</ul>
				<ul class="set1">
					<li><strong class="orange fontsize20">*</strong>广告链接</li>
					<li><span class="webname">
						<input type="text" name="url" id="url" placeholder="请输入广告链接" value="">
					</span></li>
				</ul>
				<ul class="set1" id="goodsPrice">
					<li><strong class="orange fontsize20">*</strong>商品价格</li>
					<li><span class="webname">
						<input type="text" name="price" id="price" placeholder="请输入商品价格" value="">
					</span></li>
				</ul>
				<ul class="set1" id="set_show_mv">
					<li><strong class="orange fontsize20">*</strong>前台楼层显示模板选择（默认选择模板1）</li>
					<li>
						<select id="sel_mv">
							<option value="1">模板1</option>
							<option value="2">模板2</option>
						</select>
					</li>
				</ul>
				<ul class="set1" id="yesnoshow">
					<li><strong class="orange fontsize20">*</strong>显示</li>
					<li><span class="webSwitch">
					<input name="yesnoshow" id="display" type="hidden" value="">
					<img src="/images/on.jpg" width="61" height="23" id="displayOn" onclick="displayState();" style="cursor: pointer">
					<img src="/images/off.jpg" width="61" height="23" id="displayOff" onclick="displayState();" style="cursor: pointer">
					</span> <span id="nothis"> <strong class="q"></strong>
					<strong class="w">显示楼层将会在首页显示</strong><strong class="c"></strong>
					</span></li>
				</ul>
				<ul class="set1">
					<li><strong class="orange fontsize20">*</strong>排序&nbsp;&nbsp;&nbsp;(注意：排序格式必须为数字)</li>
					<li><span class="webname">
					<input name="seq" type="text" id="seq" value="" onkeyup="this.value=this.value.replace(/\D/g,&#39;&#39;)" onafterpaste="this.value=this.value.replace(/\D/g,&#39;&#39;)"></span>
					</li>
				</ul>
				<div class="submit">
					<input name="save" type="button" value="提交" onclick="saveForm('save');"/>
				</div>
			</div>
		</form>
	</div>
</body>
</html>