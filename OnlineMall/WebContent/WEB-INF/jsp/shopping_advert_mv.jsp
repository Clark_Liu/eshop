<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:forEach items="${show }" var="tmp">
<tr>
	<td align="center">${tmp.sequence }</td><!-- 排序 -->
	<td>${tmp.adTitle }</td><!--品牌名称 -->
	<td align="center"><img src="./20170605100355.png" height="44px"></td><!--品牌图片 -->
	<td align="center">${tmp.adUrl }</td><!--广告链接-->
	<td align="center">
	<!-- 是否推荐 -->
	<c:if test="${tmp.recommend }">
		<img src="/images/yes.png" width="21" height="23" onclick="ajax_updatestate(&#39;${tmp.id }&#39;,&#39;display&#39;,this)" style="cursor: pointer;" title="可编辑" class="yes" va="true">
	</c:if>
	<c:if test="${!tmp.recommend }">
		<img src="/images/no.png" width="21" height="23" onclick="ajax_updatestate(&#39;${tmp.id }&#39;,&#39;display&#39;,this)" style="cursor: pointer;" title="可编辑" class="yes" va="false">
	</c:if>
	</td>
	<td class="ac8" align="center">
	<a href="/advert/shopping_advert_edit.htm?id=${tmp.id }&&flg=eidt">编辑</a>| 
		 <a id="id" href="/advert/delete?id=${tmp.id }" onclick="if(confirm('确定删除?')==false)return false;">删除</a>
	</td>
</tr>
</c:forEach>
<!-- 排序 -->