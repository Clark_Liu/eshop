<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0047)http://localhost:8080/admin/goods_spec_list.htm -->
<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title></title>
<link href="/css/template.css" rel="stylesheet" type="text/css">
<script src="/js/jquery-1.6.2.js"></script>
<script src="/js/jquery.shop.common.js"></script>
<script src="/js/jquery.poshytip.min.js"></script>
<script>

$(function(){
	
	
	if(<%=request.getParameter("currentPage")%> != null){
 		currentPage = "<%=request.getParameter("currentPage")%>";
 	}else{
 		currentPage = 1;
 	}
	
	
	$.ajax({	//获取规格名称 及其对应的所有规格值
		url:'/goodsspec/ification',
		type:'post',
		async:false,
		data:{
			"currentPage":currentPage
		},
		success:function(data){
			$("tbody[id=tbodytop]").html(data);
			$.ajax({
				url:'/goodsspec/skipPage',
				type:'post',
				async:true,
				data:{
					"currentPage":currentPage
				},
				success:function(data){
					$("div .fenye").html(data);
				}
				
			}); 
		}
	});
	
	


	
	
});

function ajax_update(id,fieldName,obj){
   var val=jQuery(obj).val();
   jQuery.ajax({type:'POST',
	              url:'http://localhost:8080/admin/goods_spec_ajax.htm',
				  data:{"id":id,"fieldName":fieldName,"value":val},
				beforeSend:function(){
				  
				},
			   success:function(data){
	             if(val==""){
				   jQuery(obj).attr("src","http://localhost:8080/resources/style/system/manage/blue/images/"+data+".png");
				 }else{
				   jQuery(obj).val(val);
				 }      
              }
	    });
}
</script><style id="poshytip-css-tip-skyblue" type="text/css">div.tip-skyblue{visibility:hidden;position:absolute;top:0;left:0;}div.tip-skyblue table, div.tip-skyblue td{margin:0;font-family:inherit;font-size:inherit;font-weight:inherit;font-style:inherit;font-variant:inherit;}div.tip-skyblue td.tip-bg-image span{display:block;font:1px/1px sans-serif;height:10px;width:10px;overflow:hidden;}div.tip-skyblue td.tip-right{background-position:100% 0;}div.tip-skyblue td.tip-bottom{background-position:100% 100%;}div.tip-skyblue td.tip-left{background-position:0 100%;}div.tip-skyblue div.tip-inner{background-position:-10px -10px;}div.tip-skyblue div.tip-arrow{visibility:hidden;position:absolute;overflow:hidden;font:1px/1px sans-serif;}</style></head>





<body>
<div class="cont">
  <h1 class="seth1">规格管理</h1>
  <div class="settab"> <span class="tab-one"></span> <span class="tabs"> <a href="/dispatcher/dispatch?dispatch_url=goods_spec_list.jsp?currentPage=1" class="this">管理</a> | <a href="/dispatcher/dispatch?dispatch_url=goods_spec_add.html">新增</a></span> <span class="tab-two"></span></div>
  <form name="ListForm" id="ListForm" action="http://localhost:8080/admin/goods_spec_list.htm" method="post">
  <div id="list">
    <div class="typemanager">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="specific_table">
       <input type="hidden" name="currentPage" id="currentPage" value="1">
        <tbody><tr style="background:  #2A7AD2       ; height:30px; color:#FFF">
          <td width="5%">&nbsp;</td>
          <td width="10%">排序</td>
          <td width="20%">规格名称</td>
          <td width="40%">规格值</td>
          <td align="center">操作</td>
        </tr>
                
                <tbody id="tbodytop"></tbody>
                
               
      </tbody></table>
    </div>
    <div class="fenye">
      <input name="currentPage" type="hidden" id="currentPage" value="1">
	  <input name="mulitId" type="hidden" id="mulitId">
      <a href="javascript:void(0);" onclick="return gotoPage(1)">首页</a> 第　<a class="this" href="javascript:void(0);" onclick="return gotoPage(1)">1</a> 页　<a href="javascript:void(0);" onclick="return gotoPage(1)">末页</a> </div>
  </div>
</form>
</div>


<div class="tip-skyblue"><div class="tip-inner tip-bg-image"></div><div class="tip-arrow"></div></div><div class="tip-skyblue"><div class="tip-inner tip-bg-image"></div><div class="tip-arrow"></div></div><div class="tip-skyblue"><div class="tip-inner tip-bg-image"></div><div class="tip-arrow"></div></div><div class="tip-skyblue"><div class="tip-inner tip-bg-image"></div><div class="tip-arrow"></div></div><div class="tip-skyblue"><div class="tip-inner tip-bg-image"></div><div class="tip-arrow"></div></div><div class="tip-skyblue"><div class="tip-inner tip-bg-image"></div><div class="tip-arrow"></div></div><div class="tip-skyblue"><div class="tip-inner tip-bg-image"></div><div class="tip-arrow"></div></div><div class="tip-skyblue"><div class="tip-inner tip-bg-image"></div><div class="tip-arrow"></div></div><div class="tip-skyblue"><div class="tip-inner tip-bg-image"></div><div class="tip-arrow"></div></div><div class="tip-skyblue"><div class="tip-inner tip-bg-image"></div><div class="tip-arrow"></div></div><div class="tip-skyblue"><div class="tip-inner tip-bg-image"></div><div class="tip-arrow"></div></div><div class="tip-skyblue"><div class="tip-inner tip-bg-image"></div><div class="tip-arrow"></div></div></body></html>