<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<a href="/dispatcher/dispatch?dispatch_url=goods_spec_list.jsp?currentPage=1">首页</a>
<c:if test="${currentPage != 1 }">
	<a href="/dispatcher/dispatch?dispatch_url=goods_spec_list.jsp?currentPage=${currentPage-1 }">前一页</a>
</c:if>
<c:forEach begin="1" end="${totalPageNum }" var="i">
	<c:if test="${currentPage == i }">
		<a href="/dispatcher/dispatch?dispatch_url=goods_spec_list.jsp?currentPage=${i }" class="this">${i }</a>
	</c:if>
	<c:if test="${currentPage != i }">
		<a href="/dispatcher/dispatch?dispatch_url=goods_spec_list.jsp?currentPage=${i }">${i }</a>
	</c:if>
</c:forEach>
<c:if test="${currentPage != totalPageNum }">
	<a href="/dispatcher/dispatch?dispatch_url=goods_spec_list.jsp?currentPage=${currentPage+1 }">后一页</a>
</c:if>
<a href="/dispatcher/dispatch?dispatch_url=goods_spec_list.jsp?currentPage=${totalPageNum }">末页</a>