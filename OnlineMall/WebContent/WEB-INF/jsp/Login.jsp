<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>shopping商城系统后台登录</title>
<link href="/css/login.css" rel="stylesheet" type="text/css">
<script src="/js/jquery-1.6.2.js"></script>
<script src="/js/jquery.validate.min.js"></script>
<script>
function refreshCode(){
	$("#code_img").attr("src","/user/creatCaptcha?d="+new Date().getTime());
}


	function login(){
		$(".warnning").hide();
		if( $('#code').val()==""  && ($('#username').val()!='' || $('#password').val()!='')){
			$("#codeIsNull").show();
		}else{
			$("#theForm").submit();
		}
	}
	
	

//绑定登录时间
var EnterSubmit = function(evt){
evt = window.event || evt;
 if (evt.keyCode == 13)
 { 
  login();
 }
}
window.document.onkeydown=EnterSubmit;


$(document).ready(function(){
	$(".warnning").hide();
	$("#codeIsWrong").hide();
     if(top.location!=this.location)top.location=this.location;//跳出框架在主窗口登录
	$('#username').focus();	
     
     
    var warnning = '<%=request.getParameter("warnning")%>';
    switch(warnning){
    case 'code':
    	$('#codeIsWrong').show();
   		break;
    case 'username':
    	$('#usernameWarnning').show();
   		break;
    case 'password':
    	$('#passwordWarnning').show();
   		break;
    }
     
     
     
     
     
});
</script>
</head>



<body>
<c:set value="false" var="isLogin" scope="session"></c:set>
<form name="theForm" id="theForm" action="/user/login" method="post">
<div class="main_body">
  <div class="login_top"> </div>
  <div class="login_mid">
    <div class="login_left"></div>
    <div class="login_m">
      <ul>
        <li>
          <div class="usename"></div>
		  		  <div class="utxt">
			<input name="username" type="text" id="username" value="">
		 </div>
		 <span class="warnning" id="usernameWarnning">*用户名不存在</span>
		</li>
        <li>
          <div class="password"></div>
          <div class="ptxt">
            <input name="password" type="password" id="password" autocomplete="false">
          </div>
		 <span class="warnning" id="passwordWarnning">*密码错误</span>
        </li>
        <li>
          <div class="yzm"></div>
          <div class="ytxt">
            <input name="code" type="text" id="code" style="text-transform:uppercase;" autocomplete="false">
          </div>
          <div class="yzmimg">
          	<!-- 验证码图片 -->
          	<img style="cursor:pointer;" src="/user/creatCaptcha?d=" id="code_img" onclick="refreshCode();" width="59" height="27">
		 	<span class="warnning" id="codeIsWrong">*验证码错误</span>
		 	<span class="warnning" id="codeIsNull">*验证码不能为空</span>
          </div>
        </li>
      </ul>
    </div>
    <div class="login_r"></div>
  </div>
  <div class="login_mid2">
    <div class="login_left2"></div>
    <div class="login_m2">
	  <div style="width:140px;float:left;">
	  <a href="http://localhost:8080/index.htm" target="_blank" style="padding-top:0px; display:block; float:right; margin-right:30px;color:#fff">返回首页</a></div>
      <div class="m1">
        <input name="" type="button" value="" onclick="login();" style="cursor:pointer">
        <input name="login_role" type="hidden" id="login_role" value="admin">
      </div>
      <div class="m2">
        <input name="" type="reset" value=""   style="cursor:pointer" >
      </div>
    </div>
    <div class="login_r2"></div>
  </div>
  <div class="bottom">
    <div class="bottoml"></div>
    <div class="bottomm"></div>
    <div class="bottomr"></div>
  </div>
  <div class="tell">
    <p>Copyright 2011-2014 © <a href="http://localhost:8080/admin/index.htm#" target="_blank"><span style="color: #2662AC;">shopping</span></a> xxx</p>
    </div>
</div>
</form>


</body>
</html>