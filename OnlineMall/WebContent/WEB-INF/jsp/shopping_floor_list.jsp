<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="/css/template.css" rel="stylesheet" type="text/css">
<script src="/js/jquery-1.6.2.js"></script>
<script src="/js/jquery.shop.common.js"></script>
<script src="/js/jquery.poshytip.min.js"></script>
<script>
//显示两级楼层
function addorsubtract(obj, id) {
	// 	alert(pid);
	var cls = jQuery(obj).attr("cls");
	var level = jQuery(obj).attr("level");
 	var parent = $("#" + id).attr("parent");
 	alert(parent);
	if(cls=='jian'){
		$.ajax({
			url:'/floor/index',
			type:'post',
			async:true,
			data:{
				'id':id,
				'level':level
			},
			success:function(data){
					jQuery("#" + id).after(data);
					if(level==1){
					$("[parent="+id+"]").attr("parent",parent);
					}
					jQuery(obj).attr("src", "/images/add.jpg");
					jQuery(obj).attr("cls", "add");	
			}			
		});
	} else {
			$("tr[parent=" + id + "]").remove();
			$(obj).attr("src", "/images/jian.jpg");
			$(obj).attr("cls", "jian");
		}
	
// 	if(parent == null) {
// 		if(cls == "jian") {
// 			$.ajax({
// 				type: "POST",
// 				url: "/floor/secondFloor",
// 				data: {
// 					"id": id,
// 					"currentPage": "1"
// 				},
// 				success: function(data) {
// 					jQuery("#" + id).after(data);
// 					jQuery(obj).attr("src", "/images/add.jpg");
// 					jQuery(obj).attr("cls", "add");
// 					tipStyle();
// 				}
// 			});
// 		} else {
// 			$("tr[parent=" + id + "]").remove();
// 			$(obj).attr("src", "../images_project/floor/jian.jpg");
// 			$(obj).attr("cls", "jian");
// 		}
// 	} else {
// 		$("tr[level=child_" + id + "]").remove();
// 		$(obj).attr("src", "../images_project/floor/add.jpg");
// 		$(obj).attr("cls", "jian");
// 	}
}
//ajax修改该分类状态
function ajax_updateyesnoshow(id, filedName, obj) {
	// 	debugger
	// 	alert("lll");
	var val = $(obj).hasClass('yes');
	$.ajax({
		type: "Post",
		url: "/floor/updateFloorState",
		data: {
			"id": id
		},
		success: function(data) {
			// 				debugger				
			// 				alert(data);
			if(val) {
				$(obj).attr("src", "../images_project/floor/no.png");
				$(obj).removeClass('yes');
			} else {
				$(obj).attr("src", "../images_project/floor/yes.png");
				$(obj).addClass('yes');
			}
		}
	});
}
//ajax修改可编辑框中的分类名、排序					
function ajax_updatefloornameseq(id, fieldName, obj) {
	// 	debugger;
	// 	alert("111");
	var fname = $(obj).val();
	var secondval = $(obj).attr('cls');
	if(fname != secondval) {
		$.ajax({
			type: "Post",
			url: "/floor/updateFloorNameSeq",
			data: {
				"id": id,
				"fieldName": fieldName,
				"value": fname
			},
			success: function(data) {
				// 				debugger
				// 				alert("222");
				if(fname == "") {
					if(data == "namenull") {
						alert("分类名称不能为空!");
						$(obj).attr("value", secondval);
					} else {
						alert("排序不能为空!");
						$(obj).attr("value", secondval);
					}
				} else if(data == "repeat") {
					alert("分类名称不能重复!");
					$(obj).attr("value", secondval);
				} else if(data == "geshi") {
					alert("排序格式必须为数字!");
					$(obj).attr("value", secondval);
				}
			}
		});
	}
}

//删除单条记录
function ajax_deletefloorone(id) {
	// 	debugger;
	if(!confirm("确定删除么")) {
		return;
	}
	$.ajax({
		type: "POST",
		url: "/floor/deleteFloorOne",
		data: {
			"id": id,
		},
		success: function(msg) {
			if(msg == "1") {
				window.location.reload();
			} else {
				alert(msg);
			}
		}
	});
}
//一级数据获取
function index(){
	//alert(首页);
	$.ajax({
		url:'/floor/index',
		type:'post',
		data:{},
		async:true,
		success:function(data){
			$("#tbody").html(data);
		}
	});
}
$(document).ready(function(){
	index();
});
</script>
<style id="poshytip-css-tip-skyblue" type="text/css">
div.tip-skyblue {
	visibility: hidden;
	position: absolute;
	top: 0;
	left: 0;
}

div.tip-skyblue table,
div.tip-skyblue td {
	margin: 0;
	font-family: inherit;
	font-size: inherit;
	font-weight: inherit;
	font-style: inherit;
	font-variant: inherit;
}

div.tip-skyblue td.tip-bg-image span {
	display: block;
	font: 1px/1px sans-serif;
	height: 10px;
	width: 10px;
	overflow: hidden;
}

div.tip-skyblue td.tip-right {
	background-position: 100% 0;
}

div.tip-skyblue td.tip-bottom {
	background-position: 100% 100%;
}

div.tip-skyblue td.tip-left {
	background-position: 0 100%;
}

div.tip-skyblue div.tip-inner {
	background-position: -10px -10px;
}

div.tip-skyblue div.tip-arrow {
	visibility: hidden;
	position: absolute;
	overflow: hidden;
	font: 1px/1px sans-serif;
}
</style>
</head>
<body style="">
	<div class="cont">
		<h1 class="seth1">楼层管理</h1>
		<div class="settab">
		<span class="tab-one"></span> <span class="tabs">
		<a href="/dispatcher/dispatch?dispatch_url=shopping_floor_list.jsp" class="this">管理</a> | 
		<a href="/floor/shopping_floor_add?flg=add">新增</a>
		</span> <span class="tab-two"></span>
	</div>
	<div class="operation">
			<h3>友情提示</h3>
			<ul>
				<li>通过首页楼层管理，你可以进行任意定制自己的商城首页楼层信息 (<strong class="orange fontsize20">*</strong>楼层采用两层管理，第二层用来显示自定义商品信息)
				</li>
				<!--       <li>你可以根据需要控制商品分类是否显示</li> -->
		</ul>
	</div>
	<div class="classtable">
		<form name="ListForm" id="ListForm" action="http://localhost:8080/floor/showFloor" method="post">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="class_table">
		<tbody>
			<tr style="background: #2A7AD2; height: 30px; color: #FFF">
				<td width="70" align="right">排序</td>
				<td width="450">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;楼层名称</td>
				<td width="200" align="center"></td>
				<td width="100" align="center">是否显示</td>
				<td width="126" align="center">操作</td>
			</tr>
		</tbody>
		<tbody id="tbody">
		</tbody>
	</table>
		<input type="hidden" name="currentPage" id="currentPage" value="1">
		<input name="mulitId" type="hidden" id="mulitId">
		<div class="fenye">
			<a class="this" href="http://localhost:8080/floor/showFloor#">首页</a>
			<a class="this" href="http://localhost:8080/floor/showFloor#">1</a>
			<a class="this" href="http://localhost:8080/floor/showFloor#">尾页</a>
		</div>
</form>
		</div>
	</div>
</body>
</html>