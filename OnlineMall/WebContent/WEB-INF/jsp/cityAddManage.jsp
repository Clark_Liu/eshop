<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE >
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>添加分类</title>
<link href="/css/template.css" rel="stylesheet" type="text/css">
<script src="/js/jquery-1.6.2.js"></script>
<script src="/js/jquery.validate.min.js"></script>
<script>



//提交
function saveForm(method){
	
	
	var tid0 = $("#tid01").val();
	var tid1 = $("#tid02").val();
	var tid2 = $("#tid03").val();
	
	var level = null;
	if(tid0 == "a"){
		level = 0;
	}else if(tid1 == "b"){
		level = 1;
		$("#parentId").val(tid0);
	}else if(tid2 == "c"){
		level = 2;
		$("#parentId").val(tid1);
	}else{
		level = 3;
		$("#parentId").val(tid2);
	}
	$("#level").val(level);
	
	
	
	$("#theForm").submit();
}



//设置默认状态以及判断初始显示状态
$(document).ready(function(){
	var nowPid = 0;
	var nowLV = "a";
	showLower(0,"a");

	var state = $("#display").val();
// 		debugger;
// 		alert("111");
 	if(state==""){
 		$("#display").val("1");
 		$("#displayOff").hide();
 	}else{
 		 if(state=="1"){
 			$("#displayOff").hide();
 		}else{
 			$("#displayOn").hide();
 		}
	};
	

	
//改变系统提示的样式
  $("span .w").mousemove(function(){
	var id=$(this.parentNode).attr("id");
	if(id="nothis"){
	   $(this.parentNode).attr("id","this");
	}
  }).mouseout(function(){
     var id=$(this.parentNode).attr("id");
	 if(id="this"){
	   $(this.parentNode).attr("id","nothis");
	 }
  });
  
  
  
   $("#theForm").validate({
	rules:{
		cityname:{
			required :true,
			remote:{
				url:"/city/isExistCityName",
				type: "post",  //数据发送方式
				dataType: "json",
	            data: {        //要传递的数据
	                  "cityname":
	                	  function(){
	                	 	 return $("#cityname").val();
	                	  },
	                  "level":
	                	  function(){
	                	  	return $("#level").val();
	                  	  }
                }
		   }
		},seq:{
			digits:true,
			min:0
		}
	},
	messages:{
		cityname:{
			required:"城市名不能为空!",
			remote:"该城市名已存在"
		},seq:{
			digits:"请输入整数",
			min:"请大于0"
		}
	},
	onkeyup:false,
  }); 
  
});


function displayState(){
	var state = $("#display").val();
	if(state=="1"){
		$("#display").val("0");
		$("#displayOff").show();
		$("#displayOn").hide();
	}else{
		$("#display").val("1");
		$("#displayOff").hide();
		$("#displayOn").show();
	}
}




//查询下级地区
function showLower(pid,lv){
	if(lv=="a"){
		if(pid=="a"){
			showoption(0,"a");
			$("#level").val("0");
		}else{
			showoption(pid,"a");
			nowLV="b";
			nowPid = pid;
			$("#level").val("1");
		}
		return;
	}else if(lv=="b"){     // pid为b 是二级下拉列表框的未选中状态  查询该一级分类下所有二级分类
		var pid1 = $("#tid01").val();
		if(pid == "b"){
			showoption(0,"b");
			nowPid = pid1;
			nowLV = "b";
			$("#level").val("1");
		}else{
			showoption(pid,"b");
			nowPid = pid;
			nowLV ="c";
			$("#level").val("2");
		}
		return;
	}
		var pid2 = $("#tid02").val();
		if(pid == "c"){
			showoption(0,"c");
			nowPid = pid2;
			nowLV = "c";
			$("#level").val("2");
		}else{ 
			showoption(pid,"c");
			nowPid = pid;
			nowLV = "d";
			$("#level").val("3");
		}
}
//查询之后显示城市详细
function showCity(){
	alert(nowPid);
}	

//省市区的下拉列表
function showoption(pid,lv) {
	 $.ajax({
		  type:"POST",
	      url:"/city/showSelectCity",
	      data:{"pid":pid},	       
		  success:function(data){
			  	list = JSON.parse(data).list;
			  	if(pid==0&&lv=="a"){
					$("option[name='option1']").remove();
					$("option[name='option2']").remove();
					$("option[name='option3']").remove();
					for(var index in list){
						$("#tid01").append("<option value='"+list[index].id+"' name='option1'>"+list[index].areaname+"</option>");
					}
			  	}else if(pid!=0&&lv=="a"){
					$("option[name='option2']").remove();
					$("option[name='option3']").remove();
					for(var index in list){	  
						$("#tid02").append("<option value='"+list[index].id+"' name='option2'>"+list[index].areaname+"</option>");
					};
				  }else if(pid==0&&lv=="b"){
					$("option[name='option3']").remove();
				}else if(pid!=0&&lv=="b"){
					$("option[name='option3']").remove();
					for(var index in list){	  
						$("#tid03").append("<option value='"+list[index].id+"' name='option3'>"+list[index].areaname+"</option>");
					};
				}
		  }
	 });	

} 
</script>
</head>

<body style="">
<div class="cont">
  <h1 class="seth1">地区设置</h1>
  <div class="settab"><span class="tab-one"></span>
      <span class="tabs">
      		<a href="/dispatcher/dispatch?dispatch_url=cityManage.jsp">管理</a> | <a href="/dispatcher/dispatch?dispatch_url=cityAddManage.jsp" class="this">新增</a>   
      		&nbsp;&nbsp;&nbsp;<input type="button" value="刷新" onclick="JavaScript:window.location.reload()"><br>
      </span>
 </div>
 <form name="theForm" id="theForm" action="/city/addCity" method="post">

  <input name="id" type="hidden" id="id" value="">
  <div class="setcont">
  
    <ul class="set1">
      <li><strong class="orange fontsize20">*</strong>城市名</li>
      <li>
      	<span class="webname">
      	<input type="text" id="cityname" name="cityname" placeholder="请输入需要新增的城市名" value="" size="40">
      	</span>
      </li>
    </ul>
        

    <ul class="set1">
      <li><span><strong class="orange fontsize20">*</strong>上级分类&nbsp;&nbsp;&nbsp;(如果选择上级分类，那么新增的分类则为被选择上级分类的子分类) </span><span id="two_out" style="margin-left: 140px"></span></li>
      <li>
	      <span class="webnamesec sizese" id="one_stage" style="cursor: pointer;">
				<input type="hidden" value="0" name="level" id="level" />
				<input type="hidden" value="" name="parentId" id="parentId"/>
				<select name="tid01" id="tid01" level="0" onchange="showLower(this.value,'a')">
					<option value="a">省份</option>
				
				</select>

	            <select name="tid02" id="tid02" level="1" size="1" onchange="showLower(this.value,'b')">
	            	<option value="b">城市</option>         
	          	</select>

          		<select name="tid03" id="tid03" level="2" size="1" onchange="showLower(this.value,'c')">
            		<option value="c">区县</option>
 				</select>

	      </span>
      </li>
    </ul>
	
	
	<ul class="set1">
      <li><strong class="orange fontsize20">*</strong>显示</li>
      <li>
      	<span class="webSwitch">
   	 	<input name="yesnouse" id="display" type="hidden" value="1">
        <img src="/images/on.jpg" width="61" height="23" id="displayOn" onclick="displayState();" style="cursor: pointer"> 
        <img src="/images/off.jpg" width="61" height="23" id="displayOff" onclick="displayState();" style="cursor: pointer; display: none;">       	     
        </span>
        <span id="nothis">
	        <strong class="q"></strong>
	        <strong class="w">显示地区将会在首页显示</strong>
	        <strong class="c"></strong>
        </span>
    </li>
    </ul>


    <ul class="set1">
      <li><strong class="orange fontsize20">*</strong>排序&nbsp;&nbsp;&nbsp;(注意：排序格式必须为数字;不写默认为0)</li>
	  <li> <span class="webname">
	  		<input name="seq" type="text" id="seq" value="" onkeyup="this.vlaue=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')">
	  	   </span>
	       	       
	  </li>    
    </ul>
  	<div class="submit">
    	<input name="save" type="button" value="提交" onclick="saveForm()">
 	</div>
   </div>
  </form>

</div>

</body></html>