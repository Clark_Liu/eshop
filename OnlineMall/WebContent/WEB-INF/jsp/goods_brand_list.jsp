<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<link href="/css/template.css"  rel="stylesheet" type="text/css"/>
<script src="/js/jquery-1.6.2.js"></script>
<script src="/js/jquery.shop.common.js"></script>
<script src="/js/jquery.poshytip.min.js"></script>
<script>
function  load(){
	var name=$("#name").val();
	var category=$("#category").val();
	//alert(name+"   "+category);
	$.ajax({
		url:'/brand/selectall',
		type:'post',
		async:false,
		data:{
			'name':name,
			'category':category,
			'pagenum' :1
			
		},
		success:function(data){
			fenye();
			//alert(data);
			//var tb=$("table");
			var tbody=$("#tb1");
			tbody.html(data);
		}
		
	});
}
function gotoPage(page){
	  //alert("分页ajax="+page);
	  var name=$("#name").val();
	  var category = $("#category").val();
	  $.ajax({
		  url:'/brand/fenye',
		  type:'post',
		  data:{
			  'name':name,
			'category':category,
			  "pagenum":page
		  },
		  async:true,
			success:function(data){
				fenye();
				var tbody=$("#tb1");
				tbody.html(data);
			}
	  });
}
function fenye(){
	$.ajax({
		url:'/brand/fenye1',
		type:'post',
		async:true,
		data:{},
		success:function(data){
			//alert(data);
			var div=$("#queryCondition");
			div.html(data);
		}
		
	});
}
jQuery(document).ready(function(){
	load();
	//fenye();
  //tipStyle();
//设置推荐的默认装态
	function recommendState(){
	var state = jQuery("#recommend").val();
	if(state=="true"){
		jQuery("#recommend").val("false");
		jQuery("#stateOff").show();
		jQuery("#stateOn").hide();
	}else{
		jQuery("#recommend").val("true");
		jQuery("#stateOff").hide();
		jQuery("#stateOn").show();
	}
}
});
//批量删除
function deleteall(){
	  //alert("进入批量删除");
	  var flg=confirm("确认批量删除选项？");
	  if(flg){
			//复选框的集合
			var ckbList = $("input[name='checkbox']");
			//id的集合
			var ckbId = [];
			//勾选的复选框的集合
			var ckbed = [];
			//勾选的复选框的级别的集合
			var ckbed_level = [];
			 for(var index in ckbList){
				 //ckbList中有其他不相关元素,排除掉
				if(index < ckbList.length){
					//勾选的返回true
					if($(ckbList[index]).prop("checked")){
						//把勾选的id push 进数组中
						ckbId.push($(ckbList[index]).attr("value"));
						//把勾选的选框 push 进数组中
						//ckbed.push($(ckbList[index]));
						//把勾选的选框的level push 进数组中
						//ckbed_level.push($(ckbList[index]).parent().parent().attr("levelS"));
					}
				}
			};
			//alert(ckbId);
		  $.ajax({
			  url:'/brand/deleteall',
			  type:'post',
			  async:true,
			  traditional:true,
			  data:{
				  "id":ckbId
			  },
			  success:function(data){
				  alert("返回页");
				  //window.location.href="/dispatcher/dispatch?dispatch_url=goods_brand_list.html";
				  if(data==1){
					  window.location.href="/dispatcher/dispatch?dispatch_url=goods_brand_list.html";
				  }
			  }
		  });
	  }
}
//网站logo file样式
jQuery(function(){
    var textButton="<input type='text' name='textfield' id='textfield1' class='size13' /><input type='button' name='button' id='button1' value='' class='filebtn' />"
	jQuery(textButton).insertBefore("#brandLogo");
	jQuery("#brandLogo").change(function(){
	jQuery("#textfield1").val(jQuery("#brandLogo").val());
	})
});	
//页面修改内容
function ajax_update(id,fieldName,obj){
	//alert("页面修改")
   var val=jQuery(obj).val();
	//alert(val);
	  var flg=0;
	   if(val==""){
		   val=$(obj).attr("va");
		   flg=1;
	   }
   jQuery.ajax({type:'POST',
	              url:'/brand/update',
				  data:{"id":id,"fieldName":fieldName,"value":val},
				beforeSend:function(){
				  
				},
			   success:function(data){
					  //alert("data"+data);
			             if(flg=1){
						   $(obj).attr("src","/images/"+data+".png");
						   $(obj).attr("va",data);
						   flg=0;
						 }else{
						   $(obj).val(val);
						 }      
              }
	    });
}
</script>
<body>
<div class="cont">
  <h1 class="seth1">品牌管理</h1>
  <div class="settab"> 
	<span class="tab-one"></span> <span class="tabs"> 
	<a href="/dispatcher/dispatch?dispatch_url=/goods_brand_list.html" class="this">管理</a> | 
	<a href="/brand/goods_brand_edit.htm?flg=add">新增</a> 
<!--     <a href="/dispatcher/dispatch?dispatch_url=\/goods_brand_audit.html">申请列表</a> -->
	</span> 
	<span class="tab-two"></span>	</div>
	<form name="queryForm" id="queryForm" action="" method="post">
    <div class="allmem_search">
      <ul>
        <li> <span>品牌名称</span> <span class="allmen size4">
          <input name="name" type="text" id="name" />
          </span> <span>类别</span> <span class="allmen size4">
          <input name="category" type="text" id="category" />
          </span> <span class="btn_search">
          <input name="" type="button"  value="搜索" style="cursor:pointer;" id="search" onclick="load();"/>
          </span> </li>
      </ul>
    </div>
  </form>
  <div class="operation">
    <h3>友情提示</h3>
    <ul>
      <li> 通过商品品牌管理，你可以进行查看、编辑、删除系统商品品牌</li>
      <li>设置推荐的品牌会在商城首页根据序号从小到大的顺利轮换显示</li>
      <li>设置品牌首字母，在品牌列表页通过首字母搜索品牌</li>
    </ul>
  </div>
  <form name="ListForm" id="ListForm" action="" method="post">
    <div class="brandtable">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="brand_table">
        <tbody>
        <tr style="background:blue; height:30px; color:#FFF">
          <td width="45">&nbsp;</td>
          <td width="60">排序</td>
          <td width="80">首字母</td>
          <td width="203">品牌名称</td>
          <td width="194">类别</td>
          <td width="183">品牌图片标识</td>
          <td width="121" align="center">推荐</td>
          <td width="217" align="left">操作</td>
        </tr>
        </tbody>
        <tbody id="tb1">
        </tbody>
    </table>
       <div class="fenye" align="right" id="queryCondition">
       </div>
    </div>
  </form>
</div>
</body>
</html>