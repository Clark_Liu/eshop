<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title></title>
<link href="/css/template.css" rel="stylesheet" type="text/css">
<script src="/js/jquery-1.6.2.js"></script>
<script src="/js/jquery.validate.min.js"></script>
<script>
$(document).ready(function(){
	
	
	
		//分类下拉列表
		$.ajax({
			url:'/goodsclass/findSelectClass',
			type:'post',
			async:true,
			data:{},
			success:function(data){
				var sel = $("#pid");
				var list = JSON.parse(data).list;
				for(var index in list){
					var opt = $("<option value="+ list[index].id +" level="+ list[index].level+">"+ list[index].classname +"</option>");
					sel.append(opt);
					var list2 = list[index].childList;
					for(var index2 in list2){
						var opt2 = $("<option value="+ list2[index2].id +" level="+ list2[index2].level+">" + "&nbsp;&nbsp;&nbsp;&nbsp;"+list2[index2].classname +"</option>");
						sel.append(opt2);
					}
				}
			}
		});
		
		
		//规格下拉列表
		$.ajax({
			url:'/goodstype/selectAll',
			type:'post',
			async:true,
			data:{},
			success:function(data){
				var sel = $("#goodsTypeId");
				var list = JSON.parse(data).list;
				for(var index in list){
						var opt = $("<option value="+ list[index].id +">"+ list[index].name +"</option>");
					sel.append(opt);
				}
			}
		})
		
		
//设置默认状态以及判断初始显示状态
	var state = $("#display").val();
	if(state==""){
		$("#display").val("true");
		$("#displayOff").hide();
	}else{
		 if(state=="true"){
			$("#displayOff").hide();
		}else{
			$("#displayOn").hide();
		}
	}
	
	var state1 = $("#recommend").val();
	if(state1==""){
		$("#recommend").val("true");
		$("#recommendOff").hide();
	}else{
		 if(state1=="true"){
			$("#recommendOff").hide();
		}else{
			$("#recommendOn").hide();
		}	
	}	
	
 	//改变系统提示的样式
  $("span .w").mousemove(function(){
	var id=$(this.parentNode).attr("id");
	if(id="nothis"){
	   $(this.parentNode).attr("id","this")
	}
  }).mouseout(function(){
     var id=$(this.parentNode).attr("id");
	 if(id="this"){
	   $(this.parentNode).attr("id","nothis")
	 }
  });
	$("#pid").val("");
	$("#goodsTypeId").val("");

	
	
	
	
  $("#theForm").validate({
    rules:{
	  className:{
	    required :true,
	     remote:{
		    url: "/goodsclass/verifyClassName",   //后台处理程序
            type: "post",  //数据发送方式
            data: {        //要传递的数据
                  "className":
                	  function(){
                	 	 return $("#className").val();
                	  }
		     }
		}
	  },sequence:{
		  required :true,
		  digits:true,
	  },icon_acc:{
		  remote:{
			  url:"/goodsclass/verifyFileType",
			  type:"post",
			  data:{
				  "file":
					  function(){
					  return  $('#icon_acc').val();
				  }
			  }
		  }
	  }
	 },
	messages:{
	  className:{
		  required:"分类名称不能为空",
		  remote:"该分类已经存在"}
	,sequence:{
		  required:"分类顺序不能为空",
		  digits:"必须输入整数",
	  },icon_acc:{
		  remote:"上传文件格式不正确,请上传jgp,png,gif格式"
	  }
	}
  });
  

  
  
  //图标
  $("#pid").change(function(){
	 var val = $(this).val();
	  if(val==""){
		 $("#icon").show(); 
		 $("#icon_sys_ul").show();
		  }else{
		 $("#icon").hide();	
		 $("#icon_sys_ul").hide();  
		 $("#icon_acc_ul").hide(); 
			  }
	  });
  $("input[type='radio']").click(function(){
	 var val = $(this).attr("value");
	  if(val==1){
	  $("#icon_sys_ul").hide();
	  $("#icon_acc_ul").show();	  
	  }else{
	  $("#icon_sys_ul").show();
	  $("#icon_acc_ul").hide();		  
	  }
  });
  
  //系统图标
  $(".icon_sys a").click(function(){
	  $("#icon_sys").val($(this).attr("icon"));
	  $(this).parent().find("a").removeClass("this");
	  $(this).addClass("this");
	  
	  
	  });
  
  
   $("#icon_acc").change(function(){
	 $("#textfield1").val($("#icon_acc").val());
   })
     $("#logoShow").mouseover(function(){
	    $("#logoImg").css('display','block');
   }).mouseout(function(){
		$("#logoImg").css('display','none');
	});
});





function saveForm(method){
	//类名
	var className = $("#className").val();
	//父级id
	var pid = $("#pid option:selected").val();
	
	
	$("#parentId").val(pid);
	
	
	
	var pid_level = $("#pid option:selected").attr("level");
	if(pid_level == 0){
		$("#level").val("1");
	}else if(pid_level == 1){
		$("#level").val("2");
	}else{
		$("#level").val("0");
	}
	
	
	var typeId = $("#goodsTypeId option:selected").val();
	$("#goodstypeId").val(typeId);
	
	
	var seq = $("#sequence").val();
	
	var icon_sys_id = $("#icon_sys").val();
	
  $("#theForm").submit();
}

//设置显示开关
function displayState(){
	var state = $("#display").val();
	if(state=="true"){
		$("#display").val("false");
		$("#displayOff").show();
		$("#displayOn").hide();
	}else{
		$("#display").val("true");
		$("#displayOff").hide();
		$("#displayOn").show();
	}
}

//设置推荐开关
function recommendState(){
	var state = $("#recommend").val();
	if(state=="true"){
		$("#recommend").val("false");
		$("#recommendOff").show();
		$("#recommendOn").hide();
	}else{
		$("#recommend").val("true");
		$("#recommendOff").hide();
		$("#recommendOn").show();
	}
}


function iconCheck(obj){
	
}
</script>
</head>
<body>
<div class="cont">
  <h1 class="seth1">商品分类</h1>
  <div class="settab"><span class="tab-one"></span>
      <span class="tabs">
       <a href="/dispatcher/dispatch?dispatch_url=category.jsp?currentPage=1">管理</a> |
       <a  class="this">新增</a>	   
       	   </span>
      
      <span class="tab-two"></span></div>
      <!-- 表单 -->
 	<form name="theForm" id="theForm" action="/goodsclass/addOrEditClass" method="post" enctype="multipart/form-data" novalidate="novalidate">
 		<input name="pageType" type="hidden" value="add">
 		<input name="id" type="hidden" id="id" value=''>
 		<input name="parentId" type="hidden" id="parentId" value="">
		<input name="currentPage" type="hidden" id="currentPage" value="">
		<input name="list_url" type="hidden" id="list_url" value="">
		<input name="add_url" type="hidden" id="add_url" value="">
		
  		<div class="setcont">
  		<ul class="set1">
     	<li><strong class="orange fontsize20">*</strong>分类名称</li>
     	<li><span class="webname">
        <input name="className" type="text" id="className" value="" size="40">
     	</span>
     	</li>
    </ul>
    <!--鼠标未经过样式-->
    <ul class="set1">
      <li>上级分类</li>
      <li><span class="webnamesec sizese">
        <select name="pid" id="pid">
              <option value="">请选择上级商品分类...</option>
              
        </select>
        <input name="level" type="hidden" id="level" value="">
      </span><span id="nothis"><strong class="q"></strong><strong class="w">如果选择上级分类，那么新增的分类则为被选择上级分类的子分类</strong><strong class="c"></strong></span></li>
    </ul>
	<ul class="set1">
      <li>类型</li>
	  <li>关联类型到下级<input name="child_link" type="checkbox" id="child_link" value="true">
	  </li>
      <li><span class="webnamesec sizese">
        <select name="goodsTypeId" id="goodsTypeId">
		  <option value="">请选择...</option>
		  
		</select>
        <input name="goodstypeId" type="hidden" id="goodstypeId" value="">
		
      </span><span id="nothis"><strong class="q"></strong><strong class="w">如果当前下拉选项中没有适合的类型，可以去<a href="javascript:void(0);" onclick="window.parent.openURL(&#39;url&#39;,&#39;http://localhost:8080/admin/goods_type_list.htm&#39;,&#39;main_workspace&#39;,&#39;goods_type_op&#39;,&#39;about_goods&#39;)" style="color:#fff; background-color:#FF6600; padding:2px 4px; border-radius:4px 4px 4px 4px;">类型管理</a>功能中新增新的类型</strong></span></li>
      <strong></strong>
	</ul>
	
	<ul class="set1">
      <li>显示</li>
     <input name="display" id="display" type="hidden" value="true">
          <img src="/images/on.jpg" width="61" height="23" id="displayOn" onclick="displayState();" style="cursor:pointer"> 
          <img src="/images/off.jpg" width="61" height="23" id="displayOff" onclick="displayState();" style="cursor: pointer; display: none;">
    </ul>
	<ul class="set1">
      <li>推荐</li>
       <li> <span class="webSwitch">
      <input name="recommend" id="recommend" type="hidden" value="true">
          <img src="/images/on.jpg" width="61" height="23" id="recommendOn" onclick="recommendState();" style="cursor:pointer"> <img src="/images/off.jpg" width="61" height="23" id="recommendOff" onclick="recommendState();" style="cursor: pointer; display: none;"></span><span id="nothis"><strong class="q"></strong><strong class="w">推荐商品分类将会在首页楼层显示</strong><strong class="c"></strong></span></li>
    </ul>
    <ul class="set1" id="icon">
      <li>图标</li>
       <li> <span class="webSwitch">
       
       <input name="icon_type" id="type1" type="radio"  value="0" checked="checked"><label for="type1">系统图标</label>
    
       <input name="icon_type" id="type2" type="radio" value="1" ><label for="type2">图标上传</label>
           </span><span id="nothis"><strong class="q"></strong><strong class="w">商城分类图标只在一级分类旁显示，最佳尺寸为：18x20</strong><strong class="c"></strong></span></li>
    </ul>
     
    <ul class="set1" id="icon_sys_ul">
      <li>系统图标</li>
      <li><span class="webname">
       <div  class="icon_sys" >
       		<a href="javascript:void(0);" icon="" class="this"><img src="/images/default_icon.png"></a>
            <a href="javascript:void(0);" icon="1" class=""><img src="/images/icon_1.png"></a>
            <a href="javascript:void(0);" icon="2"><img src="/images/icon_2.png"></a>
            <a href="javascript:void(0);" icon="3"><img src="/images/icon_3.png"></a>
            <a href="javascript:void(0);" icon="4"><img src="/images/icon_4.png"></a>
            <a href="javascript:void(0);" icon="5"><img src="/images/icon_5.png"></a>
            <a href="javascript:void(0);" icon="6"><img src="/images/icon_6.png"></a>
            <a href="javascript:void(0);" icon="7"><img src="/images/icon_7.png"></a>
            <a href="javascript:void(0);" icon="8"><img src="/images/icon_8.png"></a>
            <a href="javascript:void(0);" icon="9"><img src="/images/icon_9.png"></a>
            <a href="javascript:void(0);" icon="10"><img src="/images/icon_10.png"></a>
            <a href="javascript:void(0);" icon="11"><img src="/images/icon_11.png"></a>
            <a href="javascript:void(0);" icon="12"><img src="/images/icon_12.png"></a>
            <a href="javascript:void(0);" icon="13"><img src="/images/icon_13.png"></a>
            <a href="javascript:void(0);" icon="14"><img src="/images/icon_14.png"></a>
            <a href="javascript:void(0);" icon="15"><img src="/images/icon_15.png"></a>
            <a href="javascript:void(0);" icon="16"><img src="/images/icon_16.png"></a>
            <a href="javascript:void(0);" icon="17"><img src="/images/icon_17.png"></a>
            <a href="javascript:void(0);" icon="18"><img src="/images/icon_18.png"></a>
            <a href="javascript:void(0);" icon="19"><img src="/images/icon_19.png"></a>
            <a href="javascript:void(0);" icon="20"><img src="/images/icon_20.png"></a>
            <a href="javascript:void(0);" icon="21"><img src="/images/icon_21.png"></a>
            <a href="javascript:void(0);" icon="22"><img src="/images/icon_22.png"></a>
            <a href="javascript:void(0);" icon="23"><img src="/images/icon_23.png"></a>
            <a href="javascript:void(0);" icon="24"><img src="/images/icon_24.png"></a>
            <a href="javascript:void(0);" icon="25"><img src="/images/icon_25.png"></a>
            <a href="javascript:void(0);" icon="26"><img src="/images/icon_26.png"></a>
            <a href="javascript:void(0);" icon="27"><img src="/images/icon_27.png"></a>
            <a href="javascript:void(0);" icon="28"><img src="/images/icon_28.png"></a>
            <a href="javascript:void(0);" icon="29"><img src="/images/icon_29.png"></a>
            <a href="javascript:void(0);" icon="30"><img src="/images/icon_30.png"></a>
            <a href="javascript:void(0);" icon="31"><img src="/images/icon_31.png"></a>
            <a href="javascript:void(0);" icon="32"><img src="/images/icon_32.png"></a>
            <a href="javascript:void(0);" icon="33"><img src="/images/icon_33.png"></a>
            <a href="javascript:void(0);" icon="34"><img src="/images/icon_34.png"></a>
            <a href="javascript:void(0);" icon="35"><img src="/images/icon_35.png"></a>
            <a href="javascript:void(0);" icon="36"><img src="/images/icon_36.png"></a>
            <a href="javascript:void(0);" icon="38"><img src="/images/icon_38.png"></a>
            <a href="javascript:void(0);" icon="39"><img src="/images/icon_39.png"></a>
            <a href="javascript:void(0);" icon="40"><img src="/images/icon_40.png"></a>
            
       </div>
      </span></li>
       <input name="icon_sys" type="hidden" id="icon_sys" value="">
    </ul>   
    <ul class="set1" id="icon_acc_ul" style="display:none">
        <li>图标上传</li>
        <li> <span class="size13">
          <input name="textfield" type="text" id="textfield1">
          </span> <span class="filebtn">
          <input name="button" type="button" id="button1" value="">
          </span> <span style="float:left;" class="file">
          <input name="icon_acc" type="file" id="icon_acc" class="file-text" size="30">
          </span> <span class="preview"> <img src="/images/preview.jpg" width="25" height="25" id="logoShow" style="cursor:help"> </span><span id="nothis"><strong class="q"></strong><strong class="w">暂无图标</strong><strong class="c"></strong></span>                  </li>
      </ul>   
       
    
    <ul class="set1">
      <li>排序</li>
      <li><span class="webname">
        <input name="sequence" type="text" id="sequence" value="">
      </span><span id="nothis"><strong class="q"></strong><strong class="w">序号越小，分类显示越靠前</strong><strong class="c"></strong></span></li>
    </ul>
    <ul class="set1">
      <li>SEO关键字</li>
      <li><span class="webname">
        <textarea name="seo_keywords" cols="40" rows="6" id="seo_keywords"></textarea>
      </span></li>
    </ul>
    <ul class="set1">
      <li>SEO描述</li>
      <li><span class="webname">
        <textarea name="seo_description" cols="40" rows="6" id="seo_description"></textarea>
      </span></li>
    </ul>
    </div>
  </form>
  <div class="submit">
    <input name="save" type="button" value="提交" onclick="saveForm()">
  </div>
</div>

</body>

<script type="text/javascript">



</script>



</html>