<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<tr style="background: #2A7AD2; height: 30px; color: #FFF">
	<td width="20%">订单号</td>
	<td width="16%">买家名称</td>
	<td width="20%">下单时间</td>
	<td width="15%">订单总额</td>
	<td width="10%">订单状态</td>
	<td align="center" >操作</td>
</tr>
<c:forEach items="${list }" var="tmp">
	<tr id="row+${tmp.oId }">
		<td >${tmp.orderId }</td>
		<td >${tmp.orderBuyername }</td>
		<td >
			<fmt:formatDate type="both" dateStyle="medium" timeStyle="medium" value="${tmp.orderAddtime }" />
		</td>
		<td >￥&nbsp;${tmp.orderTotalprice }&nbsp;元</td>
		<c:choose>
			<c:when test="${tmp.orderStatus==1 }">
				<td >待付款</td>
				<td align="center" class="blue xiahua">
				<a href="http://localhost:8080/bill/billDetail?id=20">查看</a>
					| <a href="http://localhost:8080/bill/cancelBill?id=20" onclick="if(confirm('确定取消用户订单?')==false)return true;">取消订单</a>
				</td>
			</c:when>
			<c:when test="${tmp.orderStatus==2 }">
				<td >待发货</td>
				<td align="center" class="blue xiahua">
					<a href="http://localhost:8080/bill/billDetail?id=2">查看</a>
				</td>
			</c:when>
			<c:when test="${tmp.orderStatus==3 }">
				<td >待收货</td>
				<td align="center" class="blue xiahua">
					<a href="http://localhost:8080/bill/billDetail?id=2">查看</a>
				</td>
			</c:when>
			<c:when test="${tmp.orderStatus==4 }">
				<td >待评价</td>
				<td align="center" class="blue xiahua">
					<a href="http://localhost:8080/bill/billDetail?id=2">查看</a>
				</td>
			</c:when>
			<c:when test="${tmp.orderStatus==5 }">
				<td >已取消</td>
				<td align="center" class="blue xiahua">
					<a href="http://localhost:8080/bill/billDetail?id=1">查看</a>
					| <a href="javascript:void(0);" onclick="ajax_deletebill(1)">删除订单</a>
				</td>
			</c:when>
			<c:when test="${tmp.orderStatus==6 }">
				<td >已完成</td>
				<td align="center" class="blue xiahua">
					<a href="http://localhost:8080/bill/billDetail?id=1">查看</a>
					| <a href="javascript:void(0);" onclick="ajax_deletebill(1)">删除订单</a>
				</td>
			</c:when>
		</c:choose>

	</tr>



</c:forEach>










