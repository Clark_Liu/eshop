<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>订单管理</title>
<link href="/css/template.css" rel="stylesheet" type="text/css">
<link href="/css/jquery-ui-1.8.22.custom.css" type="text/css" rel="stylesheet">
<script src="/js/jquery-1.6.2.js"></script>
<script src="/js/jquery.validate.min.js"></script>
<script src="/js/jquery.shop.common.js"></script>
<script src="/js/jquery.poshytip.min.js"></script>
<script src="/js/jquery-ui-1.8.21.js"></script>
<script language="javascript" type="text/javascript" src="/js/WdatePicker.js"></script>
<link href="/css/WdatePicker.css" rel="stylesheet" type="text/css">
<script>
$(document).ready(function(){
  $('#beginTime').datepicker({
	  dateFormat:"yy-mm-dd",
	  changeMonth: true,
	  changeYear: true
  });
  $('#endTime').datepicker({
	  dateFormat:"yy-mm-dd",
	  changeMonth: true,
	  changeYear: true
  });
  $("#stateselect").val("");
  
  
  var currentPage = $("#currentPage").val();
  if(currentPage ==""){
	  currentPage = 1;
	  $("#currentPage").val(1);
  }
  skipPage(currentPage);
  
});


function skipPage(currentPage){
	
	var state = $("#stateselect").val();
	$("#stateHidden").val(state);
	
	var billcode = $("#billcode").val();
	$("#billcodeHidden").val(billcode);
	
	var begintime = $("#begintime").val();
	$("#begintimeHidden").val(begintime);
	
	var endtime = $("#endtime").val();
	$("#endtimeHidden").val(endtime);
	
	var beginprice = $("#beginprice").val();
	$("#beginpriceHidden").val(beginprice);
	
	var endprice = $("#endprice").val();
	$("#endpriceHidden").val(endprice);
	
	  $.ajax({
		  url:"/orders/searchOrder",
		  type:"post",
		  async:true,
		  data:{
			stateHidden:$("#stateHidden").val(),
			billcodeHidden:$("#billcodeHidden").val(),
			begintimeHidden:$("#begintimeHidden").val(),
			endtimeHidden:$("#endtimeHidden").val(),
			beginpriceHidden:$("#beginpriceHidden").val(),
			endpriceHidden:$("#endpriceHidden").val(),
			currentPage:currentPage,
		  },
		  success:function(data){
			  $("#orderTable").empty();
			  $("#orderTable").append(data);
			  
			  $.ajax({
				url:"/SkipPageUtils/skipPageSearch",
				type:"post",
				async:true,
				data:{
					currentPage:currentPage
					},
				success:function(data){
					$("div .fenye").html(data);
				}
			  })
		  }
	  })
	
	
}




function ajaxChange(id){
	debugger
	if(!confirm("确定发货么")){
		return;
	}
	$.ajax({
		type:"Post",
		url:"/bill/ajaxUpdateBillState",
		dataType:"json",
		data:{"id" : id},
		success:function(){
			window.location.reload();
		}
  });
}

//删除单条记录
function ajax_deletebill(id){
	if(!confirm("确定删除么")){
		return;
	}
	$.ajax({
		type:"POST",
		url:"/bill/deleteBill",
		data:{"id":id,},
		success:function(){	
			window.location.reload();
		}
	});
}


</script>
<style id="poshytip-css-tip-skyblue" type="text/css">
div.tip-skyblue {visibility: hidden;position: absolute;top: 0;left: 0;}
div.tip-skyblue table,div.tip-skyblue td {margin: 0;font-family: inherit;font-size: inherit;font-weight: inherit;font-style: inherit;font-variant: inherit;}
div.tip-skyblue td.tip-bg-image span {display: block;font: 1px/1px sans-serif;height: 10px;width: 10px;overflow: hidden;}
div.tip-skyblue td.tip-right {background-position: 100% 0;}
div.tip-skyblue td.tip-bottom {background-position: 100% 100%;}
div.tip-skyblue td.tip-left {background-position: 0 100%;}
div.tip-skyblue div.tip-inner {background-position: -10px -10px;}
div.tip-skyblue div.tip-arrow {visibility: hidden;position: absolute;overflow: hidden;font: 1px/1px sans-serif;}
</style>
</head>
<body style="">

	<div class="cont">
		<h1 class="seth1">订单管理</h1>
		<div class="settab">
			<span class="tab-one"></span> <span class="tabs"> <a href="javascript:void(0);" class="this">所有订单</a></span> <span class="tab-two"></span>
		</div>
		<form id = "search" action="/orders/searchOrder" method="post" id="queryForm">
			<div class="orders">
				<ul>
					<li><span>订单状态</span><span class="ordersel">
					 <select name="state" id="stateselect">
						<option value="">所有订单</option>
						<option value="1">待付款</option>
						<option value="2">待发货</option>
						<option value="3">待收货</option>
						<option value="4">待评价</option>
						<option value="5">已取消</option>
						<option value="6">已完成</option>
					</select></span>
					<input type="hidden" id="stateHidden" name="stateHidden" value=""/>
					&nbsp;&nbsp;&nbsp;
					<span class="order_input size1">订单号</span>
						<span><input type="text" name="billcode" id="billcode" value="">
							<input type="hidden" id="billcodeHidden" name="billcodeHidden" value=""/>
					</span> 
					</li>
					<li>
					<span>下单时间</span>
					<span class="order_input size2"> 
					<input type="text" id="begintime" name="begintime" value="" class="Wdate" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'endtime\',{d:-1})}'})">
					<input type="hidden" id="begintimeHidden" name="begintimeHidden" value=""/>
					<!-- <input name="begintime" type="text" id="begintime" readonly="readonly" class="Wdate" onclick="WdatePicker()" value=""> -->
					</span>
					<span>—</span>
					<span class="order_input size2"> 
					<input type="text" id="endtime" name="endtime" value="" class="Wdate" onclick="WdatePicker({minDate:'#F{$dp.$D(\'begintime\',{d:1})}'})">
					<input type="hidden" id="endtimeHidden" name="endtimeHidden" value=""/>
					<!-- <input name="endtime" type="text" id="endtime" readonly="readonly" class="Wdate" onclick="WdatePicker()" value=""> -->
					&nbsp;&nbsp;&nbsp;
					</span> 
					<span>订单金额</span><span class="order_input size2"> 
						<input name="beginprice" type="text" id="beginprice" value="">
					<input type="hidden" id="beginpriceHidden" name="beginpriceHidden" value=""/>
					</span><span>—</span><span class="order_input size2"> 
						<input name="endprice" type="text" id="endprice" value="">
					<input type="hidden" id="endpriceHidden" name="endpriceHidden" value=""/>
					</span> <span class="btn_search"> 
						<input  type="button" value="搜索" onclick="skipPage(1)" style="cursor: pointer;">
					</span></li>
				</ul>
			</div>
		</form>
		
		
		
		<form action="http://localhost:8080/bill/manager" method="post" id="ListForm">
			<div class="allshop_table">
				<table id="orderTable" width="100%" border="0" cellspacing="0" cellpadding="0">
					<tbody>
						
						
						
						
					</tbody>
				</table>
			</div>
			<input type="hidden" id="currentPage" name="currentPage" value=""/>
	   	<div class="fenye" align="right" id="queryCondition">
	   	
	   	
       	</div>
		</form>
	</div>
	<div id="ui-datepicker-div" class="ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all"></div>

</body></html>