<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title></title>
<link href="/css/template.css" rel="stylesheet" type="text/css">
<script src="/js/jquery-1.6.2.js"></script>
<script src="/js/jquery.shop.common.js"></script>
<script src="/js/jquery.poshytip.min.js"></script>


<script>

//点击加号生成下级菜单
function addorsubtract(obj,id,parentId){
 var cls=$(obj).attr("cls");
 var level=$(obj).attr("level");
  if(cls=="jian"){
  $.ajax({type:'POST',
		       url:'/goodsclass/findByPid',
		        data:{"pid":id,
		        		"ppid":parentId
		        		},
				success:function(data){
	                $("#"+id).after(data);
					$(obj).attr("src","/images/add.jpg");
					$(obj).attr("cls","add");
					tipStyle();
              }
  });
  }else{
       if(level=="0_"+id){
	    $("tr[level=child_"+id+"]").remove();
	   }
       else
	   $("tr[parent="+id+"]").remove();
	   $(obj).attr("src","/images/jian.jpg");
	   $(obj).attr("cls","jian");
  }
}

//更新序号和名称
function ajax_update(id,fieldName,obj){
   var val=$(obj).val();
   var flg = 0;
   if(val == ""){
	   val = $(obj).attr("valb");
	   flg = 1;
   }
  		 $.ajax({type:'POST',
	              url:'/goodsclass/updateClass',
				  data:{"id":id,
					  "fieldName":fieldName,
					  "value":val},
				beforeSend:function(){
				  
				},
			   success:function(data){
				   
	             if(flg = 1){
				   $(obj).attr("src","/images/"+data+".png");
				   $(obj).attr("valb",data);
				 }else{
				   $(obj).val(val);
				 }      
              }
	    }); 
}



//单个删除
function ajax_delete(id,fieldName,obj){
	if(confirm("删除分类会同时删除该分类的所有下级，是否继续?")){
	    var objp = $(obj).parent().parent();
	    level = $(objp).attr("levels");
	    val = $(obj).attr("valb");
	    
	    
		$.ajax({type:'POST',
			url:'/goodsclass/updateClass',
			  data:{"id":id,
				  "level":level,
				  "fieldName":fieldName,
				  "value":val},
					async:true,
				  		success:function(){
					   $("tr[id=" + id +"]").remove();
					   if(level=="0"){
						    $("tr[level=child_"+id+"]").remove();
						}else if(level == '1'){
							$("tr[parent="+id+"]").remove();
						}
	              }
		    });
		}
}

//选择批量修改(推荐/删除)
function ajax_updatebatch(fieldName,obj){
	if(confirm("确定要执行此批量操作吗?")){
	//复选框的集合
	var ckbList = $("input[name='ids']");
	//id的集合
	var ckbId = [];
	//勾选的复选框的集合
	var ckbed = [];
	//勾选的复选框的级别的集合
	var ckbed_level = [];
	 for(var index in ckbList){
		 //ckbList中有其他不相关元素,排除掉
		if(index < ckbList.length){
			//勾选的返回true
			if($(ckbList[index]).prop("checked")){
				//把勾选的id push 进数组中
				ckbId.push($(ckbList[index]).attr("value"));
				//把勾选的选框 push 进数组中
				ckbed.push($(ckbList[index]));
				//把勾选的选框的level push 进数组中
				ckbed_level.push($(ckbList[index]).parent().parent().attr("levelS"));
			}
		}
	};
	$.ajax({
		url:'/goodsclass/updateClassBatch',
		type:'post',
		async:false,
		traditional:true,
		dataType:'text',
		data:{
			"ckbId":ckbId,
			"ckbed_level":ckbed_level,
			"fieldName":fieldName
		},
		success:function(){
			if(fieldName == "recommend"){
				for(var index in ckbed){
					var img = $(ckbed[index]).parent().parent().children("#recommend").children("#recommendFlg")[0];
					$(img).attr("src","/images/true.png");
					$(img).attr("valb","true");
				}
			}else if(fieldName == "deletestatus"){
				for(var index in ckbed){
					
					var tr = $(ckbed[index]).parent().parent();
					var level = tr.attr("levels");
					var id = tr.attr("id");
				
					tr.remove();
				    if(level=="0"){
					    $("tr[level=child_"+id+"]").remove();
					}else if(level == '1'){
						$("tr[parent="+id+"]").remove();
					}
				}
			}
			for(var index in ckbed){
				$(ckbed[index]).prop("checked",false);
			}
			$("#all").prop("checked",false);
		}
	});
	}
}














$(document).ready(function(){
 
});
</script><style id="poshytip-css-tip-skyblue" type="text/css">div.tip-skyblue{visibility:hidden;position:absolute;top:0;left:0;}div.tip-skyblue table, div.tip-skyblue td{margin:0;font-family:inherit;font-size:inherit;font-weight:inherit;font-style:inherit;font-variant:inherit;}div.tip-skyblue td.tip-bg-image span{display:block;font:1px/1px sans-serif;height:10px;width:10px;overflow:hidden;}div.tip-skyblue td.tip-right{background-position:100% 0;}div.tip-skyblue td.tip-bottom{background-position:100% 100%;}div.tip-skyblue td.tip-left{background-position:0 100%;}div.tip-skyblue div.tip-inner{background-position:-10px -10px;}div.tip-skyblue div.tip-arrow{visibility:hidden;position:absolute;overflow:hidden;font:1px/1px sans-serif;}</style></head>





<body>
<div class="cont">
  <h1 class="seth1">商品分类</h1>
  <div class="settab"><span class="tab-one"></span> <span class="tabs"> <a href="/dispatcher/dispatch?dispatch_url=category.jsp?currentPage=1" class="this">管理</a> | <a href="/dispatcher/dispatch?dispatch_url=category_addPage.jsp">新增</a></span> <span class="tab-two"></span></div>
   <div class="operation">
    <h3>友情提示</h3>
    <ul>
      <li>通过商品分类管理，你可以进行查看、编辑、删除系统商品分类</li>
      <li>你可以根据需要控制商品分类是否显示</li>
    </ul>
  </div> 
  <div class="classtable">
  <form name="ListForm" id="ListForm" action="http://y14u504114.imwork.net:12111/shopping/admin/goods_class_list.htm" method="post">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="class_table">
      <tbody>
    </tbody></table>
	  <input type="hidden" name="currentPage" id="currentPage" value="1">
	  <input name="mulitId" type="hidden" id="mulitId">
	  <div class="fenye"> 
	<!-- <a href="http://y14u504114.imwork.net:12111/shopping/admin/goods_class_list.htm?currentPage=1">首页</a>
	   第　<a class="this" href="http://y14u504114.imwork.net:12111/shopping/admin/goods_class_list.htm?currentPage=1">1</a> 页　
	   <a href="http://y14u504114.imwork.net:12111/shopping/admin/goods_class_list.htm?currentPage=1">末页</a> --> 
	  </div>	
   </form>
  </div>  
</div>


<div class="tip-skyblue">
<div class="tip-inner tip-bg-image">
</div>
<div class="tip-arrow">
</div>
</div>
<div class="tip-skyblue">
<div class="tip-inner tip-bg-image"></div>
<div class="tip-arrow">
</div>
</div>
<div class="tip-skyblue">
<div class="tip-inner tip-bg-image">
</div><div class="tip-arrow">
</div></div><div class="tip-skyblue">
<div class="tip-inner tip-bg-image"></div>
<div class="tip-arrow"></div></div><div class="tip-skyblue">
<div class="tip-inner tip-bg-image"></div><div class="tip-arrow">
</div></div><div class="tip-skyblue"><div class="tip-inner tip-bg-image">
</div><div class="tip-arrow"></div></div><div class="tip-skyblue">
<div class="tip-inner tip-bg-image"></div><div class="tip-arrow">
</div></div><div class="tip-skyblue"><div class="tip-inner tip-bg-image"></div>
<div class="tip-arrow"></div></div><div class="tip-skyblue"><div class="tip-inner tip-bg-image">
</div><div class="tip-arrow"></div></div><div class="tip-skyblue"><div class="tip-inner tip-bg-image">
</div><div class="tip-arrow"></div></div><div class="tip-skyblue"><div class="tip-inner tip-bg-image">
</div><div class="tip-arrow"></div></div><div class="tip-skyblue"><div class="tip-inner tip-bg-image">
</div><div class="tip-arrow"></div></div><div class="tip-skyblue"><div class="tip-inner tip-bg-image">
</div><div class="tip-arrow"></div></div><div class="tip-skyblue"><div class="tip-inner tip-bg-image">
</div><div class="tip-arrow"></div></div><div class="tip-skyblue"><div class="tip-inner tip-bg-image">
</div><div class="tip-arrow"></div></div><div class="tip-skyblue"><div class="tip-inner tip-bg-image">
</div><div class="tip-arrow"></div></div><div class="tip-skyblue"><div class="tip-inner tip-bg-image">
</div><div class="tip-arrow"></div></div><div class="tip-skyblue"><div class="tip-inner tip-bg-image">
</div><div class="tip-arrow"></div></div><div class="tip-skyblue"><div class="tip-inner tip-bg-image">
</div><div class="tip-arrow"></div></div><div class="tip-skyblue"><div class="tip-inner tip-bg-image">
</div><div class="tip-arrow"></div></div><div class="tip-skyblue"><div class="tip-inner tip-bg-image">
</div><div class="tip-arrow"></div></div><div class="tip-skyblue"><div class="tip-inner tip-bg-image">
</div><div class="tip-arrow"></div></div><div class="tip-skyblue"><div class="tip-inner tip-bg-image">
</div><div class="tip-arrow"></div></div><div class="tip-skyblue"><div class="tip-inner tip-bg-image">
</div><div class="tip-arrow"></div></div>
</body>
<script>
	$(function(){
		
		if(<%=request.getParameter("currentPage")%> != null){
			currentPage = "<%=request.getParameter("currentPage")%>";
		}else{
			currentPage = 1;
		}
		
		$.ajax({
			url:'/goodsclass/findAll',
			type:'post',
			async:true,
			data:{
				"currentPage":currentPage
			},
			success:function(data){
				$("tbody").html(data);
				$.ajax({
					url:'/SkipPageUtils/skipPage',
					type:'post',
					async:true,
					data:{
						"currentPage":currentPage,
						"url":"category.jsp"
					},
					success:function(data){
						$("div .fenye").html(data);
					}
					
				}) 
			}
		})
		
		 
		
		
		
		
		
		
		
	})
</script>

</html>