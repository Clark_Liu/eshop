<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

    <!-- 表头 -->
    <tr style="background:#2A7AD2; height:30px;color:#FFF">
        <td width="38">&nbsp;</td>
        <td width="70">排序</td>
        <td width="450" align="left">分类名称</td>
        <td width="99" align="center">类型</td>
        <td width="52" align="center">显示</td>
        <td width="55" align="center">推荐</td>
        <td width="126" align="left">操作</td>
      </tr>
    
    <c:forEach items="${goodsdata }" var="tmp1">
    
    	<c:if test="${!tmp1.deletestatus }">
    	
    	
	    	<c:if test="${tmp1.level==0 }">
	    	
			 <tr id="${tmp1.id }" levels = "${tmp1.level }">
			   <td align="center">
			   		<input name="ids" id="ids" type="checkbox" value="${tmp1.id }">
			   </td>
			   <td colspan="2" align="center">
			   <ul class="addclass">
				     <li class="ac1">
				        <input name="addorsubtract_${tmp1.id }" id="addorsubtract_${tmp1.id }" type="hidden" value="true">
				        <img src="/images/jian.jpg" cls="jian" level="${tmp1.level }_${tmp1.id }" width="14" height="14" id="jian_${tmp11.id }" onclick="addorsubtract(this,${tmp1.id },${tmp1.id });" style="cursor:pointer;"> 
				     </li>
					<li class="ac2">
						<span class="num">
					  		<input type="text" name="0" id="0" value="${tmp1.sequence }" onchange="ajax_update(&#39;${tmp1.id }&#39;,&#39;sequence&#39;,this)" title="可编辑">
						</span>
					</li>
					<li class="ac3">
						<span class="classies">
				 			<input type="text" name="${tmp1.id }" id="${tmp1.id }" value="${tmp1.classname }" onchange="ajax_update(&#39;${tmp1.id }&#39;,&#39;className&#39;,this)" title="">
				 		</span> 
				 		<span class="newclass">
				 			<a href="/dispatcher/dispatch?dispatch_url=category_addNextClassPage.jsp?pid=${tmp1.id }">新增下级</a>
				 		</span>
				 	</li>
				</ul>
				</td>
			<td align="center">${tmp1.typeName }</td>
			<c:if test="${tmp1.display }">
			<td align="center" id="display"><img id="displayFlg" src="/images/true.png" width="21" height="23" onclick="ajax_update(&#39;${tmp1.id }&#39;,&#39;display&#39;,this)" style="cursor:pointer;" title="可编辑" valb="true"></td>
			</c:if>
			<c:if test="${!tmp1.display }">
			<td align="center" id="display"><img id="displayFlg" src="/images/false.png" width="21" height="23" onclick="ajax_update(&#39;${tmp1.id }&#39;,&#39;display&#39;,this)" style="cursor:pointer;" title="可编辑" valb="false"></td>
			</c:if>
			<c:if test="${tmp1.recommend }">
			<td align="center" id="recommend"><img id="recommendFlg" src="/images/true.png" width="21" height="23" onclick="ajax_update(&#39;${tmp1.id }&#39;,&#39;recommend&#39;,this)" style="cursor:pointer;" title="推荐后会在首页楼层显示" valb="true"></td>
			</c:if>
			<c:if test="${!tmp1.recommend }">
			<td align="center" id="recommend"> <img id="recommendFlg" src="/images/false.png" width="21" height="23" onclick="ajax_update(&#39;${tmp1.id }&#39;,&#39;recommend&#39;,this)" style="cursor:pointer;" title="推荐后会在首页楼层显示" valb="false"></td>
			</c:if>
			<td align="left" class="ac8"><a href="/goodsclass/selectClassSimple?id=${tmp1.id } ">编辑</a>|<a href="javascript:void(0);" onclick="ajax_delete('${tmp1.id }','deletestatus',this)" valb="true">删除</a></td>
			</tr>
			</c:if>
		</c:if>
		
		<%-- 
		
		<c:forEach items="${goodsdata }" var="tmp2">
		
		<c:if test="${tmp2.level==1 }">
		<c:if test="${tmp2.parentId==tmp1.id }">
	
		
		
		<tr id="${tmp2.id }" parent="${tmp1.id }" level="child_${tmp1.id }">
 			<td align="center"><input name="ids" id="ids" type="checkbox" value="${tmp2.id }"></td>
  		  <td colspan="2" align="center">
	 			<ul class="addclass">
			   <li class="acc1"><img src="./jian.jpg" cls="jian" width="14" height="14" onclick="addorsubtract(this,${tmp2.id })" style="cursor:pointer;"></li>
			   <li class="ac2"><span class="num"><input type="text" name="0" id="0" value="0" onchange="ajax_update(&#39;${tmp2.id }&#39;,&#39;sequence&#39;,this)" title="可编辑"></span></li>
			   <li class="acc3"><span class="classies"><input type="text" name="${tmp2.classname }" id="${tmp2.classname }" value="${tmp2.classname }" onchange="ajax_update(&#39;${tmp2.id }&#39;,&#39;className&#39;,this)" title="可编辑"></span>
			   <span class="newclass" style="$sty"><a href="http://localhost:8080/admin/goods_class_add.htm?pid=${tmp2.id }">新增下级</a></span></li>
			       </ul>    
		  </td>
		    <td align="center"></td>
		    <td align="center"><img src="./true.png" width="21" height="23" onclick="ajax_update(&#39;${tmp2.id }&#39;,&#39;display&#39;,this)" style="cursor:pointer;" title="可编辑"></td>
		    <td align="center"><img src="./true.png" width="21" height="23" onclick="ajax_update(&#39;${tmp2.id }&#39;,&#39;recommend&#39;,this)" style="cursor:pointer;" title="可编辑"></td>
		    <td align="left" class="ac8"><a href="http://localhost:8080/admin/goods_class_edit.htm?id=${tmp2.id }&amp;currentPage=1">编辑</a>|<a href="javascript:void(0);" onclick="if(confirm(&#39;删除分类会同时删除该分类的所有下级，是否继续?&#39;))window.location.href=&#39;http://localhost:8080/admin/goods_class_del.htm?mulitId=${tmp2.id }&amp;currentPage=1&#39;">删除</a></td>
		</tr>
			</c:if>
		
		
		</c:if>
		
		
		
		<c:forEach items="${goodsdata }" var="tmp3">
		
		<c:if test="${tmp3.level==2 }">
		<c:if test="${tmp3.parentId==tmp2.id }">
	
		
		
		<tr id="${tmp3.id }" parent="${tmp3.id }" level="child_${tmp2.id }">
 			<td align="center"><input name="ids" id="ids" type="checkbox" value="${tmp3.id }"></td>
  		  <td colspan="2" align="center">
	 			<ul class="addclass">
			   <li class="acc1"><img src="./jian.jpg" cls="jian" width="14" height="14" onclick="addorsubtract(this,${tmp3.id })" style="cursor:pointer;"></li>
			   <li class="ac2"><span class="num"><input type="text" name="0" id="0" value="0" onchange="ajax_update(&#39;${tmp3.id }&#39;,&#39;sequence&#39;,this)" title="可编辑"></span></li>
			   <li class="acc3"><span class="classies"><input type="text" name="${tmp3.classname }" id="${tmp3.classname }" value="${tmp3.classname }" onchange="ajax_update(&#39;${tmp3.id }&#39;,&#39;className&#39;,this)" title="可编辑"></span>
			   <span class="newclass" style="$sty"><a href="http://localhost:8080/admin/goods_class_add.htm?pid=${tmp3.id }">新增下级</a></span></li>
			       </ul>    
		  </td>
		    <td align="center"></td>
		    <td align="center"><img src="./true.png" width="21" height="23" onclick="ajax_update(&#39;${tmp3.id }&#39;,&#39;display&#39;,this)" style="cursor:pointer;" title="可编辑"></td>
		    <td align="center"><img src="./true.png" width="21" height="23" onclick="ajax_update(&#39;${tmp3.id }&#39;,&#39;recommend&#39;,this)" style="cursor:pointer;" title="可编辑"></td>
		    <td align="left" class="ac8"><a href="http://localhost:8080/admin/goods_class_edit.htm?id=${tmp3.id }&amp;currentPage=1">编辑</a>|<a href="javascript:void(0);" onclick="if(confirm(&#39;删除分类会同时删除该分类的所有下级，是否继续?&#39;))window.location.href=&#39;http://localhost:8080/admin/goods_class_del.htm?mulitId=${tmp3.id }&amp;currentPage=1&#39;">删除</a></td>
		</tr>
			</c:if>
		
		
		</c:if>
		
		
		</c:forEach>
		
		</c:forEach> --%>
		
		
    </c:forEach>
    
    
    
    <tr style="background:#F2F2F2; height:30px;">
		<td align="center"><input type="checkbox" name="all" id="all" value="" onclick="selectAll(this)"></td>
        <td colspan="2" align="center" class="sall"><span class="classall">全部</span><span class="shop_btn_del" id="alldel">
          <input name="input" type="button" value="删除" style="cursor:pointer;" onclick="ajax_updatebatch('deletestatus',this)">
          </span><span class="shop_btn_del" id="alldel">
            <input name="input" type="button" value="推荐" style="cursor:pointer;" onclick="ajax_updatebatch('recommend',this)">
          </span></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
	</tr>
	
	
	
	
	
	
	
	