<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>广告添加</title>
<link href="/css/template.css" rel="stylesheet" type="text/css">
<script src="/js/jquery-1.6.2.js"></script>
<script src="/js/jquery.validate.min.js"></script>
<script>
jQuery(document).ready(function(){
	var style=$("#style").val();
	var flg=$("#rec").val();
	alert(flg);
 	//改变系统提示的样式
  jQuery("span .w").mousemove(function(){
	var id=jQuery(this.parentNode).attr("id");
	if(id="nothis"){
	   jQuery(this.parentNode).attr("id","this")
	}
  }).mouseout(function(){
     var id=jQuery(this.parentNode).attr("id");
	 if(id="this"){
	   jQuery(this.parentNode).attr("id","nothis")
	 }
  });
//设置显示的默认装态
	//alert("设置开关样式");
	var rec = jQuery("#yesnoshow1").val();
	alert(rec);
	if(rec=="true"){
		jQuery("#stateOn").show();
		jQuery("#stateOff").hide();
	}else{
		jQuery("#stateOn").hide();
		jQuery("#stateOff").show();
	}
//表单验证
if(style=='add'){
jQuery("#theForm").validate({
    rules:{
    	advername:{
  	    required :true,
	     remote:{
			    url: "/advert/theform",     //后台处理程序
	            type: "post",               //数据发送方式
	            dataType: "json",           //接受数据格式   
	            data: {                     //要传递的数据
	                  "advername": function(){return jQuery("#advername").val();},
					  "id":function(){return jQuery("#id").val()}
			     }
				}
  	  	},
    	url:{
    		required :true,
    	},
    	seq:{
    		required :true,
    		maxlength:2
    	},
    	file:{
    		required :true,
    		accept:"jpg|png|gif"
    	},
    },
	messages:{
	advername:{required:"广告名称不能为空",remote:"该广告已经存在"},
	url:{required:"广告链接不能为空"},
	seq:{required:"广告排序不能为空"},
	file:{required:"广告图片不能为空",accept:"系统不允许类型"}
	}
  });
}
if(style=='eidt'){
	jQuery("#theForm").validate({
	    rules:{
	    	advername:{
	  	    required :true,
		     remote:{
				    url: "/advert/theform",     //后台处理程序
		            type: "post",               //数据发送方式
		            dataType: "json",           //接受数据格式   
		            data: {                     //要传递的数据
		                  "advername": function(){return jQuery("#advername").val();},
						  "id":function(){return jQuery("#id").val()}
				     }
					}
	  	  	},
	    	url:{
	    		required :true,
	    	},
	    	seq:{
	    		required :true,
	    		maxlength:2
	    	},
	    },
		messages:{
		advername:{required:"广告名称不能为空",remote:"该广告已经存在"},
		url:{required:"广告链接不能为空"},
		seq:{required:"广告排序不能为空"},
		}
	  });
}
//标志图片鼠标经过显示
	jQuery("#adverImgShow").mouseover(function(){
		jQuery("#adverImg").show();
	})
	jQuery("#adverImgShow").mouseout(function(){
		jQuery("#adverImg").hide();
	})
	jQuery("#adverLogo").change(function(){
		jQuery("#textfield1").val(jQuery("#adverLogo").val());
	})
});
//修改显示状态
function yesnoshowState(){
	var state = jQuery("#yesnoshow1").val();
	if(state=="true"){
		jQuery("#yesnoshow1").val("false");
		jQuery("#stateOff").show();
		jQuery("#stateOn").hide();
	}else{
		jQuery("#yesnoshow1").val("true");
		jQuery("#stateOff").hide();
		jQuery("#stateOn").show();
	}
}
//jq表单提交
function saveBrand(method){
	jQuery("#cmd").val(method);
	$("#theForm").submit();
}
</script>
</head>
<body style="">
	<form action="/advert/add" method="post" enctype="multipart/form-data" name="theForm" id="theForm">
		<input name="id" id="id" type="hidden" value="${advert_edit.id }">
		<input name="style" id="style" type="hidden" value="${style }">
		<input type="text" value="${advert_edit.recommend }"/>
		<div class="cont">
			<h1 class="seth1">广告管理</h1>
			<div class="settab">
				<span class="tab-one"></span> <span class="tabs"> 
				<a href="/dispatcher/dispatch?dispatch_url=/shopping_advert_list.jsp">管理</a> | 
				<c:if test="${style=='add' }">
				<a href="/dispatcher/dispatch?dispatch_url=/shopping_advert_add.jsp" class="this">新增</a>
				</c:if>
				<c:if test="${style=='eidt' }">
				<a href="javascript:void(0);" class="this">编辑</a>
				</c:if>
				</span> <span class="tab-two"></span>
			</div>
			<div class="setcont" id="base">
				<!--鼠标经过样式-->
				<ul class="set1">
					<li><strong class="orange fontsize20">*</strong>广告名称</li>
					<li><span class="webname">
					<input type="text" name="advername" id="advername" placeholder="请输入广告名称" value="${advert_edit.adTitle }">
					</span></li>
				</ul>
				<ul class="set1">
					<li><strong class="orange fontsize20">*</strong>广告图片</li>
					<li><span class="size13"> <input name="textfield"
							type="text" id="textfield1">
					</span> <span class="filebtn"> <input name="button" type="button"
							id="button1" value="浏览">
					</span> <span style="float: left;" class="file"> <input type="file"
							name="file" id="adverLogo" size="30">
					</span> <span class="preview"></span> <span id="nothis"><strong
							class="q"></strong><strong class="w">最佳尺寸93*33，支持格式gif,jpg,jpeg,png</strong><strong
							class="c"></strong></span></li>
				</ul>

				<ul class="set1">
					<li><strong class="orange fontsize20">*</strong>广告链接</li>
					<li><span class="webname">
						<input type="text" name="url" id="url" placeholder="请输入广告链接" value="${advert_edit.adUrl }">
					</span></li>
				</ul>
				<ul class="set1">
					<li><strong class="orange fontsize20">*</strong>是否显示</li>
					<li>
						<span class="webSwitch">
						<input name="yesnoshow1" id="yesnoshow1" type="hidden" value="${advert_edit.recommend }">
						<img src="/images/on.jpg" width="61" height="23" id="stateOn" onclick="yesnoshowState();" style="cursor: pointer"/>
						<img src="/images/off.jpg" width="61" height="23" id="stateOff" onclick="yesnoshowState();" style="cursor: pointer"/>
						</span>
					<span id="nothis"><strong class="q"></strong><strong class="w">推荐品牌将在首页轮换显示</strong><strong class="c"></strong>
					</span></li>
				</ul>

				<ul class="set1">
					<li><strong class="orange fontsize20">*</strong>排序</li>
					<li><span class="webname">
					<input name="seq" type="text" id="seq" value="${advert_edit.sequence }"
							onkeyup="this.value=this.value.replace(/\D/g,&#39;&#39;)"
							onafterpaste="this.value=this.value.replace(/\D/g,&#39;&#39;)">
					</span> <span id="nothis"> <strong class="q"></strong><strong
							class="w">序号越小显示越靠前</strong><strong class="c"></strong>
					</span></li>
				</ul>
			</div>
		</div>
  <div class="submit">
    <input name="" type="button" value="提交" onclick="saveBrand('save');"/>
  </div>
	</form>
</body>
</html>