<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<c:forEach items="${goodsdata }" var="tmp">
	<tr>
		<td align="center"><input name="id" type="checkbox" id="${tmp.id }" value="${tmp.sequence }"></td>
	    <td>
	    	<span class="pxnum size5">
	        	<input type="text" name="${tmp.sequence }" class="sequence" id="${tmp.sequence }" value="${tmp.sequence }" onblur="ajax_update(&#39;7&#39;,&#39;sequence&#39;,this)" title="可编辑">
	    	</span>
	    </td>
	  	<td>
	  		<span class="pxnum size7">
	    		<input name="${tmp.name }" class="name" type="text" id="${tmp.name }" onblur="ajax_update(&#39;7&#39;,&#39;name&#39;,this)" value="${tmp.name }" title="可编辑">
	    	</span>
	    </td>
	  	<td align="center" class="ac8">
	  		<a href="" class="edit" id="${tmp.id }" >编辑</a>
	  		|
	  		<a href="/dispatcher/dispatch?dispatch_url=goods_type_list.jsp" class="del" mulitId="${tmp.id }" >删除</a>
	  	</td>
	</tr>
</c:forEach>
		<tr>
          <td align="center"><input type="checkbox" name="all" id="all" value="" onclick="selectAll(this)">
            <input name="mulitId" type="hidden" id="mulitId">
            <input name="currentPage" type="hidden" id="currentPage" value="1"></td>
          <td colspan="2" class="bdel"><span class="sp1">全部</span><span class="sp2">
            <div class="shop_btn_del shopbtn">
              <input name="Input" type="button" value="删除" onclick="ajax_updatebatch('deletestatus',this)" style="cursor:pointer;">
            </div>
            </span></td>
          <td>&nbsp;</td>
        </tr>
 <script type="text/javascript">
 	$("a[class=edit]").click(function(){
 		var id = $(this).attr("id");
 		var name = $(this).parent().parent().children().children().children("input[class=name]").val();
 		var sequence =  $(this).parent().parent().children().children().children("input[class=sequence]").val();
 		$(this).attr('href',encodeURI('/dispatcher/dispatch?dispatch_url=goods_type_edit.html&id='+id+'&name='+name+'&sequence='+sequence)); 
//    		window.location.href="/dispatcher/dispatch?dispatch_url=goods_type_edit.html?id="+id+"&name="+name+"&sequence="+sequence;
 	});
 	$("a[class=del]").click(function(){//单删
 		var mulitId = $(this).attr("mulitId");
 		$.ajax({
 			url:'/goodstype/deleteone?id='+mulitId,
 			type:'post',
 			async:false,
 			data:{},
 			success:function(data){
 				window.location.reload();//刷新当前页面
            }
 		});
 		
 	});
 	$("input[type=button]").click(function(){//多删
 		var len = $("input[type='checkbox']:checked").length;
 		if(len == 0)
 		{
 			alert("至少选择一条数据记录");
 		}
 		else
 		{
 			var a = window.confirm("确定执行该操作？");
 	 		if(a==true)
 	 		{
 	 	 		var ch;
 	 			$.each($('input[type=checkbox]:checked'),function(i){
 	 	 			ch =$(this).attr("id");
 	 	 			$.ajax({
 	 	 	 			url:'/goodstype/deleteone?id='+ch,
 	 	 	 			type:'post',
 	 	 	 			async:false,
 	 	 	 			data:{},
 	 	 	 			success:function(data){
 	 	 	            }
 	 	 	 		});
 	 	 			window.location.reload();//刷新当前页面
 	 	 		});
 	 		}
 	 		else
 	 		{
 	 			
 	 		}
 		}
 	});
</script>