<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>添加分类</title>

<style type="text/css">
#na{width:325px; text-align:center; display:block;}
#va{width:300px; text-align:center; display:block;}
#warning{width:300px; text-align:center; display:block;}
#del{ width:120px; text-align:center;display:block;} 
</style>

<link href="/css/template.css" rel="stylesheet" type="text/css">
<link href="/css/public.css" type="text/css" rel="stylesheet">
<link href="/css/goods.css" type="text/css" rel="stylesheet">
<script src="/js/jquery-1.6.2.js"></script>
<script src="/js/jquery.validate.min.js"></script>
<style>.cke{visibility:hidden;}</style>


<script type="text/javascript" src="/ckeditor4.1/ckeditor.js"></script>


<script type="text/javascript">

    
    
    
//设置默认状态以及判断初始显示状态
$(document).ready(function(){
	
	
	
	//生成品牌的下拉列表
	$.ajax({
		url:"/brand/findAllSelected",
		type:"post",
		async:false,
		data:{},
		success:function(data){
			var list = JSON.parse(data).list;
			for(var i in list){
				var ipt = $("<option value="+list[i].id+">"+list[i].name+"</option>");
				$("#brandId").append(ipt);
			}
		}
	});
	
	

	//分类下拉列表
	$.ajax({
		url:'/goodsclass/findSelectClass',
		type:'post',
		async:true,
		data:{},
		success:function(data){
			var sel = $("#sortId");
			var list = JSON.parse(data).list;
			for(var index in list){
				var opt = $("<option value="+ list[index].id +" level="+ list[index].level+">"+ list[index].classname +"</option>");
				sel.append(opt);
				var list2 = list[index].childList;
				for(var index2 in list2){
					var opt2 = $("<option value="+ list2[index2].id +" level="+ list2[index2].level+">" + "&nbsp;&nbsp;&nbsp;&nbsp;"+list2[index2].classname +"</option>");
					sel.append(opt2);
					var list3 = list2[index2].childList;
					for(var index3 in list3){
						var opt3 = $("<option value="+ list3[index3].id +" level="+ list3[index3].level+">" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+list3[index3].classname +"</option>");
						sel.append(opt3);
					}
				}
			}
		}
	});
	
	//规格下拉列表
	$.ajax({
		url:"/goodsspec/findAll",
		type:"post",
		async:true,
		data:{},
		success:function(data){
			var list = JSON.parse(data).list;
			var sel1 = $("#specnameId1");
			var sel2 = $("#specnameId2");
			for(var index in list){
				var opt= $("<option value="+ list[index].id +">"+ list[index].name+"</option>");
				var opt2= $("<option value="+ list[index].id +">"+ list[index].name+"</option>");
				sel1.append(opt);
				sel2.append(opt2);
			}
		}
	})
	
	
	//判断规格值是否相同
	$("#specnameId2").blur(function(){
		var nn1 = $("#specnameId1").val();
		var nn2 = $("#specnameId2").val();
		if((nn1 == nn2) && (nn1 != 0)){
			alert("11111");
			$("#specnameId2 option[value='bb']").attr("selected","selected");
			var sel = $("#specvalueId2");
			sel.empty();
			sel.append("<option value='' selected='selected'>请选择规格值</option>");
			
		}
	});
	$("#specnameId1").blur(function(){
		var nn1 = $("#specnameId1").val();
		var nn2 = $("#specnameId2").val();
		if((nn1 == nn2) && (nn1 != 0)){
			alert("22222");
			$("#specnameId1 option[value='aa']").attr("selected","selected");
			var sel = $("#specvalueId1");
			sel.empty();
			sel.append("<option value='' selected='selected'>请选择规格值</option>");
			
		}
	}); 
	
	
	//判断库存为非负整数
	$.validator.addMethod("verify",function(value,element){
		  var re = /^\d+$/;
		  if(re.test($("#stock").val())){			
		  	return true;
		  }else {
	      	return false;
		  }		  
	});
	//判断进价为非负浮点数
	$.validator.addMethod("num1",function(value,element){
		  var re = /^\d+(\.\d+)?$/;
		  if(re.test($("#buyprice").val())){			
		  	return true;
		  }else {
	      	return false;
		  }		  
	});
	//判断原价为非负浮点数
	$.validator.addMethod("num2",function(value,element){
		  var re = /^\d+(\.\d+)?$/;
		  if(re.test($("#originalprice").val())){			
		  	return true;
		  }else {
	      	return false;
		  }		  
	});
	//判断标价为非负浮点数
	$.validator.addMethod("num3",function(value,element){
		  var re = /^\d+(\.\d+)?$/;
		  if(re.test($("#sellprice").val())){			
		  	return true;
		  }else {
	      	return false;
		  }		  
	});
	//判断重量为非负浮点数
	$.validator.addMethod("num4",function(value,element){
		  var re = /^\d+(\.\d+)?$/;
		  if(re.test($("#weight").val())){			
		  	return true;
		  }else {
	      	return false;
		  }		  
	});
	
	
 	$("#theForm").validate({
		  rules:{
			goodsname:{
			  required:true,
	          remote:{
			    url: "/goods/checkGoodName",     //后台处理程序
	            type: "post",               //数据发送方式
	            data: {                     //要传递的数据
	                  "goodsname": function(){return $("#goodsname").val();}
			    }
			  }
			},
			artnumber:{
				required:true,
				remote:{
					url:"/goods/checkArtnumber",
					type:"post",
					data:{
						"artnumber": function(){return $("#artnumber").val();}
					}
				}
			 },
			stock:{
				required:true,
				verify:true 
			},  
			buyprice:{
			  required:true,
			  num1:true 
			},
			originalprice:{
				required:true,
				num2:true 
			},
			sellprice:{
				required:true,
				num3:true 
			},
			brandId:{
				required:true,
			},
			sortId:{
				required:true,
				remote:{
				    url: "/goods/checkSort",     //后台处理程序
		            type: "post",               //数据发送方式
		            data: {                     //要传递的数据
		                  "sortId": function(){return $("#sortId").val();}
				    }
				  }
			},specvalueId:{
				required:true,
			}
		},
		  messages:{
			 goodsname:{
			  	required:"商品名称不能为空",
			  	remote:"商品名称已存在"
			},
			artnumber:{
				required:"货号不能为空",
				remote:"货号已存在"
			},
			stock:{
				required:"库存不能为空",
				verify:"库存必须是正整数或者0" 
			},
			buyprice:{
			  	required:"进价不能为空",
			  	num1:"进价必须是正数或者0" 
			},
			originalprice:{
				required:"原价不能为空",
				num2:"原价必须是正数或者0" 
			},
			sellprice:{
				required:"标价不能为空",
				num3:"标价必须是正数或者0" 
			},					
			brandId:{
				required:"品牌不能为空",
			},
			sortId:{
				required:"分类不能为空",
				remote:"不能添加一、二级分类,必须添加三级分类"
			},specvalueId:{
				required:"规格必须填写一个"
			}
		}
		});  
// 		var message = $("#message").val();
// 		if(message != ""){
// 			alert(message);
// 		}
	
	
	
	var state1 = $("#display1").val();
 	if(state1 == ""){
 		$("#display1").val("1");
 		$("#displayOff1").hide();
 	}else{
 		 if(state1 == "1"){
 			$("#displayOff1").hide();
 		}else{
 			$("#displayOn1").hide();
 		}
	};
	
	var state2 = $("#display2").val();
 	if(state2 == ""){
 		$("#display2").val("1");
 		$("#displayOff2").hide();
 	}else{
 		 if(state2 == "1"){
 			$("#displayOff2").hide();
 		}else{
 			$("#displayOn2").hide();
 		}
	};
		
//标志图片鼠标经过显示
	$("#brandImgShow").mouseover(function(){
		$("#brandImg").show();
	});
	$("#brandImgShow").mouseout(function(){
		$("#brandImg").hide();
	});
	$("#brandLogo").change(function(){
		$("#textfield1").val($("#brandLogo").val());
	});	
	
//改变系统提示的样式
  $("span .w").mousemove(function(){
	var id = $(this.parentNode).attr("id");
	if(id="nothis"){
	   $(this.parentNode).attr("id","this");
	}
  }).mouseout(function(){
     var id = $(this.parentNode).attr("id");
	 if(id="this"){
	   $(this.parentNode).attr("id","nothis");
	 }
  });
  
});




//修改是否推荐状态
function displayRecommend(){
	var state1 = $("#display1").val();
	if(state1=="1"){
		$("#display1").val("0");
		$("#displayOff1").show();
		$("#displayOn1").hide();
	}else{
		$("#display1").val("1");
		$("#displayOff1").hide();
		$("#displayOn1").show();
	}
}
//修改是否显示状态
function displayState(){
	var state2 = $("#display2").val();
	if(state2=="1"){
		$("#display2").val("0");
		$("#displayOff2").show();
		$("#displayOn2").hide();
	}else{
		$("#display2").val("1");
		$("#displayOff2").hide();
		$("#displayOn2").show();
	}
}
/*  //提交
function save(){
	alert("!!!!");
	saveEdit();
	$("#theForm").submit();
}  */




//ajax根据规格 更改规格值下拉列表
function ajaxChangeSK(obj){
	var sk = obj.options[obj.selectedIndex].value;
	var id = $(obj).attr("id");
	if(sk == "aa"){
		var sel = $("#specvalueId1");
		sel.empty();
		sel.append("<option value='' selected='selected'>请选择规格值</option>");
	}else if(sk == "bb"){
		var sel = $("#specvalueId2");
		sel.empty();
		sel.append("<option value='' selected='selected'>请选择规格值</option>");
	}else{
		$.ajax({
			type:"Post",
			url:"/goodsspec/ajaxSearchSpec_value",
			data:{ specnameId : sk },
			success:function(data){
				var list = JSON.parse(data).list;
				
				if(id == "specnameId1"){
					var sel = $("#specvalueId1");
					sel.empty();
					sel.append("<option value='' selected='selected'>请选择规格值</option>");
					if(list.length!=0){
						for(var index in list){
							var opt = $("<option value='"+list[index].id+"'>"+list[index].value+"</option>");
							sel.append(opt);
						}
					}
				}else{
					var sel = $("#specvalueId2");
					sel.empty();
					sel.append("<option value='' selected='selected'>请选择规格值</option>");
					if(list.length!=0){
						for(var index in list){
							var opt = $("<option value='"+list[index].id+"'>"+list[index].value+"</option>")
							sel.append(opt);
						}
					}
				}
				
			}
		});
	}
}

</script>
<!-- <script type="text/javascript" src="/js/config.js"></script>
<link rel="stylesheet" type="text/css" href="/css/editor.css">
<script type="text/javascript" src="/js/zh-cn.js"></script>
<script type="text/javascript" src="/js/styles.js"></script>
<style id="cke_ui_color" type="text/css"></style> -->

</head>

<body style="">
<div class="cont">
  <h1 class="seth1">商品分类</h1>
  <div class="settab"><span class="tab-one"></span>
      <span class="tabs">
			<a href="/dispatcher/dispatch?dispatch_url=shopping_goodsManage.jsp">所有商品</a> | <a href="/dispatcher/dispatch?dispatch_url=shopping_goodsAdd.jsp" class="this">新增商品</a>
      		&nbsp;&nbsp;&nbsp;<input type="button" value="刷新" onclick="JavaScript:window.location.reload()"><br>			
	  </span> 
    
 </div>
 <form name="theForm" id="theForm" action="/goods/goodsAddOrEdit" method="post" enctype="multipart/form-data" novalidate="novalidate">
  <input name="id" type="hidden" id="id" value="">
  <input name="method" type="hidden" value="add"/>
  <div class="setcont">
  
    <ul class="set1">
      <li><strong class="orange fontsize20">*</strong>商品名称</li>
      <li>
      	<span class="webname">
      		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      		<input type="text" id="goodsname" name="goodsname" placeholder="请输入商品名称" value="" size="40">
      	</span>
	    <span id="msg" style="visibility:visible;color:red;"></span>		      
      </li>
    </ul>
     
      <ul class="set1">
	      <li><strong class="orange fontsize20">*</strong>商品图片</li>
	      <li>
		      <span class="webname">
	      			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;			      
		      		<input name="textfield" type="text" id="textfield1">
		      </span>
			  <span class="filebtn">
			  		<input name="button" type="button" id="button1" value="">
			  </span>
			  <span style="float:left;" class="file">
			  		<input type="file" name="file" id="brandLogo" size="40">
  			  </span>
			  <span class="preview"></span>
			  <span id="nothis"><strong class="q"></strong><strong class="w">最佳尺寸93*33，支持格式gif,jpg,jpeg,png</strong><strong class="c"></strong></span>
		  </li>
      </ul>     
     
    <ul class="set1">
      <li><strong class="orange fontsize20">*</strong>商品货号</li>
      <li>
			<span class="webname">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="text" id="artnumber" name="artnumber" value="" size="40"></span>
			<span id="nothis"></span>	
      </li>     
    </ul>   
    
	<ul class="set1">
      <li><strong class="orange fontsize20">*</strong>库&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;存</li>
      <li>
			<span class="webname">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="text" id="stock" name="stock" value="" size="40" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')"></span>
			<span id="nothis"></span>	      		
      </li>     
    </ul>        
     
    <ul class="set1">
      <li><strong class="orange fontsize20">*</strong>商品价格</li>
      <li>
      	  <span class="webname">&nbsp;&nbsp;原&nbsp;&nbsp;&nbsp;价：<input type="text" id="originalprice" name="originalprice" value="" size="40" onkeyup="this.value=this.value.replace(/[^0-9.]/g,'')" onafterpaste="this.value=this.value.replace(/[^0-9.]/g,'')"></span>
	      <span id="nothis"></span>	      
      </li>
      <li>
      	  <span class="webname">&nbsp;&nbsp;进&nbsp;&nbsp;&nbsp;价：<input type="text" id="buyprice" name="buyprice" value="" size="40"></span>
	      <span id="nothis"></span>      
      </li>
      <li>
      	  <span class="webname">&nbsp;&nbsp;标&nbsp;&nbsp;&nbsp;价：<input type="text" id="sellprice" name="sellprice" value="" size="40"></span>
	      <span id="nothis"></span>      
      </li>      
    </ul>        

	<ul class="set1">
      <li><strong class="orange fontsize20">*</strong>品牌名称</li>
	  <li>	        
  		<span class="webnamesec sizese" id="one_stage" style="cursor: pointer;">	
	  		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  		  
	  		<select name="brandId" id="brandId">                   	
      				
      					<option value="" selected="selected">请选择</option>
			</select>
		</span>	
		<span id="nothis"></span> 
	  </li>		
	</ul>

    <ul class="set1">
      <li><span><strong class="orange fontsize20">*</strong>上级分类&nbsp;&nbsp;&nbsp;(如果选择上级分类，那么新增的分类则为被选择上级分类的子分类) </span><span id="two_out" style="margin-left: 140px"></span></li>
      <li>
      	<span class="webnamesec sizese" id="one_stage" style="cursor: pointer;">
	        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
           	<select id="sortId" name="sortId" class="form-control m-b" style="height:30px;">
           			
           				<option value="" selected="selected">请选择</option>
           		</select>
	      </span>
	      <span id="nothis"></span> 	      
      </li>
    </ul>
		
	<ul class="set1">
	    <li><strong class="orange fontsize20">*</strong>商品规格（请至少选择一个规格）</li>
	       <li id="goods_spec">
	            <span id="na"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="blue">规格名</font></strong></span>
	            <span id="va"><strong><font color="blue">规格值</font></strong></span>
	            <span id="warning">
				               
	            </span>
	        </li>
	    <li>
	  		<span class="webnamesec sizese" id="one_stage" style="cursor: pointer;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;		    
		    <span>
		    <select name="specnameId" id="specnameId1" onchange="ajaxChangeSK(this);" class="form-control m-b">	   
				<option value="aa" selected="selected">请选择规格一</option>
					
			</select>
			</span>
			<span id="nv4">
			<select name="specvalueId" id="specvalueId1">
				<option value="">请选择规格值一</option>
			</select>
			</span>
			</span>
		</li>
		<li>
	  		<span class="webnamesec sizese" id="one_stage" style="cursor: pointer;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;		    
		    <span>
		    <select name="specnameId" id="specnameId2" onchange="ajaxChangeSK(this);" class="form-control m-b">	   
				<option value="bb" selected="selected">请选择规格二</option>
				
					
			</select>
			</span>
			<span id="nv2">
			<select name="specvalueId" id="specvalueId2">
				<option value="" selected="selected">请选择规格值二</option>
			</select>
			</span>
			</span>
		</li>
	</ul>
	
	<ul class="set1">
      <li><strong class="orange fontsize20">*</strong>是否推荐</li>
      <li>
      	<span class="webSwitch">
	   	 	<input name="recommend" id="display1" type="hidden" value="1">
	        <img src="/images/on.jpg" width="61" height="23" id="displayOn1" onclick="displayRecommend();" style="cursor: pointer"> 
	        <img src="/images/off.jpg" width="61" height="23" id="displayOff1" onclick="displayRecommend();" style="cursor: pointer; display: none;">       	     
        </span>
        <span id="nothis">
	        <strong class="q"></strong><strong class="w">推荐商品将会在首页显示</strong><strong class="c"></strong>
        </span>
    </li>
    </ul>

	<ul class="set1">
      <li><strong class="orange fontsize20">*</strong>是否上架</li>
      <li>
      	<span class="webSwitch">
	   	 	<input name="state" id="display2" type="hidden" value="1">
	        <img src="/images/on.jpg" width="61" height="23" id="displayOn2" onclick="displayState();" style="cursor: pointer"> 
	        <img src="/images/off.jpg" width="61" height="23" id="displayOff2" onclick="displayState();" style="cursor: pointer; display: none;">       	     
        </span>
        <span id="nothis">
	        <strong class="q"></strong><strong class="w">上架商品将会在首页显示</strong><strong class="c"></strong>
        </span>
    </li>
    </ul>
    
	<ul class="set1">
		<li><strong class="orange fontsize20">*</strong>商品详情</li>
		<li>
			<div>
			</div>
    	</li>		
 	</ul>

   </div>
			  <textarea id="content" name="content"></textarea>
  	<div class="submit">
	 	<input type="button" value="保存" id="save1" onclick="save()" />  	 
    	<!-- <input name="save" type="submit" value="提交"> -->
 	</div>
  </form>

</div>

</body>


<script type="text/javascript" src="/ckfinder2.4/ckfinder.js"></script>


<script type="text/javascript">
    var editor = null;
    window.onload = function(){
        editor = CKEDITOR.replace( 'content', {
            customConfig:'/ckeditor4.1/myconfig.js',
            on: {
                instanceReady: function( ev ) {
                    this.dataProcessor.writer.setRules( 'p', {
                        indent: false,
                        breakBeforeOpen: false,   //<p>之前不加换行
                        breakAfterOpen: false,    //<p>之后不加换行
                        breakBeforeClose: false,  //</p>之前不加换行
                        breakAfterClose: false    //</p>之后不加换行7
                    });
                }
            }
        });
        CKFinder.setupCKEditor( editor, '/ckfinder2.4/' );
        
      /*   CKEDITOR.editorConfig = function( config ) {
            //其他一些配置
            filebrowserBrowseUrl = '/WEB-INF/jsp/shopping_goodsAdd.jsp';
            filebrowserImageBrowseUrl = '/WEB-INF/jsp/shopping_goodsAdd.jsp?type=Images';
            filebrowserFlashBrowseUrl = '/WEB-INF/jsp/shopping_goodsAdd.jsp?type=Flash';
            filebrowserUploadUrl = '/src/com/mall/controller/GoodsController.java?command=QuickUpload&type=Files';
            filebrowserImageUploadUrl = '/src/com/mall/controller/GoodsController.java?command=QuickUpload&type=Images';
            filebrowserFlashUploadUrl = '/src/com/mall/controller/GoodsController.java?command=QuickUpload&type=Flash';
        }; */
    }
    
    
  /*   editor.setData("1111");
    editor.getData(); */
    
    function save(){
    	saveEdit();
    	$("#theForm").submit();
    }
    
    
    function saveEdit(){
        editor.updateElement(); //非常重要的一句代码
       
        //前台验证工作
        //提交到后台
    }
</script>
</html>