<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>城市管理</title>
<link href="/css/template.css" rel="stylesheet" type="text/css">
<link href="/css/overlay.css" type="text/css" rel="stylesheet">

<style id="poshytip-css-tip-skyblue" type="text/css">
div.tip-skyblue{visibility:hidden;position:absolute;top:0;left:0;}
div.tip-skyblue table, div.tip-skyblue td{margin:0;font-family:inherit;font-size:inherit;font-weight:inherit;font-style:inherit;font-variant:inherit;}div.tip-skyblue td.tip-bg-image span{display:block;font:1px/1px sans-serif;height:10px;width:10px;overflow:hidden;}
div.tip-skyblue td.tip-right{background-position:100% 0;}
div.tip-skyblue td.tip-bottom{background-position:100% 100%;}
div.tip-skyblue td.tip-left{background-position:0 100%;}
div.tip-skyblue div.tip-inner{background-position:-10px -10px;}
div.tip-skyblue div.tip-arrow{visibility:hidden;position:absolute;overflow:hidden;font:1px/1px sans-serif;}
</style>
</head>
<body style="overflow-y: scroll; overflow-x: hidden;">

<div class="cont">
  <h1 class="seth1">地区设置</h1>
  <div class="settab"><span class="tab-one"></span> 
	  <span class="tabs"> <a href="javascript:void(0);" class="this">管理</a> | <a href="/dispatcher/dispatch?dispatch_url=cityAddManage.jsp">新增</a></span>
	  <!-- | <a href="javascript:void(0);" onclick="import_area(this);">导入全国地区数据【三级】</a> -->   
	  <span class="tab-two"></span>
  </div>
  <form method="post" id="theForm2">
<!--     <input name="list_url" type="hidden" id="list_url" value="http://192.168.1.3:8080/shopping/admin/area_list.htm"> -->
    <input name="pid" type="hidden" id="pid" value="">
    <input name="count" type="hidden" id="count" value="0">
    <input name="currentPage" type="hidden" id="currentPage" value=""/>
    <div class="operation">
    <h3>友情提示</h3>
    <ul>
      <li>地区设置是管理系统用户的相关地址信息，默认使用中国大陆三级区域信息</li>
    </ul>
  </div>
    <div class="sear_ads">
      <ul>
        <li>
			<span>查看下一级地区</span>
			<span>
				<select name="tid01" id="tid01" onchange="showLower(this.value,'a')">
					<option value="a">省份</option>
					
					
				</select>
            </span> 
            <span>
	            <select size="1" id="tid02" name="tid02" onchange="showLower(this.value,'b')">
	            	<option value="b">城市</option>   
	            	
	            	      
	          	</select>
          	</span> 
          	<span>
          		<select size="1" id="tid03" name="tid03" onchange="showLower(this.value,'c')">
            		<option value="c">区县</option>
            		
            		
 				</select>
          	</span>
       	</li>
      </ul>
    </div>
    
    <div class="add_ad_table2">
		<table id="add_tr" width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody>
	     <!--    <tr id="addtr" style="background:#2A7AD2; height:30px; color:#FFF">
	          <td width="118">排序</td>
	          <td width="640">地区名称</td>
	          <td width="200">常用地区</td>
	          <td width="215">操作</td>
	        </tr> -->
		</tbody>
		</table>
    </div>
    <div class="fenye" id="fenye" align="right">

    </div>  
  </form>
</div>
<!-- <script src="../js/jquery-1.6.2.js"></script> -->
<script src="/js/jquery-1.8.3.min.js"></script>
<!-- <script src="../js/jquery-ui-1.8.21.js"></script> -->
<!-- <script src="../js/jquery.shop.common.js"></script> -->
<!-- <script src="../js/jquery.poshytip.min.js"></script> -->
<script>
//当前table里的pid和级别
var nowPid = 0;
var nowLV = "a";
$(document).ready(function(){
	showLower(0,"a");
});



//查询下级地区
function showLower(pid,lv){
	if(lv=="a"){
		if(pid=="a"){
			showoption(0,"a");
			showcity(0);
		}else{
			showoption(pid,"a");
			showcity(pid);
			nowLV="b";
			nowPid = pid;
		}
		return;
	}else if(lv=="b"){     // pid为b 是二级下拉列表框的未选中状态  查询该一级分类下所有二级分类
		var pid1 = $("#tid01").val();
		if(pid == "b"){
			showoption(0,"b");
			showcity(pid1);
			nowPid = pid1;
			nowLV = "b";
		}else{
			showoption(pid,"b");
			showcity(pid);
			nowPid = pid;
			nowLV ="c";
		}
		return;
	}
		var pid2 = $("#tid02").val();
		if(pid == "c"){
			showoption(0,"c");
			showcity(pid2);
			nowPid = pid2;
			nowLV = "c";
		}else{ 
			showoption(pid,"c");
			showcity(pid);
			nowPid = pid;
			nowLV = "d";
		}
}
//查询之后显示城市详细
function showCity(){
	alert(nowPid);
	alert("----");
	showcity(nowPid);	
}	

//省市区的下拉列表
function showoption(pid,lv) {
	 $.ajax({
		  type:"POST",
	      url:"/city/showSelectCity",
	      data:{"pid":pid},	       
		  success:function(data){
			  	list = JSON.parse(data).list;
			  	if(pid==0&&lv=="a"){
					$("option[name='option1']").remove();
					$("option[name='option2']").remove();
					$("option[name='option3']").remove();
					for(var index in list){
						$("#tid01").append("<option value='"+list[index].id+"' name='option1'>"+list[index].areaname+"</option>");
					}
			  	}else if(pid!=0&&lv=="a"){
					$("option[name='option2']").remove();
					$("option[name='option3']").remove();
					for(var index in list){	  
						$("#tid02").append("<option value='"+list[index].id+"' name='option2'>"+list[index].areaname+"</option>");
					};
				  }else if(pid==0&&lv=="b"){
					$("option[name='option3']").remove();
				}else if(pid!=0&&lv=="b"){
					$("option[name='option3']").remove();
					for(var index in list){	  
						$("#tid03").append("<option value='"+list[index].id+"' name='option3'>"+list[index].areaname+"</option>");
					};
				}
		  }
	 });	
}




//显示城市信息
function showcity(pid) {
	$("tr[name='cityRow']").remove();
	 $.ajax({
	    	type:'POST',
	        url:'/city/showSelectCity',
		  	data:{"pid":pid
		  		},
		  	success:function(data){  
		  		list = JSON.parse(data).list;
		  		skipPage(list,1);
		  	}	
	 });
}


/**
 * list为作为分页用的集合
 * i为当前页,crrentPage
 */
function skipPage(list,currentPage){
		//console.log("currentPage="+currentPage);
		var pageSize = 10;
		var recNum = list.length;
		$("#add_tr").empty();
		$("#add_tr").append("<tr id=\"addtr\" style=\"background:#2A7AD2; height:30px; color:#FFF\"></tr>");
		$("#addtr").append("<td width=\"118\">排序</td><td width=\"640\">地区名称</td>");
		$("#addtr").append("<td width=\"200\">常用地区</td><td width=\"215\">操作</td>");
		for(var index=(currentPage-1)*pageSize ;index < (currentPage*pageSize); index++){
	 		if(list[index] != null){
				$("#add_tr").append("<tr id='addtr"+list[index].id+"'   name='cityRow' ></tr>");
				$("#addtr"+list[index].id+"").append("<td><div class='editul '><ul class='set3'><li style='margin-left:0px;'><span class='pxnum'><input name='seq' type='text' id='seq_"+list[index].id+"'  value='"+list[index].sequence+"' size='5' onblur=\"ajax_update('"+list[index].id+"','toseq',this)\"></span></li></ul></div></td>");
				$("#addtr"+list[index].id+"").append("<td><div class='editul '><ul class='set3'><li style='margin-left:0px;'><span class='pxnum'><input name='cityname' type='text' id='cityname_"+list[index].id+"' cls='"+list[index].areaname+"' value='"+list[index].areaname+"' size='40' onblur=\"ajax_update('"+list[index].id+"','toname',this,'"+list[index].parentId+"','"+list[index].level+"')\"    ></span></li></ul></div></td>");
				if(list[index].common==1){
					 $("#addtr"+list[index].id+"").append("<td><img src='/images/yes.png' width='21' height='23' onclick=\"ajax_updateyesnouse('"+list[index].id+"','display',this)\" style='cursor:pointer;' title='可编辑'class='yes'/> </td>");
				}else{
					 $("#addtr"+list[index].id+"").append("<td><img src='/images/no.png' width='21' height='23' onclick=\"ajax_updateyesnouse('"+list[index].id+"','display',this)\" style='cursor:pointer;' title='可编辑'/></td>");
				}
				$("#addtr"+list[index].id+"").append("<td id='' class='ac8'><a href='javascript:void(0);' onclick=\"delcity("+list[index].id+");\">删除</a></td>");		
	 		}
		};
			//向上取整,有小数就整数部分加1
			totalPage = Math.ceil(recNum / pageSize);
			//console.log("totalPage="+totalPage);
		
		$("#fenye").empty();
		$("#fenye").append("<a href='javascript:void(0);' onclick='skipPage(list,1)'>首页</a>");
		if(currentPage != 1){
			$("#fenye").append("<a href='javascript:void(0);' onclick='skipPage(list,"+(currentPage-1)+")'>前一页</a>");
		}
		for(var index = 1;index<=totalPage;index++){
			if(currentPage == index){
				$("#fenye").append("<a href='javascript:void(0);' class='this' onclick='skipPage(list,"+index+")'>"+index+"</a>");
			}else{
				$("#fenye").append("<a href='javascript:void(0);'  onclick='skipPage(list,"+index+")'>"+index+"</a>");
			}
		}
		if(currentPage != totalPage){
				$("#fenye").append("<a href='javascript:void(0);' onclick='skipPage(list,"+(currentPage+1)+")'>后一页</a>");
		}
		$("#fenye").append("<a href='javascript:void(0);' onclick='skipPage(list,"+totalPage+")'>末页</a>");
}



//ajax修改城市的状态
function ajax_updateyesnouse(id, fieldName, obj){
	var value = $(obj).hasClass('yes');
	$.ajax({
		type:"Post",
		url:"/city/updateCitySeqAndNameAndStatus",
		data:{"id":id,
			"fieldName":fieldName,
			"value":value
			},
		success:function(data){
			if(data != 0){
				if(value){
					$(obj).attr("src","/images/no.png");
					$(obj).removeClass('yes');
				}else{
					$(obj).attr("src","/images/yes.png");
					$(obj).addClass('yes');
				}			
			}
		}
	});	
}

//ajax修改页面的城市名，排序
function ajax_update(id, fieldName, obj, pid,level){
	
	var c = $(obj).val();
	var secondc = $(obj).attr('cls');
	if(c != secondc){
		$.ajax({
			type:'POST',
			url:'/city/updateCitySeqAndNameAndStatus',
		    data:{"id":id,
		    	"fieldName":fieldName,
		    	"value":c,
		    	"level":level},			    
		    success:function(data){
		    		alert(data);
			    	if(c==""){
						if(data=="citynamenull"){
							alert("城市名称不能为空!");
							showCity();
						}
						if(data=="seqnull"){
							alert("排序不能为空!");
							showCity();
						}					
					}else if(data=="repeat"){
						alert("城市名不能重复!");
						showCity();		
					}else if(data=="geshi"){
						alert("排序格式必须为数字!");
						showCity();	
					} 	
			    	if(pid=="0"){
			    		alert("1111");
			    		$("option[name='option1']").remove();
			    		nowPid=0;
			    		nowLV = "a";
			    		showoption(0,"a");
			    		return;
			    	}else if(pid=="b"){     // pid为b 是二级下拉列表框的未选中状态  查询该一级分类下所有二级分类
			    		alert("2222");
// 			    		$("option[name='option2']").remove();
			    		var pid2 = $("#tid01").val();
			    		nowPid = pid2;
			    		nowLV = "b";
			    		showoption(0,"b");
			    		return;
			    	}
			    		alert("3333");
			    		showoption(nowPid,"c");
			    		nowPid = pid;
			    	
// 			    	window.location.reload();   	
            }
		    
	    });
	}
}


// 	$("#add_tr").after("<tr id='add_tr"+count+"'> ></tr>");
// 	$("#add_tr"+value['id']+"").append("<td><div class='editul '><ul class='set3'><li style='margin-left:0px;'><span class='pxnum'><input name='list_city["+count+"].seq' type='text' id='newseq_"+count+"' value='0' size='5' ><input name='list_city["+count+"].pid' type='hidden' id='neid_"+count+"' value='"+nowPid+"' size='5' ></span></li></ul></div></td>");
// 	$("#addtr"+value['id']+"").append("<td><div class='editul '><ul class='set3'><li style='margin-left:0px;'><span class='pxnum'><input name='list_city["+count+"].cityname' type='text' id='newcityname_"+count+"' value='' size='40' ></span></li></ul></div></td>");
// 	if(value['yesnouse']==1){
// 		 $("#add_tr"+value['id']+"").append("<td><img src='../images_project/city/yes.png' width='21' height='23' onclick='ajax_updateyesnouse('"+value["id"]+"','display',this)' style='cursor:pointer;' title='可编辑'class='yes'/> </td>");
// 	}else{
// 		 $("#add_tr"+value['id']+"").append("<td><img src='../images_project/city/no.png' width='21' height='23' onclick='ajax_updateyesnouse('"+value["id"]+"','display',this)' style='cursor:pointer;' title='可编辑'/></td>");
// 	}
// 	$("#add_tr"+value['id']+"").append("<td id='' class='ac8'><a href='javascript:void(0);' onclick='remove_city("+count+")'>删除</a></td>");					 


//新增一个添加城市的文本框
var count=0;
function area_add(){
    count++;
    $("#add_tr").append("<tr id='newaddtr"+count+"'><td><div class='editul '><ul class='set3'><li style='margin-left:0px;'><span class='pxnum'><input name='list_city["+count+"].seq' type='text' id='newseq_"+count+"' value='0' size='5' ><input name='list_city["+count+"].pid' type='hidden' id='neid_"+count+"' value='"+nowPid+"' size='5' ></span></li></ul></div></td><td><div class='editul '><ul class='set3'><li style='margin-left:0px;'><span class='pxnum'><input name='list_city["+count+"].cityname' type='text' id='newcityname_"+count+"' value='' size='40' ></span></li></ul></div></td><td></td><td id='' class='ac8'><a href='javascript:void(0);' onclick='remove_city("+count+")'>删除</a></td></tr>");       
    $("#count").attr("value",count);
}
//删除一行城市文本框
function remove_city(num){
	$("#newaddtr"+num+"").remove();
}

//点击删除按钮，删除城市
function delcity(id){
// 	debugger
	if(!confirm("确定删除么")){
		return;
	}
	$.ajax({
		type:"POST",
		url:"/city/deleteCity",
		data:{"id":id,},
		success:function(msg){
			if(msg=="1"){
				showcity(nowPid);
// 				alert("删除成功");
				if(nowLV == "a"){
					$("option[name='option1']").remove();
					$("option[name='option2']").remove();
					$("option[name='option3']").remove();
					showoption(0,"a");
				}
				if(nowLV == "b"){
					$("option[name='option2']").remove();
					$("option[name='option3']").remove();
					showoption(nowPid,"a");
				}else{
					$("option[name='option3']").remove();
					showoption(nowPid,"b");
				}
			}else{
				alert(msg);
			}
		}
	});
}

</script>


</body></html>