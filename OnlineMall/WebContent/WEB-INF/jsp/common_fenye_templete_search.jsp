<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<a href="javascript:void(0);" onclick="skipPage(1)"     >首页</a>
<c:if test="${currentPage != 1 }">
	<a href="javascript:void(0);" onclick="skipPage(${currentPage-1 })"     >前一页</a>
</c:if>
<c:forEach begin="1" end="${totalPageNum }" var="i">
	<c:if test="${currentPage == i }">
		<a href="javascript:void(0);" onclick="skipPage(${i })" class="this">${i }</a>
	</c:if>
	<c:if test="${currentPage != i }">
		<a href="javascript:void(0);" onclick="skipPage(${i })"    >${i }</a>
	</c:if>
</c:forEach>
<c:if test="${currentPage != totalPageNum }">
	<a href="javascript:void(0);" onclick="skipPage(${currentPage+1 })"   >后一页</a>
</c:if>
<a href="javascript:void(0);" onclick="skipPage(${totalPageNum })"    >末页</a>