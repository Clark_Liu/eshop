package com.mall.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mall.dao.GoodsTypeMapper;

@Service
public class GoodsTypeDeleteService {

	@Autowired
	private GoodsTypeMapper dao;
	
	public void deleteone(long id)
	{
		dao.deleteByPrimaryKey(id);
	}
	
}
