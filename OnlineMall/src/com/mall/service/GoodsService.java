package com.mall.service;


import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.SynchronousQueue;

import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.mall.dao.GoodsId_spedId_propertyIdMapper;
import com.mall.dao.GoodsMapper;
import com.mall.dao.GoodsSpecIficationMapper;
import com.mall.dao.GoodsSpecPropertyMapper;
import com.mall.entity.Goods;
import com.mall.entity.GoodsClass;
import com.mall.entity.GoodsId_spedId_propertyId;
import com.mall.entity.GoodsSpecIfication;
import com.mall.entity.GoodsSpecProperty;
import com.mall.utils.IsNumberUtils;
import com.sun.xml.internal.bind.v2.model.core.ID;

import net.sf.json.JSONObject;

@Service
public class GoodsService {
	@Autowired
	private GoodsMapper dao;
	@Autowired
	private GoodsId_spedId_propertyIdMapper dao2;
	@Autowired
	private GoodsSpecPropertyMapper dao3;
	
	public List<Goods> findAll(HttpSession session,int currentPage) {
		PageHelper.startPage(currentPage, 5);
		List<Goods> list = dao.findAll();
		
		PageInfo<Goods> pageInfo = new PageInfo<>(list);
		int i = pageInfo.getPages();
		
		
		session.setAttribute("totalPageNum", i);
		return list;
	}

	public List<Goods> search(String goodsname, Long brandId, Long sortId, Integer recommend,Integer sortLevel, String sortName,Integer currentPage,HttpSession session) {
		Goods g = new Goods();
		if(goodsname !="" || goodsname != null) {
			g.setGoodsName(goodsname);
		}
		if(brandId != null) {
			g.setGbId(brandId);
		}
		System.out.println(sortLevel);
		System.out.println(sortName);
		if(sortId != null) {
			switch(sortLevel) {
			case 0:
				g.setFirstclassname(sortName);
				break;
			case 1:
				g.setSecendclassname(sortName);
				break;
			case 2:
				g.setThirdclassname(sortName);
				break;
			}
		}
		if(recommend != null) {
			if(recommend == 1) {
				g.setGoodsRecommend(true);
			}else {
				g.setGoodsRecommend(false);
			}
		}
		
		PageHelper.startPage(currentPage, 5);
		List<Goods> list = dao.search(g);
		PageInfo<Goods> pageInfo = new PageInfo<>(list);
		int i = pageInfo.getPages();
		
		
		session.setAttribute("totalPageNum", i);
		
		return list;
		
		
	}

	public int updateGoodsName(Long id, String value) {
		
		int i = dao.isExistGoodsName(value);
		if(i == 0) {
			dao.updateGoodsName(id,value);
			return 1;
		}
		return 0;
		
	}

	public void updateGoodsRecommend(Long id, String val) {
		boolean isRec = false;
		if("no".equals(val)) {
			isRec = true;
		}
		dao.updateGoodsRecommend(id,isRec);
	}

	public void deleteGoodsOne(Long id) {
		dao.deleteGoodsOne(id);
		
		
	}

	public Boolean checkGoodName(String goodsname) {
		List<Goods> list = dao.checkGoodName(goodsname);
		if(list.size()>0) {
			return false;
		}else {
			return true;
		}
	}
	
	public Boolean checkArtnumber(Long artnumber) {
		List<Goods> list = dao.checkArtnumber(artnumber);
		if(list.size()>0) {
			return false;
		}else {
			return true;
		}
	}

	public Boolean checkSort(Long sortId) {
		GoodsClass gc = dao.checkSort(sortId);
		if(gc.getLevel() != 2) {
			return false;
		}
		return true;
		
	}

	public void goodsAddOrEdit(String goodsname, MultipartFile file, Long artnumber, Long stock, Double originalprice,
			Double buyprice, Double sellprice, String brandId, String sortId, String[] specnameId, String[] specvalueId,
			Integer recommend, Integer state, String content,String method,Long id) {
		Goods g = new Goods();
		//设置商品名称
		g.setGoodsName(goodsname);
		
		//设置商品添加时间
		Date currentDate = new Date();
		g.setGoodsAddtime(currentDate);
		
		//设置商品图片
		if(file.getOriginalFilename() != "") {
			String originalFilename = file.getOriginalFilename();
			String fileName[] = originalFilename.split("\\.");
			String fileType = fileName[fileName.length-1];
			
			SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");//获取当前时间
			String newFileName = goodsname + df.format(currentDate) + "." + fileType;
			
			InputStream is = null;
			OutputStream os = null;
			try {
				os = new FileOutputStream("C:\\Users\\Administrator\\git\\eshop\\OnlineMall\\WebContent\\upload\\goodsPicture" + "\\" + newFileName);
				//os = new FileOutputStream("I:\\JavaApps\\eshop\\OnlineMall\\WebContent\\upload\\goodsPicture\\"+ newFileName);
				//os = new FileOutputStream(string + "\\" + newFileName);
				is = file.getInputStream();
				IOUtils.copy(is, os);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}finally {
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			g.setGoodsPicture("/upload/goodsPicture/" + newFileName);
		}
		//设置货号
		g.setId(artnumber);
		
		//设置库存
		g.setGoodsInventory(stock);
		//设置进价
		BigDecimal b = new BigDecimal(buyprice);
		g.setGoodsMarketprice(b);
		//原价
		b = new BigDecimal(originalprice);
		g.setGoodsOriginalprice(b);
		//售价
		b = new BigDecimal(sellprice);
		g.setGoodsCurrentprice(b);
		//设置分类id
		g.setGcId(Long.parseLong(sortId));
		//设置品牌id
		g.setGbId(Long.parseLong(brandId));
		//设置是否推荐
		if(recommend == 1) {
			g.setGoodsRecommend(true);
		}else {
			g.setGoodsRecommend(false);
		}
		//设置状态
		if(state == 1) {
			g.setGoodsStatus(true);
		}else {
			g.setGoodsStatus(false);
		}
		//设置商品详情
		g.setGoodsDetail(content);
		
		//设置商品规格名和规格值
		GoodsId_spedId_propertyId gsp = new GoodsId_spedId_propertyId();
		gsp.setGoodsId(artnumber);
		if(specnameId.length>4) {
			gsp.setGoodsSspecificationId5(Long.parseLong(specnameId[4]));
			gsp.setGoodsSpecpropertyId5(Long.parseLong(specvalueId[4]));
		}
		if(specnameId.length>3) {
			gsp.setGoodsSspecificationId4(Long.parseLong(specnameId[3]));
			gsp.setGoodsSpecpropertyId4(Long.parseLong(specvalueId[3]));
		}
		if(specnameId.length>2) {
			gsp.setGoodsSspecificationId3(Long.parseLong(specnameId[2]));
			gsp.setGoodsSpecpropertyId3(Long.parseLong(specvalueId[2]));
		}
		if(specnameId.length>1) {
			if(IsNumberUtils.isNumber(specnameId[1])) {
				gsp.setGoodsSspecificationId2(Long.parseLong(specnameId[1]));
			}
			if(IsNumberUtils.isNumber(specvalueId[1])&&!"".equals(specvalueId[1])) {
				gsp.setGoodsSpecpropertyId2(Long.parseLong(specvalueId[1]));
			}
		}
		if(specnameId.length>0) {
			if(IsNumberUtils.isNumber(specnameId[0])) {
				gsp.setGoodsSspecificationId1(Long.parseLong(specnameId[0]));
			}
			if(IsNumberUtils.isNumber(specvalueId[0])&&!"".equals(specvalueId[0])) {
				gsp.setGoodsSpecpropertyId1(Long.parseLong(specvalueId[0]));
			}
		}
		if("add".equals(method)) {
			dao.insertSelective(g);
			dao2.insertSelective(gsp);
		}else if("edit".equals(method)){
			dao.update(g,id);
			dao2.updateByPrimaryKeySelective(gsp,id);
		}
	}

	public void selectByGoodsId(Long goodsid,PrintWriter out) {
		Goods g = dao.selectByGoodsId(goodsid);
		GoodsId_spedId_propertyId gsp = dao2.selectByGoodsid(goodsid);
		Long gsId1 = gsp.getGoodsSspecificationId1();
		Long gsId2 = gsp.getGoodsSspecificationId2();
		List<GoodsSpecProperty> gspList1 = dao3.selectBytypeid(gsId1);
		List<GoodsSpecProperty> gspList2 = dao3.selectBytypeid(gsId2);
		JSONObject js = new JSONObject();
		js.put("g", g);
		js.put("gsp", gsp);
		js.put("gsp1", gspList1);
		js.put("gsp2", gspList2);
		out.write(js.toString());
	}
}
