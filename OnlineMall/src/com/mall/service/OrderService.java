package com.mall.service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.mall.dao.OrdersMapper;
import com.mall.entity.Orders;

@Service
public class OrderService {
	@Autowired
	private OrdersMapper dao;
	
	public List<Orders> findAll(Integer currentPage,HttpSession session) {
		PageHelper.startPage(currentPage, 2);
		List<Orders> list = dao.findAll();
		
		PageInfo<Orders> pInfo = new PageInfo<>(list);
		session.setAttribute("totalPageNum", pInfo.getPages());
		
		return list;
	}

	public List<Orders> searchOrder(Integer state, String billcode, String begintimeHidden,
			String endtimeHidden, Integer beginpriceHidden, Integer endpriceHidden,HttpSession session,Integer currentPage) {
		BigDecimal beginprice = null;
		if(beginpriceHidden != null) {
			beginprice = new BigDecimal(beginpriceHidden);
			
		}
		
		BigDecimal endprice = null;
		if(endpriceHidden != null) {
			endprice = new BigDecimal(endpriceHidden);
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date begintime = null;
		Date endtime = null;
		try {
			if(begintimeHidden != "") {
				begintime = sdf.parse(begintimeHidden);
			}
			if(endtimeHidden != "") {
				endtime = sdf.parse(endtimeHidden);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		PageHelper.startPage(currentPage, 3);
		List<Orders> list = dao.searchOrder(state,billcode,beginprice,endprice,begintime,endtime);
		PageInfo<Orders> pinfo = new PageInfo<>(list);
		session.setAttribute("totalPageNum", pinfo.getPages());
		return list;
		
		
	}

}
