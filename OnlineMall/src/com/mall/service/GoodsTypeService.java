package com.mall.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.mall.dao.GoodsSpecIficationMapper;
import com.mall.dao.GoodsTypeMapper;
import com.mall.dao.Goods_Fjs_BrandCategoryMapper;
import com.mall.dao.Goods_Fjs_GoodsTypePropertyMapper;
import com.mall.dao.Goods_Fjs_Type_BrandMapper;
import com.mall.dao.Goods_Fjs_Type_SpecMapper;
import com.mall.entity.GoodsSpecIfication;
import com.mall.entity.GoodsType;
import com.mall.entity.Goods_Fjs_BrandCategory;
import com.mall.entity.Goods_Fjs_GoodsTypeProperty;
import com.mall.entity.Goods_Fjs_Type_Brand;
import com.mall.entity.Goods_Fjs_Type_Spec;

@Service
public class GoodsTypeService {
	
	@Autowired
	private GoodsTypeMapper dao;//2��?������D��
	
	public List<GoodsType> selectAll()
	{
		List<GoodsType> list = dao.selectAll();
		return list;
	}
	
	public void addtype(String name,Integer sequence)
	{
		GoodsType gt = new GoodsType();
		gt.setName(name);
		gt.setSequence(sequence);
		gt.setDeletestatus(false);
		dao.insertSelective(gt);
	}
	
	public void uptype(Long id,String name,Integer sequence)
	{
		GoodsType gt = new GoodsType();
		gt.setName(name);
		gt.setId(id);
		gt.setSequence(sequence);
		gt.setDeletestatus(false);
		dao.updateByPrimaryKeySelective(gt);
	}
	
	@Autowired
	private GoodsTypeMapper dao2;//��?3y����D��
	
	public void deleteone(long id)
	{
		dao2.deleteByPrimaryKey(id);
	}
	
	@Autowired
	private GoodsSpecIficationMapper dao3;//2��?��1?????o��1????��
	
	public List<GoodsSpecIfication> selectiAll()
	{
		List<GoodsSpecIfication> list = dao3.getiandp();
		return list;
	}

	@Autowired
	private Goods_Fjs_BrandCategoryMapper dao4;//2��?��?��??��?����o��?��????
	
	public List<Goods_Fjs_BrandCategory> selectbrand()
	{
		List<Goods_Fjs_BrandCategory> list = dao4.selectbcandb();
		return list;
	}
	
	@Autowired
	private Goods_Fjs_Type_SpecMapper dao5;
	
	public void addtands(Long typeid,Long specid)
	{
		Goods_Fjs_Type_Spec gs = new Goods_Fjs_Type_Spec();
		gs.setSpecId(specid);
		gs.setTypeId(typeid);
		dao5.insertSelective(gs);
	}
	
	public List<Goods_Fjs_Type_Spec> hqspec()
	{
		List<Goods_Fjs_Type_Spec> list = dao5.selectAll();
		return list;
	}
	
	@Autowired
	private Goods_Fjs_Type_BrandMapper dao6;
	
	public void addtandb(Long typeid,Long brandid)
	{
		Goods_Fjs_Type_Brand gb = new Goods_Fjs_Type_Brand();
		gb.setBrandId(brandid);
		gb.setTypeId(typeid);
		dao6.insertSelective(gb);
	}
	
	public List<Goods_Fjs_Type_Brand> hqbrand()
	{
		List<Goods_Fjs_Type_Brand> list = dao6.selectAll();
		return list;
	}
	
	@Autowired
	private Goods_Fjs_GoodsTypePropertyMapper dao7;
	
	public List<Goods_Fjs_GoodsTypeProperty> hqshuxing()
	{
		List<Goods_Fjs_GoodsTypeProperty> list = dao7.selectAll();
		return list;
	}
	
	public void addsx(String name,String value,Integer sequences,Boolean displayd,Long typeid)
	{
		Goods_Fjs_GoodsTypeProperty gtp = new Goods_Fjs_GoodsTypeProperty();
		gtp.setGoodstypeId(typeid);
		gtp.setName(name);
		gtp.setValue(value);
		gtp.setSequence(sequences);
		gtp.setDeletestatus(true);
		gtp.setDisplay(displayd);
		dao7.insertSelective(gtp);
	}
	
	public void upsx(long Id,String name,String value,Integer sequences,Boolean displayd,Long typeid)
	{
		Goods_Fjs_GoodsTypeProperty gtp = new Goods_Fjs_GoodsTypeProperty();
		gtp.setId(Id);
		gtp.setGoodstypeId(typeid);
		gtp.setName(name);
		gtp.setValue(value);
		gtp.setSequence(sequences);
		gtp.setDeletestatus(false);
		gtp.setDisplay(displayd);
		dao7.updateByPrimaryKeySelective(gtp);
	}
	
	public List<GoodsType> service(Integer currentPage,HttpServletRequest req) {
			
			Integer pageSize = 12;
			PageHelper.startPage(currentPage, pageSize);
			List<GoodsType> list = dao.selectAll();
			
			
			Integer totalRecNum = dao.countFirstClass(); 
			Integer totalPageNum  = 0;
			if(totalRecNum % pageSize > 0) {
				totalPageNum = totalRecNum/pageSize + 1;
			}else {
				totalPageNum = totalRecNum/pageSize;
			}
			
			
			req.getSession().setAttribute("totalRecNum", totalRecNum);
			
			req.getSession().setAttribute("totalPageNum", totalPageNum);
			
			return list;
	}
	
}
