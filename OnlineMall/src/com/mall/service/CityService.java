package com.mall.service;


import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mall.dao.CityMapper;
import com.mall.entity.City;

@Service
public class CityService {
	@Autowired
	private CityMapper dao;
	
	public List<City> showSelectCity(Long pid,HttpSession session) {
		List<City> list = null;
		list = dao.showSelectCity(pid);
		return list;
	}

	
	public int updateCitySeqAndNameAndStatus(Long id, String fieldName, String value) {
		City c = new City();
		c.setId(id);
		switch(fieldName) {
		case "toseq":
			Integer seq = Integer.parseInt(value);
			c.setSequence(seq);
			break;
		case "toname":
			c.setAreaname(value);
			break;
		case "display":
			Boolean val = !(Boolean.parseBoolean(value));
			c.setCommon(val);
			break;
		}
		return dao.updateByPrimaryKeySelective(c);
	}


	public Boolean selectByAreaname(String value,Integer level) {
		List<City> list = dao.selectByAreaname(value,level);
		
		Boolean isExist = null;
		if(list.size() > 0) {
			isExist = true;
		}else {
			isExist = false;
		}
		return isExist;
 	}


	public int deleteCity(Long id) {
		City c = new City();
		c.setId(id);
		c.setDeletestatus(true);
		return dao.updateByPrimaryKeySelective(c);
	}


	public int addCity(String cityname, Integer level, Integer seq, Integer yesnouse, String parentId) {
		City c = new City();
		c.setAreaname(cityname);
		c.setLevel(level);
		c.setSequence(seq);
		Boolean b = false;
		if(yesnouse == 1) {
			b = true;
		}
		c.setCommon(b);
		Long pid = null;
		if(!"".equals(parentId)) {
			pid = Long.parseLong(parentId);
		}
		c.setParentId(pid);
		c.setDeletestatus(false);
		c.setAddtime(new Date());
		int i = dao.insertSelective(c);
		return i ;
	}
}