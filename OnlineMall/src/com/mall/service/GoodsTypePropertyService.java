package com.mall.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mall.dao.GoodsSpecPropertyMapper;
import com.mall.entity.GoodsSpecProperty;

@Service
public class GoodsTypePropertyService {
	
	@Autowired
	private GoodsSpecPropertyMapper dao;
	
	public List<GoodsSpecProperty> selectAll()
	{
		List<GoodsSpecProperty> list = dao.selectAll();
		return list;
	}
	 

}
