package com.mall.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.mall.dao.Shopping_advertMapper;
import com.mall.entity.Shopping_advert;

@Service
public class ShoppingAdvertService {
	@Autowired
	private Shopping_advertMapper dao;

	// 分页取数据
	public List<Shopping_advert> fenye(int currentPage, HttpSession session) {
		
		System.out.println("进入分页service");
		System.out.println("#####"+currentPage);
		int sum = dao.count();
		int pagenum = 1;
		if (sum % 5 > 0) {
			pagenum = sum / 5 + 1;
		}else{
			pagenum= sum/5;
		}
		if (currentPage < 0) {
			currentPage = 0;
		} else if (currentPage > pagenum) {
			currentPage = pagenum;
		}
		
		List<Shopping_advert> list = new ArrayList<>();
		PageHelper.startPage(currentPage, 5);
		list = dao.selectForshow();
//		System.out.println("数据库记录条数=" + sum);
//		System.out.println("页数=" + pagenum);
//		System.out.println("当前页码=" + currentPage);
		session.setAttribute("pagenum1", pagenum);
		session.setAttribute("pagenow", currentPage);
		return list;
	}

	// 增加
	public void insert(Shopping_advert sa) {
		dao.insert(sa);
	}

	// 表单校验
	public Shopping_advert selectforForm(Shopping_advert sa) {
		System.out.println("表单验证service");
		return dao.selectForform(sa);
	}

	// 编辑更新
	public void update(Shopping_advert sa) {
		System.out.println("进入广告修改service");
		dao.updateByPrimaryKeySelective(sa);
	}

	// 取出单签编辑的数据
	public Shopping_advert eidtnow(Integer id) {
		return dao.selectByPrimaryKey(id);
	}

	// 删除 真删
	public void delete(Integer id) {
		dao.deleteByPrimaryKey(id);
	}

	// 模糊查询
	public List<Shopping_advert> search(int pageNum, Shopping_advert sa,HttpSession session) {
		System.out.println("进入模糊分页service");
		
		//List<Goods_brand> listCount=dao1.search(goods_brand);
		List<Shopping_advert> listCount= dao.selectBytitle(sa);
		int sum=listCount.size();
		System.out.println("模糊查询记录="+sum);
		List<Shopping_advert> list = new ArrayList<>();
		PageHelper.startPage(pageNum, 5);
		list = dao.selectBytitle(sa);
		
		int pagenum = 1;
		if (sum % 5 > 0) {
			pagenum = sum / 5 + 1;
		}else{
			pagenum = sum/5;
		}
		if (pageNum < 0) {
			pageNum = 0;
		} else if (pageNum > pagenum) {
			pageNum = pagenum;
		}

//		System.out.println("数据库记录条数=" + sum);
//		System.out.println("搜索页数=" + pagenum);
//		System.out.println("搜索当前页码=" + pageNum);
		session.setAttribute("pagenum1", pagenum);
		session.setAttribute("pagenow", pageNum);
		return list;

	}
}
