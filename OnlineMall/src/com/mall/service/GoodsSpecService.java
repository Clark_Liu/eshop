package com.mall.service;

import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.mall.dao.GoodsSpecIficationMapper;
import com.mall.dao.GoodsSpecPropertyMapper;
import com.mall.dao.Goods_Fjs_GoodsTypePropertyMapper;
import com.mall.entity.GoodsSpecIfication;
import com.mall.entity.GoodsSpecProperty;

import net.sf.json.JSONObject;

@Service
public class GoodsSpecService {
	
	@Autowired
	private GoodsSpecIficationMapper dao;
	
	
	public List<GoodsSpecIfication> service(Integer currentPage,HttpServletRequest req) {
		
		Integer pageSize = 12;
		PageHelper.startPage(currentPage, pageSize);
		List<GoodsSpecIfication> list = dao.getiandp();
		
		
		Integer totalRecNum = dao.countFirstClass(); 
		Integer totalPageNum  = 0;
		if(totalRecNum % pageSize > 0) {
			totalPageNum = totalRecNum/pageSize + 1;
		}else {
			totalPageNum = totalRecNum/pageSize;
		}
		
		
		req.getSession().setAttribute("totalRecNum", totalRecNum);
		
		req.getSession().setAttribute("totalPageNum", totalPageNum);
		
		return list;
	}
	
	public void deleteone(long id)
	{
		dao.deleteByPrimaryKey(id);
	}
	
	public List<GoodsSpecIfication> selectAll()
	{
		List<GoodsSpecIfication> list = dao.selectAll();
		return list;
	}
	
	public void addifi(String name,Integer sequence,String type)
	{
		GoodsSpecIfication gsi = new GoodsSpecIfication();
		gsi.setName(name);
		gsi.setSequence(sequence);
		gsi.setDeletestatus(false);
		gsi.setType(type);
		dao.insertSelective(gsi);
	}
	
	@Autowired
	private GoodsSpecPropertyMapper dao2;
	
	public List<GoodsSpecProperty>selectimg()
	{
		List<GoodsSpecProperty> list = dao2.selectAll();
		return list;
	}
	
	public void addgg(String name,Integer sequences,String img,long specId) 
	{
		GoodsSpecProperty gsp = new GoodsSpecProperty();
		gsp.setValue(name);
		gsp.setSequence(sequences);
		if(img == "")
		{
			
		}
		else
		{
			long Img = Integer.parseInt(img);
			gsp.setSpecimageId(Img);
		}
		gsp.setSpecId(specId);
		gsp.setDeletestatus(false);
		dao2.insertSelective(gsp);
	}

	public void findAll(PrintWriter out) {
		List<GoodsSpecIfication> list = dao.selectAll();
		
		JSONObject js = new JSONObject();
		js.put("list", list);
		
		
		out.write(js.toString());
	}

	public void findBySpecId(Long specnameId,PrintWriter out) {
		List<GoodsSpecProperty> list = dao2.findBySpecId(specnameId);
		
		JSONObject js = new JSONObject();
		js.put("list", list);
		out.write(js.toString());
	}
	
}
