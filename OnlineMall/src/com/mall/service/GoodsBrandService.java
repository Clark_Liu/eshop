package com.mall.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.mall.dao.Goods_brandMapper;
import com.mall.entity.Goods_brand;


@Service
public class GoodsBrandService {
	@Autowired
	private Goods_brandMapper dao1;
	
	public List<Goods_brand> selectall(){
		return dao1.selectAll();
	}
		
	/**
	 * �޸�ͨ��
	 * @param id
	 * @param fieldName
	 * @param val
	 * @return
	 */
	public String update(Long id, String fieldName, String val) {
		System.out.println("主页数据修改service");
		String ret = null;
		// Goods gs= new Goods();
		// Shopping_goodsbrand sg= new Shopping_goodsbrand();
		Goods_brand gb = new Goods_brand();
		gb.setId(id);
		//System.out.println(fieldName);
		switch (fieldName) {
		case "sequence":		
			gb.setSequence(Integer.parseInt(val));
			break;
		case "first_word":
			//System.out.println(val);
			gb.setFirstWord(val);
			break;
		case "name":
			//System.out.println(val);
			gb.setName(val);
			break;
//		case "display":		
//			Boolean b = Boolean.parseBoolean(val);
//			Boolean a=!b;
//			ret=a.toString();
//			sg.setDisplay(a);
//			break;
		case "recommend":
			Boolean b1 = Boolean.parseBoolean(val);
			Boolean a1=!b1;
			ret=a1.toString();
			gb.setRecommend(a1);
			break;
		case "delete":
			gb.setDeletestatus(true);
			break;	
		}
		//dao.updateByPrimaryKeySelectivenew(gs);
		dao1.updateById(gb);
		//System.out.println("ret" + "=" + ret);
		return ret;
	}
	//表单验证
	public Goods_brand brand_edit(Long id){
		return dao1.selectByIdForEdit(id);
	}
	//编辑功能
	public void updateAndfileupload(Goods_brand brand){
		dao1.updateById(brand);
	}

	//单个删除功能呢
	public void insert(Goods_brand brand) {
		if (brand.getDeletestatus() == null)
			//System.out.println(123);
		dao1.insertSelective(brand);
	}

	// 批量删除
	public boolean deleteall(String[] id) {
		System.out.println("进入批量删除service");
		if (id == null) {
			System.out.println(123);
		}
		if (dao1.updateByIddeleteall(id) > 0) {
			return true;
		}
		return false;
	}

	public Goods_brand selectforForm(Goods_brand Goods_brand) {
		return dao1.selectByNameForform(Goods_brand);
	}

	// 模糊查询
	public List<Goods_brand> search(Goods_brand goods_brand) {
		System.out.println("进入模糊查询service");
		return dao1.search(goods_brand);
	}
	//模糊查询分页功能
	public List<Goods_brand> search1(int currentPage, HttpSession session,Goods_brand goods_brand) {
		System.out.println("进入模糊分页service");
		
		List<Goods_brand> listCount=dao1.search(goods_brand);
		int sum=listCount.size();
		System.out.println("模糊查询记录="+sum);
		List<Goods_brand> list = new ArrayList<>();
		PageHelper.startPage(currentPage, 5);
		list=dao1.search(goods_brand);
		
		
		System.out.println("模糊查询记录="+sum);
		int pagenum = 1;
		if (sum % 5 > 0) {
			pagenum = sum / 5 + 1;
		}
		if (currentPage < 0) {
			currentPage = 0;
		} else if (currentPage > pagenum) {
			currentPage = pagenum;
		}
//		List<Goods_brand> list = new ArrayList<>();
//		PageHelper.startPage(currentPage, 5);
//		list=dao1.search(goods_brand);
		//list = dao1.selectAll();

		//System.out.println("数据库记录条数=" + sum);
		//System.out.println("搜索页数=" + pagenum);
		//System.out.println("搜索当前页码=" + currentPage);
		session.setAttribute("pagenum1", pagenum);
		session.setAttribute("pagenow", currentPage);
		return list;
	}
	// 主页显示分页功能
	public List<Goods_brand> fenye(int currentPage, HttpSession session) {
		System.out.println("进入分页service");
		int sum = dao1.selectcountByid();
		int pagenum = 1;
		if (sum % 5 > 0) {
			pagenum = sum / 5 + 1;
		}
		if (currentPage < 0) {
			currentPage = 0;
		} else if (currentPage > pagenum) {
			currentPage = pagenum;
		}
		List<Goods_brand> list = new ArrayList<>();
		PageHelper.startPage(currentPage, 5);
		list = dao1.selectAll();

		//System.out.println("数据库记录条数=" + sum);
		//System.out.println("页数=" + pagenum);
		//System.out.println("当前页码=" + currentPage);
		session.setAttribute("pagenum1", pagenum);
		session.setAttribute("pagenow", currentPage);
		return list;
	}
}
