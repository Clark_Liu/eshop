package com.mall.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mall.dao.GoodsSpecIficationMapper;
import com.mall.dao.GoodsSpecPropertyMapper;
import com.mall.entity.GoodsSpecIfication;
import com.mall.entity.GoodsSpecProperty;

@Service
public class GoodsTypeIficationService {
	
	@Autowired
	private GoodsSpecIficationMapper dao;
	
	public List<GoodsSpecIfication> selectAll()
	{
		
		List<GoodsSpecIfication> list = dao.getiandp();
		return list;
	}

}
