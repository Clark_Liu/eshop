package com.mall.service;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.mall.dao.GoodsClass1Mapper;
import com.mall.dao.GoodsClassMapper;
import com.mall.entity.GoodsClass;
import com.mall.entity.GoodsClass1;

import net.sf.json.JSONObject;

@Service
public class GoodsClassService {
	@Autowired
	private GoodsClassMapper dao;
	
	@Autowired
	private GoodsClass1Mapper dao1;
	
	public List<GoodsClass> service(Integer currentPage,HttpServletRequest req) {
		
		Integer pageSize = 5;
		PageHelper.startPage(currentPage, pageSize);
		List<GoodsClass> list = dao.findAll();
		PageInfo<GoodsClass> pInfo = new PageInfo<>(list);
		
		//一级分类总记录数
		req.getSession().setAttribute("totalRecNum", pInfo.getTotal());
		//由记录数获得的 总页数
		req.getSession().setAttribute("totalPageNum", pInfo.getPages());
		
		return list;
	}

	
	public List<GoodsClass> serviceByParentId(Long pid) {

		List<GoodsClass> list = dao.selectByParendId(pid);

		return list;
	}

	public void updateBy(Long id, Integer level, String fieldName, String value, PrintWriter out) {
		
		GoodsClass gc = new GoodsClass();
		gc.setId(id);
		Boolean bo = null;
		switch (fieldName) {
			case "sequence":
				Integer val = Integer.parseInt(value);
				gc.setSequence(val);
				dao.updateById(gc);
				break;
			case "className":
				gc.setClassname(value);
				dao.updateById(gc);
				break;
			case "display":
				bo = Boolean.parseBoolean(value);
				gc.setDisplay(!bo);
				dao.updateById(gc);
				break;
			case "recommend":
				bo = Boolean.parseBoolean(value);
				gc.setRecommend(!bo);
				dao.updateById(gc);
				break;
			case "deletestatus":
				Boolean bo1 = Boolean.parseBoolean(value);
				gc.setDeletestatus(bo1);
				dao.updateById(gc);
				switch (level) {
					case 0:
						dao.updateByPrimaryKeyDeleteFirstClass(id);
						break;
					case 1:
						dao.updateByPrimaryKeyDeleteSecondClass(id);
						break;
				}
				break;
		}
		if (bo != null) {
			out.write(!bo + "");
		}
	}

	public void updateClassBatch(String[] ckbId,String[] ckbed_level, String fieldName) {
		if ("recommend".equals(fieldName)) {
			dao.updateByPrimaryKeyArrayRecomend(ckbId);
		} else if ("deletestatus".equals(fieldName)) {
			for(int i = 0 ;i < ckbed_level.length;i++) {
				Long id = Long.parseLong(ckbId[i]);
				GoodsClass gc = new GoodsClass();
				gc.setId(id);
				gc.setDeletestatus(true);
				dao.updateById(gc);
				switch(ckbed_level[i]) {
				case "0":
					dao.updateByPrimaryKeyDeleteFirstClass(id);
					break;
				case "1":
					dao.updateByPrimaryKeyDeleteFirstClass(id);
					break;
				}
				gc = null;
			}
			
			//dao.updateByPrimaryKeyArrayDelete(ckbId);
		}
	}
	
	
	public void  findSelect(PrintWriter out){
		
		List<GoodsClass1> list = dao1.getFirstClass();
		JSONObject js = new JSONObject();
		js.put("list", list);
		
		out.write(js.toString());
	}
	
	public Boolean verifyClassName(String ClassName) {
		
		List<GoodsClass> list = dao.selectByClassName(ClassName);
		
		
		//用户名存在
		if(list.size() > 0) {
			return false;
		}else {
			return true;
		}
		
	}
	
	public void addOrEditClass(String id,String className,String level,String parentId,String display,String recommend,
			String goodstypeId,String icon_type,String icon_sys,String sequence,String pageType,MultipartFile icon_acc,HttpServletRequest req) {
		GoodsClass gc = new GoodsClass();
		gc.setClassname(className);
		
		Date currentDate = new Date();
		gc.setAddtime(currentDate);
		
		if(level != null && !("".equals(level))) {
			gc.setLevel(Integer.parseInt(level));
		}
		
		if(parentId != null && !("".equals(parentId))) {
			gc.setParentId(Long.parseLong(parentId));
		}
		Boolean display1 = true;
		if("0".equals(display)) {
			display1 = false;
		}
		gc.setDisplay(display1);
		
		Boolean recommend1 = true;
		if("0".equals(recommend)) {
			recommend1 = false;
		}
		gc.setRecommend(recommend1);
		
		if(goodstypeId != null && !("".equals(goodstypeId))) {
			gc.setGoodstypeId(Long.parseLong(goodstypeId.trim()));
		}
		
		if(icon_type != null) {
			gc.setIconType(Integer.parseInt(icon_type));
		}
		
		if(icon_sys != null && !("".equals(icon_sys)) && ("0").equals(level)  ) {
			gc.setIconSys(icon_sys);
		}
		
		if(sequence != null && !("".equals(sequence))) {
			gc.setSequence(Integer.parseInt(sequence));
		}else {
			gc.setSequence(0);
		}
		
		gc.setDeletestatus(false);
		
		
		if(gc.getIconType() == 1) {
			
//			String string = req.getServletContext().getRealPath("/upload/icon_acc");
//			System.out.println(string);
//			System.out.println(icon_acc.getName());//获取到表单的名字
//			System.out.println(icon_acc.getOriginalFilename());//获取上传文件的原始名称
//			System.out.println(icon_acc.getSize());//获取上传文件大小
//			System.out.println(icon_acc.getContentType());//获取上传文件的类型
			String originalFilename = icon_acc.getOriginalFilename();
			String fileName[] = originalFilename.split("\\.");
			String fileType = fileName[fileName.length-1];
			
			SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");//获取当前时间
			String newFileName = className + df.format(currentDate) + "." + fileType;
			
			
			InputStream is = null;
			OutputStream os = null;
			try {
				os = new FileOutputStream("C:\\Users\\Administrator\\git\\eshop\\OnlineMall\\WebContent\\upload\\icon_acc" + "\\" + newFileName);
				//os = new FileOutputStream(string + "\\" + newFileName);
				is = icon_acc.getInputStream();
				IOUtils.copy(is, os);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}finally {
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			gc.setIconAccPath("/upload/icon_acc/" + newFileName);
		}
		
		
		if("add".equals(pageType)) {
			
			dao.insertSelective1(gc);
		}else {
			gc.setId(Long.parseLong(id));
			dao.updateById(gc);
		}
		
		
		
	}
	
	public void selectClassSimple(String id,HttpServletRequest req) {
		Long id2 = Long.parseLong(id);
		List<GoodsClass> gc = dao.selectByidSimple(id2);
		req.getSession().setAttribute("gc", gc.get(0));
	}



}
