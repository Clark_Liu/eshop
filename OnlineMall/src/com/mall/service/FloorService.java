package com.mall.service;

import java.util.List;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mall.dao.Floor_categoryMapper;
import com.mall.dao.Floor_contentMapper;
import com.mall.entity.Floor_category;
import com.mall.entity.Floor_content;

@Service
public class FloorService {
	@Autowired
	private Floor_categoryMapper dao;
	@Autowired
	private Floor_contentMapper  dao2;
	
	
	//查询首页一二级
	public List<Floor_category> index(Integer id){
		return dao.selectForIndex(id);
	}
	//查询首页三级
	public List<Floor_content> index3(Integer id){
		return dao2.selectForIndex3(id);
	}
	//查询一二级分类
	public String floor_category(){
		List<Floor_category> list= dao.selectForFloorCategory();
		JSONObject jo = new JSONObject();
		jo.put("floor_category", list);
		return jo.toString();
	}
	//一二级添加
	public void add(Floor_category fc){
		dao.insertSelective(fc);
	}
	//三级内容添加
	public void add3(Floor_content fcon){
		dao2.insertSelective(fcon);
	}
}
