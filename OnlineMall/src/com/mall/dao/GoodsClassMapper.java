package com.mall.dao;

import java.util.List;


import com.mall.entity.GoodsClass;
import com.mall.entity.GoodsClassWithBLOBs;

public interface GoodsClassMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table shopping_goodsclass
     *
     * @mbggenerated
     */
    int deleteByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table shopping_goodsclass
     *
     * @mbggenerated
     */
    int insert(GoodsClassWithBLOBs record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table shopping_goodsclass
     *
     * @mbggenerated
     */
    int insertSelective(GoodsClassWithBLOBs record);
   
    
    
    int insertSelective1(GoodsClass record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table shopping_goodsclass
     *
     * @mbggenerated
     */
    GoodsClassWithBLOBs selectByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table shopping_goodsclass
     *
     * @mbggenerated
     */
    int updateByPrimaryKeySelective(GoodsClassWithBLOBs record);
    
    
    
    int updateById(GoodsClass gc);

    
    int updateByPrimaryKeyArrayRecomend(String[] array);
    
    
    
    int updateByPrimaryKeyArrayDelete(String[] array);
    
    
    //删除一级分类及其下级所有的分类
    int updateByPrimaryKeyDeleteFirstClass(Long id );
    
    
    //删除二级分类及其下级所有的分类
    int updateByPrimaryKeyDeleteSecondClass(Long id );
    
    
    
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table shopping_goodsclass
     *
     * @mbggenerated
     */
    int updateByPrimaryKeyWithBLOBs(GoodsClassWithBLOBs record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table shopping_goodsclass
     *
     * @mbggenerated
     */
    int updateByPrimaryKey(GoodsClass record);
    
    List<GoodsClass> selectByidSimple(Long id);
    
    
    List<GoodsClass> findAll();
    
    
    List<GoodsClass> selectByParendId(Long pid);
    
    
    
    List<GoodsClass> findFirstClass();
    
    List<GoodsClass> findSecondClass(Long parent_id);
    
    List<GoodsClass> findThirdClass(Long parent_id);
    
    
    List<GoodsClass> selectByClassName(String ClassName);

	int countFirstClass();
    
    
    
}