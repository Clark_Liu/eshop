package com.mall.utils;

public class IsNumberUtils {

	public static boolean isNumber(String str) {
		boolean re = true;
		for (int i = 0; i < str.length(); i++) {
			if (!Character.isDigit(str.charAt(i))) {
				re = false;
				break;
			}
		}
		return re;
	}

}
