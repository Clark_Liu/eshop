package com.mall.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 所有的请求都走进这个controller,然后由这个DispatchController进行请求分发
 * 这样做的好处是: 以后除了index页以外都可以放在jsp目录下,方便管理,而不是在根目录下
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/dispatcher")
public class DispatchController {
	@RequestMapping("/dispatch")
	public String dispatch(String dispatch_url) {
		return dispatch_url;
		
	}
}
