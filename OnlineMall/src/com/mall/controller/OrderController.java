package com.mall.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.catalina.Session;
import org.apache.taglibs.standard.lang.jstl.test.beans.PublicInterface2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.EnableLoadTimeWeaving;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.mall.entity.Orders;
import com.mall.service.OrderService;

import net.sf.json.JSON;
import net.sf.json.JSONObject;

@Controller
@RequestMapping("/orders")
public class OrderController {
	@Autowired
	private OrderService service;
	
	@RequestMapping("/findAll")
	public ModelAndView findAll(Integer currentPage,HttpSession session) {
		ModelAndView mv = new ModelAndView("shopping_orderManage_list_templete.jsp");
		List<Orders> list = service.findAll(currentPage,session);
		
		mv.addObject("list", list);
		
		return mv;
	}
	
	@ResponseBody
	@RequestMapping("/searchOrder")
	public ModelAndView searchOrder(Integer stateHidden,String billcodeHidden,
			String begintimeHidden,String endtimeHidden,
			Integer beginpriceHidden,Integer endpriceHidden,Integer currentPage,HttpSession session) {
		List<Orders> list = service.searchOrder(stateHidden,billcodeHidden,begintimeHidden,endtimeHidden,beginpriceHidden,endpriceHidden,session,currentPage);
		ModelAndView mv = new ModelAndView("shopping_orderManage_list_templete.jsp");
		mv.addObject("list", list);
		return mv;
	}
}
