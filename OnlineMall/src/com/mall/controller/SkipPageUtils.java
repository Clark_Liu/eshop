package com.mall.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/SkipPageUtils")
public class SkipPageUtils {
	
	@RequestMapping("/skipPage")
	public ModelAndView skipPage(String url,Integer currentPage,HttpServletRequest req) {
		ModelAndView mv = new ModelAndView("common_fenye_templete.jsp");
		Integer totalPageNum = (Integer)req.getSession().getAttribute("totalPageNum");
		mv.addObject("totalPageNum",totalPageNum);
		mv.addObject("currentPage",currentPage);
		mv.addObject("url",url);
		return mv;
	}
	
	
	
	@RequestMapping("/skipPageSearch")
	public ModelAndView skipPageSearch(String url,Integer currentPage,HttpServletRequest req) {
		ModelAndView mv = new ModelAndView("common_fenye_templete_search.jsp");
		Integer totalPageNum = (Integer)req.getSession().getAttribute("totalPageNum");
		mv.addObject("totalPageNum",totalPageNum);
		mv.addObject("currentPage",currentPage);
		return mv;
	}
	
	@RequestMapping("/skipPageSearch2")
	public ModelAndView skipPageSearch2(String url,Integer currentPage,HttpServletRequest req) {
		ModelAndView mv = new ModelAndView("common_fenye_templete_search2.jsp");
		Integer totalPageNum = (Integer)req.getSession().getAttribute("totalPageNum");
		mv.addObject("totalPageNum",totalPageNum);
		mv.addObject("currentPage",currentPage);
		return mv;
	}
}
