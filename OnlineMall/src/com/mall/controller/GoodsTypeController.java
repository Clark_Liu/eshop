package com.mall.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.mall.entity.GoodsSpecIfication;
import com.mall.entity.GoodsType;
import com.mall.entity.Goods_Fjs_BrandCategory;
import com.mall.entity.Goods_Fjs_GoodsTypeProperty;
import com.mall.entity.Goods_Fjs_Type_Brand;
import com.mall.entity.Goods_Fjs_Type_Spec;
import com.mall.service.GoodsTypeService;

@Controller
@RequestMapping("/goodstype")
public class GoodsTypeController {
	
	@Autowired
	private GoodsTypeService service;
	
	@RequestMapping("/findall")//查找类型
	public ModelAndView selectAll(int currentPage,HttpServletRequest req)
	{
		ModelAndView mv = new ModelAndView("goods_type_list_list_templete.jsp");
//		List<GoodsType> list = service.selectAll();
		List<GoodsType> list = service.service(currentPage,req);
		mv.addObject("goodsdata", list);
		return mv;
	}
	
	@RequestMapping("/deleteone")//删除类型
	public void deleteOne(Integer id)
	{
		service.deleteone(id);
	}
	
	@RequestMapping("/ification")//查找规格名和规格值
	public ModelAndView selectification()
	{
		ModelAndView mv = new ModelAndView("goods_type_add_list_templete.jsp");
		List<GoodsSpecIfication> list = service.selectiAll();
		mv.addObject("ificationdata", list);
		return mv;
	}
	
	@RequestMapping("/brand")//查找品牌分类和品牌名
	public ModelAndView selectbrand()
	{
		ModelAndView mv = new ModelAndView("goods_type_add_list_templete_brand.jsp");
		List<Goods_Fjs_BrandCategory> list = service.selectbrand();
		mv.addObject("branddata", list);
		return mv;
	}
	
	@RequestMapping("/yanzhengname")//验证名字是否存在
								   //从goods_type_add.html传过来的name值
	public ModelAndView yanzhengname(String name)
	{
		ModelAndView mv = new ModelAndView("goods_type_add_list_templete_yanzheng.jsp");
		List<GoodsType> list = service.selectAll();
		mv.addObject("yanzhenglist", list);
		mv.addObject("yanzheng", name);
		return mv;
	}
	
	@RequestMapping("/addtype")
	public ModelAndView addtype(String name,Integer sequence)
	{
		service.addtype(name,sequence);
		ModelAndView mv = new ModelAndView("goods_type_add_list_templete_typeid.jsp");
		List<GoodsType> list = service.selectAll();
		mv.addObject("addtype", list);
		mv.addObject("yanzheng", name);
		return mv;
	}
	
	@RequestMapping("/uptype")//编辑修改类型名称
	public ModelAndView uptype(String id,String name,String sequence)
	{
		long tid = Integer.parseInt(id);
		Integer tsequence = Integer.parseInt(sequence);
		service.uptype(tid,name,tsequence);
		ModelAndView mv = new ModelAndView("goods_type_add_list_templete_typeid.jsp");
		List<GoodsType> list = service.selectAll();
		mv.addObject("addtype", list);
		mv.addObject("yanzheng", name);
		return mv;
	}
	
	@RequestMapping("/addtands")
	public void addtands(String spec_id,String type_id)
	{
		long specid = Integer.parseInt(spec_id);
		long typeid = Integer.parseInt(type_id);
		service.addtands(typeid, specid);
	}
	
	@RequestMapping("/addtandb")
	public void addtandb(String brand_id,String type_id)
	{
		long brandid = Integer.parseInt(brand_id);
		long typeid = Integer.parseInt(type_id);
		service.addtandb(typeid, brandid);
	}
	
	@RequestMapping("/addsx")
	public String addsx(String name,String value,String sequence,String display,String type_id)
	{
		Boolean displayd = Boolean.parseBoolean(display);
		Integer sequences = Integer.parseInt(sequence);
		long typeid = Integer.parseInt(type_id);
		service.addsx(name,value,sequences,displayd,typeid);
		return "goods_type_add_list_templete_tiaozhuan.jsp";
	}
	
	@RequestMapping("/upsx")
	public void upsx(String id,String name,String value,String sequence,String display,String type_id)
	{
		long Id = Integer.parseInt(id);
		Boolean displayd = Boolean.parseBoolean(display);
		Integer sequences = Integer.parseInt(sequence);
		long typeid = Integer.parseInt(type_id);
		service.upsx(Id,name,value,sequences,displayd,typeid);
	}
	
	@RequestMapping("/hqspec")
	public ModelAndView hqspec()
	{
		ModelAndView mv = new ModelAndView("goods_type_edit_list_templete_spec.jsp");
		List<Goods_Fjs_Type_Spec> list = service.hqspec();
		mv.addObject("hqspec", list);
		return mv;
	}
	
	@RequestMapping("/hqbrand")
	public ModelAndView hqbrand()
	{
		ModelAndView mv = new ModelAndView("goods_type_edit_list_templete_brand.jsp");
		List<Goods_Fjs_Type_Brand> list = service.hqbrand();
		mv.addObject("hqbrand", list);
		return mv;
	}
	
	@RequestMapping("/skipPage")
	public ModelAndView skipPage(Integer currentPage,HttpServletRequest req) {
		ModelAndView mv = new ModelAndView("goods_type_list_fenye_templete.jsp");
		Integer totalPageNum = (Integer)req.getSession().getAttribute("totalPageNum");
		mv.addObject("totalPageNum",totalPageNum);
		mv.addObject("currentPage",currentPage);
		return mv;
	}
	
	@RequestMapping("/hqshuxing")
	public ModelAndView hqshuxing(String type_id)
	{
		ModelAndView mv = new ModelAndView("goods_type_edit_list_templete_hqshuxing.jsp");
		List<Goods_Fjs_GoodsTypeProperty> list = service.hqshuxing();
		mv.addObject("hqshuxing", list);
		mv.addObject("type_id", type_id);
		return mv;
	}
	
}
