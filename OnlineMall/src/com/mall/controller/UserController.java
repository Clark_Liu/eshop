package com.mall.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.mall.entity.Captcha;
import com.mall.entity.User;
import com.mall.service.UserServce;
import com.sun.org.apache.xalan.internal.xsltc.compiler.sym;

import net.sf.json.JSONObject;

@Controller
@RequestMapping("/user")
public class UserController {
	@Autowired
	private UserServce service;
	
	//生成验证码
	@RequestMapping("/creatCaptcha")
	public void creatCaptcha(HttpServletResponse resp, HttpSession session) throws IOException {
        resp.setContentType("image/jpg");
        int width = 59;
        int height = 27;
        Captcha c = Captcha.getInstance();
        c.set(width, height);
        String checkcode = c.generateCheckcode();
        // 验证码放入session
        session.setAttribute("cCode", checkcode);
        OutputStream os = resp.getOutputStream();
        ImageIO.write(c.generateCheckImg(checkcode), "jpg", os);
	}
	
	//登录验证功能
	@RequestMapping("/login")
	public String  login(String username,String password,String code,HttpSession session) {
		String cCode = (String) session.getAttribute("cCode");
		
		if(code == "") {
			return "Login.jsp";
		}else if (!code.equalsIgnoreCase(cCode)) {
			return "Login.jsp?warnning=code";
		}else {
			int i = service.login(username, password,session);
			if(i == 0) {
				return "Login.jsp?warnning=password";
			}else if(i == -1) {
				return "Login.jsp?warnning=username";
			}
			session.setAttribute("isLogin", true);
			return "kill_index.jsp";
		}
	}
	
	//查询所有
	@RequestMapping("/findAll")
	public ModelAndView findAll(int currentPage,HttpServletRequest req) {
		ModelAndView mv = new ModelAndView("userManage_templete.jsp");
		List<User> list = service.findAll(currentPage,req);
		
		mv.addObject("list",list);
		return mv;
	}
	
	//按照 id 查找
	@RequestMapping("/findSimple")
	public void findSimple(String id,PrintWriter out) {
		Long i = Long.parseLong(id);
		User u = service.findSimple(i);
		
		JSONObject js = new JSONObject();
		js.put("u", u);
		out.write(js.toString());
	}
	
	
	//模糊搜索
	@RequestMapping("/search")
	public ModelAndView search(Integer currentPage,String username,String tel,String begintime,String endtime,HttpServletRequest req,HttpSession session ) {
		ModelAndView mv = new ModelAndView("userManage_templete.jsp");
		
		
		List<User> list = service.search(currentPage,username,tel,begintime,endtime,req);
		mv.addObject("list",list);
		mv.addObject("currentPage",currentPage);
		return mv;
		
	}
	
	@RequestMapping("/exit")
	public String exit(HttpSession session) {
		session.setAttribute("isLogin",false);
		return "Login.jsp";
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
/*	//登录验证及验证码验证
    @RequestMapping("login")
    public ModelAndView login(String account, String password, String checkcode,HttpSession session) {
        // session.setMaxInactiveInterval(20);
        ModelAndView mav = null;
        PageData pd = new PageData();
        // 获取session中的验证码
        String cCode = (String) session.getAttribute("cCode");
        log.info("用户输入的验证码是：" + checkcode + "，内存中保存的验证码是：" + cCode);
        // 验证码为null则跳转登录界面
        if (checkcode == null) {
            mav = new ModelAndView("login");
        }
        else if (!checkcode.equalsIgnoreCase(cCode)) {// 比对验证码，忽视大小写。验证码错误则返回登录界面
            pd.setSuccess(false);
            pd.setMessage("验证码错误");
            mav = new ModelAndView("login");
            mav.addObject("pd", pd);
        }
        return mav;
        }
    }
	*/
	
	
	
	
	
	
	
}
