package com.mall.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.UUID;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.mall.entity.GoodsSpecIfication;
import com.mall.entity.GoodsSpecProperty;
import com.mall.entity.Goods_brand;
import com.mall.service.GoodsSpecService;

@Controller
@RequestMapping("/goodsspec")
public class GoodsSpecController {
	
	@Autowired
	private GoodsSpecService service;
	
	@RequestMapping("/ification")//���ҹ�����͹��ֵ
	public ModelAndView selectification(int currentPage,HttpServletRequest req)
	{
		ModelAndView mv = new ModelAndView("goods_spec_list_list_templete.jsp");
//		List<GoodsSpecIfication> list = service.selectiAll();
		List<GoodsSpecIfication> list = service.service(currentPage,req);
		mv.addObject("ificationdata", list);
		return mv;
	}
	
	@RequestMapping("/skipPage")
	public ModelAndView skipPage(Integer currentPage,HttpServletRequest req) {
		ModelAndView mv = new ModelAndView("goods_spec_list_fenye_templete.jsp");
		Integer totalPageNum = (Integer)req.getSession().getAttribute("totalPageNum");
		mv.addObject("totalPageNum",totalPageNum);
		mv.addObject("currentPage",currentPage);
		return mv;
	}
	
	@RequestMapping("/deleteone")//ɾ�������
	public void deleteOne(Integer id)
	{
		service.deleteone(id);
	}
	
	@RequestMapping("/yanzhengname")//��֤�����Ƿ����
	   //��goods_spec_add.html��������nameֵ
	public ModelAndView yanzhengname(String name)
	{
		ModelAndView mv = new ModelAndView("goods_spec_add_list_templete_yanzheng.jsp");
		List<GoodsSpecIfication> list = service.selectAll();
		mv.addObject("yanzhenglist", list);
		mv.addObject("yanzheng", name);
		return mv;
	}
	
	@RequestMapping("/addifi")
	public ModelAndView addtype(String name,Integer sequence,String type)
	{
		service.addifi(name,sequence,type);
		ModelAndView mv = new ModelAndView("goods_spec_add_list_templete_specid.jsp");
		List<GoodsSpecIfication> list = service.selectAll();
		mv.addObject("addifi", list);
		mv.addObject("yanzheng", name);
		return mv;
	}
	
	@RequestMapping("/addgg")
	public ModelAndView addgg(String name,String sequence,String img,String spec_id)
	{
		Integer sequences = Integer.parseInt(sequence);
		long specId = Integer.parseInt(spec_id);
		service.addgg(name,sequences,img,specId);
		ModelAndView mv = new ModelAndView("goods_spec_add_list_templete_imgid.jsp");
		mv.addObject("imgid", img);
		return mv;
	}
	
	@RequestMapping("/selectimg")
	public ModelAndView selectimg()
	{
		ModelAndView mv = new ModelAndView("goods_spec_add_list_templete_selectimg.jsp");
		List<GoodsSpecProperty> list = service.selectimg();
		mv.addObject("selectimg", list);
		return mv;
	}
	
	@RequestMapping("/form")
	public void updateAndfileupload(HttpServletRequest req) 
	{
		MultipartHttpServletRequest mreq = (MultipartHttpServletRequest) req;
		MultipartFile file = mreq.getFile("specImage");
		String fname = file.getOriginalFilename();
		ServletContext servletContext = req.getServletContext();
		String realPath1 = servletContext.getRealPath("upload/spec");
		File filenew = new File(realPath1 + "/" + fname);
		
		String realPath2 = servletContext.getRealPath("E:\\OnlineMall\\WebContent\\upload\\spec");
		File filenew2 = new File(realPath2 + "/" + fname);
		try {
			filenew.createNewFile();
			filenew2.createNewFile();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			BufferedOutputStream bs = new BufferedOutputStream(
					new FileOutputStream(filenew));
			bs.write(file.getBytes());
			bs.flush();
			bs.close();
			BufferedOutputStream bs2 = new BufferedOutputStream(
					new FileOutputStream(filenew2));
			bs2.write(file.getBytes());
			bs2.flush();
			bs2.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@RequestMapping("/findAll")
	public void findAll(PrintWriter out) {
		service.findAll(out);
	}
	
	@RequestMapping("/ajaxSearchSpec_value")
	public void ajaxSearchSpec_value(Long specnameId,PrintWriter out ) {
		service.findBySpecId(specnameId,out);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
