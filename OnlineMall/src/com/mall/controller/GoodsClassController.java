package com.mall.controller;

import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.mall.entity.GoodsClass;
import com.mall.service.GoodsClassService;

@Controller
@RequestMapping("/goodsclass")
public class GoodsClassController {
	@Autowired
	private GoodsClassService service;
	
	@RequestMapping("/findAll")
	public ModelAndView findAll(int currentPage,HttpServletRequest req) {
		ModelAndView mv = new ModelAndView("category_list_templete.jsp");
		List<GoodsClass> list = service.service(currentPage,req);
		mv.addObject("goodsdata", list);
		return mv;
	}
	
	@RequestMapping("/skipPage")
	public ModelAndView skipPage(Integer currentPage,HttpServletRequest req) {
		ModelAndView mv = new ModelAndView("category_list_fenye_templete.jsp");
		Integer totalPageNum = (Integer)req.getSession().getAttribute("totalPageNum");
		mv.addObject("totalPageNum",totalPageNum);
		mv.addObject("currentPage",currentPage);
		
		return mv;
	}
	
	
	@RequestMapping("/findByPid")
	public ModelAndView findByParentId(Long pid,Long ppid) {
		ModelAndView mv = new ModelAndView("category_list_templete23.jsp");
		List<GoodsClass> list = service.serviceByParentId(pid);
		mv.addObject("level23data", list);
		mv.addObject("ppid",ppid);
		return mv;
	}
	
	
	
	@RequestMapping("/updateClass")
	public void updateClassNameAndSequence(Long id,Integer level,String fieldName,String value,PrintWriter out ) {
		
		 service.updateBy(id,level,fieldName,value,out);
		
	}
	
	@ResponseBody
	@RequestMapping("/updateClassBatch")
	public void updateClassBatch(String[] ckbId,String[] ckbed_level,String fieldName) {
		service.updateClassBatch(ckbId,ckbed_level, fieldName);
		
	}
	
	@RequestMapping("/findSelectClass")
	public void findSelectClass(PrintWriter out) {
		service.findSelect(out);
	}
	
	//添加类别名称中的验证类别名称
	@ResponseBody
	@RequestMapping("/verifyClassName")
	public void verifyClassName1(String className,PrintWriter out) {
		
		if(className != null) {
			out.print(service.verifyClassName(className));
		}else {
			out.print(true);
		}
		
	}
	
	
	
	//添加类别名称中的验证类别名称
	@ResponseBody
	@RequestMapping("/verifyFileType")
	public void verifyFileType(String file,PrintWriter out) {
		
		String fileName[] = file.split("\\.");
		String fileType = fileName[fileName.length-1];
		
		switch(fileType) {
		case"jpg":
		case"png":
		case"gif":
			out.print(true);
			break;
		default:
			out.print(false);
		}
	}
	
	
	@RequestMapping("/addOrEditClass")
	public String createClass(String id,String className,String level,String parentId,String display,String recommend,
			String goodstypeId,String icon_type,String icon_sys,String sequence,String pageType,MultipartFile icon_acc,HttpServletRequest req) {
		
		
		service.addOrEditClass(id,className,level,parentId, display, recommend,
				 goodstypeId, icon_type, icon_sys, sequence,pageType,icon_acc,req);
		return "/goods_class_list_addSeccess.html";
	}
	@RequestMapping("/selectClassSimple")
	public String selectClassSimple(String id,HttpServletRequest req) {
		service.selectClassSimple(id,req);
		
		return "/category_editPage.jsp";
	}
	
	@RequestMapping("/fileUpload")
	public void upload() {
		
	}
	
	
	
}
