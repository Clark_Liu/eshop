package com.mall.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.mall.entity.Floor_category;
import com.mall.entity.Floor_content;
import com.mall.service.FloorService;

@Controller
@RequestMapping("/floor")
public class FloorController {
	@Autowired
	private FloorService service;

	// 查询分类
	@RequestMapping("/floor_category")
	public void floor_category(PrintWriter out) {
		out.write(service.floor_category());
	}

	// 首页显示
	@RequestMapping("/index")
	public ModelAndView index(Integer id, Integer level) {
		System.out.println(id + "##########");
		List<Floor_category> list = new ArrayList<>();
		ModelAndView mv = new ModelAndView("shopping_floor_list_mv1.jsp");
		if (id != null) {
			if (level == 0) {
				System.out.println("二级显示");
				list = service.index(id);
			} else {
				System.out.println("三级内容显示");
				List<Floor_content> list3 = new ArrayList<>();
				list3 = service.index3(id);
				mv.addObject("floor1", list3);
				return mv;
			}
		} else {
			System.out.println("一级显示");
			list = service.index(0);
		}
		mv.addObject("floor1", list);
		return mv;
	}

	// 分发添加 编辑标记
	@RequestMapping("/shopping_floor_add")
	public String brand_edit(Long id, HttpSession session, String flg,
			Integer pid, Integer level) {
		System.out.println("进入楼层添加分发controller");
		if ("eidt".equals(flg)) {
			System.out.println("进入编辑功能");
			session.setAttribute("flg", "eidt");
		} else {
			System.out.println("进入添加功能");
			if (pid != null) {
				session.setAttribute("level", level);
				session.setAttribute("pid", pid);
			}else{
				session.setAttribute("level", null);
				session.setAttribute("pid", null);
			}
			session.setAttribute("flg", "add");
		}
		return "shopping_floor_add.jsp";
	}

	// 增加
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String add(HttpServletRequest req, Integer id, Integer level,
			Integer show_mv, String floorname, String url, Boolean yesnoshow,
			Double price, Integer seq) {
		System.out.println("level=" + level);
		System.out.println("name=" + floorname);
		MultipartHttpServletRequest mreq = (MultipartHttpServletRequest) req;
		MultipartFile file = mreq.getFile("file");
		String fname = file.getOriginalFilename();// 获取上传文件原名
		// System.out.println("原文件名" + fname);
		if (fname != null && fname != "") {
			// System.out.println("###################################可判断");
			ServletContext servletContext = req.getServletContext();// 得到上下文对象
			// 获取上传路径
			String realPath1 = servletContext.getRealPath("upload/floor");
			// System.out.println(realPath1);
			File filenew = new File(realPath1 + "/" + fname);
			// System.out.println(filenew);
			try {
				filenew.createNewFile();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				BufferedOutputStream bs = new BufferedOutputStream(
						new FileOutputStream(filenew));
				bs.write(file.getBytes());
				bs.flush();
				bs.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (level != 1) {
			Floor_category fc = new Floor_category();
			fc.setLevel(level + 1);// 级别
			fc.setDisplay(yesnoshow);// 显示
			fc.setImage(fname);// 上传图片名
			fc.setMvId(show_mv);// 显示方式
			fc.setName(floorname);// 楼层名字
			fc.setDeletestatus(false);// 数据状态
			fc.setSequence(seq);// 排序
			fc.setUrl(url);// 商品路径
			if (id != null) {
				fc.setPid(id);
				service.add(fc);
				System.out.println("添加1表第二级");
			} else {
				fc.setPid(0);// 父级id
				service.add(fc);
				System.out.println("添加1表第一级");
			}
		} else {
			Floor_content fc1 = new Floor_content();
			System.out.println(floorname + "");
			fc1.setLevel(level + 1);
			fc1.setDisplay(yesnoshow);
			fc1.setImage(fname);
			fc1.setPrice(price);
			fc1.setPid(id);
			fc1.setSequnce(seq);
			fc1.setName(floorname);
			fc1.setUrl(url);
			fc1.setMvId(show_mv);
			fc1.setDeletestatus(false);

			System.out.println("传进值=" + fc1.getName());
			service.add3(fc1);
			System.out.println("添加表2");
		}
		return "shopping_floor_list.jsp";
	}

}
