package com.mall.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.mall.entity.Goods_brand;
import com.mall.service.GoodsBrandService;

import net.sf.json.JSONObject;

@Controller
@RequestMapping("/brand")
public class GoodsBrandController {

	@Autowired
	private GoodsBrandService service;

	// 主页显示功能 搜索功能
	@RequestMapping("/selectall")
	public ModelAndView selectall(String pagenum, String name, String category,
			HttpSession session) {
		List<Goods_brand> list = new ArrayList<>();
		int num = Integer.parseInt(pagenum);
		if (name == "" && category == "") {
			list = service.fenye(num, session);
			session.setAttribute("FenYeFlg", "0");
			// list = service.fenye(1, session);
		} else {
			session.setAttribute("FenYeFlg", "1");
			System.out.println("进入模糊查询");
			Goods_brand gb = new Goods_brand();
			gb.setName(name);
			gb.setBrandCategory(category);
			list = service.search1(num, session, gb);
			// list = service.search(gb);
			// list = service.fenye(num, session);
		}

		ModelAndView mv = new ModelAndView("brand_list_mv.jsp");
		mv.addObject("brandlist", list);
		return mv;
	}
	
	//商品页面的下拉列表 
	@RequestMapping("/findAllSelected")
	public void findAll(PrintWriter out) {
		
		List<Goods_brand> list = service.selectall();
		JSONObject js = new JSONObject();
		js.put("list", list);
		
		out.write(js.toString());
		
	}
	
	
	
	
	
	
	
	
	
	
	// 返回翻页数据的单一
	@RequestMapping("/fenye1")
	public ModelAndView fanye(String pagenow, HttpSession session) {
		ModelAndView mv = new ModelAndView("goods_brand_fenyeMV.jsp");
		mv.addObject("pagenum", session.getAttribute("pagenum1"));
		System.out.println("controller的设置当前页="+ session.getAttribute("pagenow"));
		mv.addObject("currentPage", session.getAttribute("pagenow"));
		return mv;
	}

	// 翻页功能
	@RequestMapping("/fenye")
	public ModelAndView selectall(String pagenum, HttpSession session,
			String name, String category) {
		System.out.println("进入分页controller方法名为fenye");
		List<Goods_brand> list = new ArrayList<>();
		//System.out.println("当前页码" + pagenum + "#####");
		int num = Integer.parseInt(pagenum);
		if ("0".equals(session.getAttribute("FenYeFlg"))) {
			list = service.fenye(num, session);
		} else {
			Goods_brand gb = new Goods_brand();
			gb.setName(name);
			gb.setBrandCategory(category);
			list = service.search1(num, session, gb);
		}
		ModelAndView mv = new ModelAndView("brand_list_mv.jsp");
		mv.addObject("brandlist", list);
		return mv;
	}

	// 主页面修改
	@RequestMapping("/update")
	public void update(Long id, String fieldName, String value, PrintWriter out) {
		System.out.println("修改功能controller");
		String ret = service.update(id, fieldName, value);
		if (ret != null) {
			out.write(ret);
		}
	}

	//删除功能
	@RequestMapping("/delete")
	public String delete(Long mulitId) {
		System.out.println("单个删除");
		service.update(mulitId, "delete", null);
		return "goods_brand_list.html";
	}

	// 批量删除
	@RequestMapping("/deleteall")
	public void deleteall(String[] id, PrintWriter out) {
		System.out.println("进入批量删除controller");
		if (id == null) {
			System.out.println("kong1111111111");
		}
		if (service.deleteall(id)) {
			System.out.println("###########11111");
			out.write("1");
		} else {
			out.write("0");
		}
	}

	// 编辑功能
	@RequestMapping("/goods_brand_edit.htm")
	public String brand_edit(Long id, HttpSession session, String flg) {
		System.out.println("进入品牌编辑controller");
		if ("eidt".equals(flg)) {
			System.out.println("进入编辑功能");
			session.setAttribute("brand_edit", service.brand_edit(id));
			session.setAttribute("style", "eidt");
		} else {
			System.out.println("进入添加功能");
			session.setAttribute("brand_edit", null);
			session.setAttribute("style", "add");
		}
		return "goods_brand_add.jsp";
	}

	// 文件上传
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public String updateAndfileupload(HttpServletRequest req, String name,
			String first_word, String cat_name, boolean recommend,
			Integer sequence, Long id, String brand_type) {
		Goods_brand gb = new Goods_brand();
		//System.out.println(name + "+++++" + first_word + "+++++" + cat_name
		//		+ "+++" + recommend + "++++" + sequence + "++++" + id);

		MultipartHttpServletRequest mreq = (MultipartHttpServletRequest) req;
		MultipartFile file = mreq.getFile("brandLogo");
		String fname = file.getOriginalFilename();// 获取上传文件原名
		//System.out.println("原文件名" + fname);
		if (fname != null && fname != "") {
			System.out.println("###################################可判断");
			gb.setBrandlogoImg(fname);
			ServletContext servletContext = req.getServletContext();// 得到上下文对象
			// 获取上传路径
			String realPath1 = servletContext.getRealPath("upload/brand");
			//System.out.println(realPath1);
			File filenew = new File(realPath1 + "/" + fname);
			//System.out.println(filenew);
			try {
				filenew.createNewFile();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				BufferedOutputStream bs = new BufferedOutputStream(
						new FileOutputStream(filenew));
				bs.write(file.getBytes());
				bs.flush();
				bs.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		gb.setName(name);
		gb.setFirstWord(first_word);
		gb.setBrandCategory(cat_name);
		gb.setRecommend(recommend);
		gb.setSequence(sequence);		
		if (id == null) {
			gb.setDeletestatus(false);
			service.insert(gb);

		} else {
			gb.setId(id);
			service.updateAndfileupload(gb);

		}
		return "goods_brand_list.jsp";
	}

	// 表单验证
	@RequestMapping({ "/admin/goods_brand_verify.htm" })
	public void goods_brand_verify(HttpServletRequest request,
			HttpServletResponse response, String name, String id) {
		System.out.println("表单验证controller");
		//System.out.println("name=" + name + "####" + "id=" + id);
		boolean ret = true;
		Goods_brand gb = new Goods_brand();
		Goods_brand gb1 = new Goods_brand();
		if (name != null || name != "") {
			gb.setName(name);
			gb1 = service.selectforForm(gb);
		}
		if (id == null || id == "") {// 判断为添加功能
			if (gb1 != null) {
				System.out.println("判断添加功能时候品牌是否存在");
				ret = false;
			}
		} else {// 判断为编辑功能
			System.out.println("判断编辑功能时候品牌是否存在");
			String id1 = gb1.getId().toString();
			if (id.equals(id1)) {
				ret = true;
			} else {
				ret = false;
			}
		}

		try {
			PrintWriter writer = response.getWriter();
			writer.print(ret);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
