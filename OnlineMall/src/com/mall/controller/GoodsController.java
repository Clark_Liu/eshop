package com.mall.controller;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.mall.entity.Goods;
import com.mall.service.GoodsService;
import com.sun.org.apache.xalan.internal.xsltc.compiler.sym;

@Controller
@RequestMapping("/goods")
public class GoodsController {
	
	@Autowired
	private GoodsService service; 
	
	@RequestMapping("/findAll")
	public ModelAndView findAll(HttpSession session,Integer currentPage) {
		ModelAndView mv = new ModelAndView("shopping_goodsManage_list_templete.jsp");
		List<Goods> list = service.findAll(session,currentPage);
		
		mv.addObject("list",list);
		
		return mv;
	}
	
	
	@RequestMapping("/search")
	public ModelAndView search(String goodsname,Long brandId,Long sortId,Integer recommend,Integer sortLevel,String sortName,Integer currentPage,HttpSession session) {
		ModelAndView mv = new ModelAndView("shopping_goodsManage_list_templete.jsp");	
		List<Goods> list = service.search(goodsname,brandId,sortId,recommend,sortLevel,sortName,currentPage,session);
		
		mv.addObject("list", list);
		
		return mv;
	}
	
	
	@RequestMapping("/updateGoodsName")
	public void updateGoodsName(Long id,String value,PrintWriter out) {
		
		if(value == null || "".equals(value)) {
			out.write("namenull");
		}else {
			int i = service.updateGoodsName(id,value);
			if(i ==0) {
				out.write("repeat");
			}
		}
	}
	
	@RequestMapping("/updateGoodsRecommend")
	public void updateGoodsRecommend(Long id,String val,PrintWriter out) {
		service.updateGoodsRecommend(id,val);
		if("no".equals(val)) {
			out.write("yes");
		}else {
			out.write("no");
		}
	}
	
	@RequestMapping("/deleteGoodsOne")
	public void deleteGoodsOne(Long id,PrintWriter out) {
		service.deleteGoodsOne(id);
		out.write("1");
	}
	

	
	/**
	 * 
	 * @param goodsname 商品名
	 * @param file 商品图片
	 * @param artnumber 商品货号
	 * @param stock 库存
	 * @param originalprice 原价
	 * @param buyprice 进价
	 * @param sellprice 售价
	 * @param brandId 品牌名
	 * @param sortId 分类名
	 * @param specnameId 规格名一
	 * @param specvalueId 规格值二
	 * @param specnameId2 规格名二
	 * @param specvalueId2 规格值三
	 * @param recommend 是否推荐
	 * @param state 是否上架
	 * @param content 商品详情
	 */
	@RequestMapping("/goodsAddOrEdit")
	public String  goodsAddOrEdit(String goodsname,MultipartFile file,Long artnumber,
			Long stock,Double originalprice,Double buyprice,Double sellprice,
			String brandId,String sortId,String[] specnameId,String[] specvalueId,
			Integer recommend,Integer state,String content,String method,Long id) {
		service.goodsAddOrEdit(goodsname,file,artnumber,stock,originalprice,
				buyprice,sellprice,brandId,sortId,specnameId,specvalueId,recommend,state,content,method,id);
		return "/shopping_goods_addSeccess.html";
		/*	System.out.println(goodsname);
		System.out.println(file.getOriginalFilename());
		System.out.println(artnumber);
		System.out.println(stock);
		System.out.println(originalprice);
		System.out.println(buyprice);
		System.out.println(sellprice);
		System.out.println(brandId);
		System.out.println(sortId);
		System.out.println(Arrays.toString(specnameId));
		System.out.println(Arrays.toString(specvalueId));
		System.out.println(recommend);
		System.out.println(state);
		System.out.println(content);
		System.out.println("--------------------");*/
	}
	
	@RequestMapping("/checkGoodName")
	public void checkGoodName(String goodsname,String goodsnameOld,PrintWriter out) {
		if(goodsname.equals(goodsnameOld)) {
			out.print(true);
		}else {
			Boolean b = service.checkGoodName(goodsname);
			out.print(b);
		}
	}
	
	@RequestMapping("/checkArtnumber")
	public void checkArtnumber(Long artnumber,Long id,PrintWriter out) {
		if(artnumber == id) {
			out.print(true);
		}else {
			Boolean b = service.checkArtnumber(artnumber);
			out.print(b);
		}
	}
	
	@RequestMapping("/checkSort")
	public void checkSort(Long sortId,PrintWriter out) {
		Boolean b = service.checkSort(sortId);
		out.print(b);
	}
	
	@RequestMapping("/selectByGoodsId")
	public void selectByGoodsId(Long goodsid,PrintWriter out) {
		service.selectByGoodsId(goodsid,out);
	}
	
	@RequestMapping("/goodsEdit")
	public void  goodsEdit(Long id,String goodsname,MultipartFile file,Long artnumber,
			Long stock,Double originalprice,Double buyprice,Double sellprice,
			String brandId,String sortId,String[] specnameId,String[] specvalueId,
			Integer recommend,Integer state,String content) {
	/*	service.goodsEdit(id,goodsname,file,artnumber,stock,originalprice,buyprice,sellprice,
				brandId,sortId,specnameId,specvalueId,recommend,state,content);*/
		
		
		
		
		System.out.println(id);
		System.out.println(goodsname);
		System.out.println(file.getOriginalFilename());
		System.out.println(artnumber);
		System.out.println(stock);
		System.out.println(originalprice);
		System.out.println(buyprice);
		System.out.println(sellprice);
		System.out.println(brandId);
		System.out.println(sortId);
		System.out.println(Arrays.toString(specnameId));
		System.out.println(Arrays.toString(specvalueId));
		System.out.println(recommend);
		System.out.println(state);
		System.out.println(content);
	}
}
