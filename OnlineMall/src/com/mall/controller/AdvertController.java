package com.mall.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.mall.entity.Shopping_advert;
import com.mall.service.ShoppingAdvertService;

@Controller
@RequestMapping("/advert")
public class AdvertController {
	@Autowired
	private ShoppingAdvertService service;

	// 编辑或添加功能
	@RequestMapping("/shopping_advert_edit.htm")
	public String brand_edit(Integer id, HttpSession session, String flg) {
		System.out.println("进入广告编辑或增加controller");
		if ("eidt".equals(flg)) {
			System.out.println("进入编辑功能");
			Shopping_advert sa = new Shopping_advert();
			sa = service.eidtnow(id);
			System.out.println(sa.getRecommend());
			session.setAttribute("advert_edit", sa);
			session.setAttribute("style", "eidt");
		} else {
			System.out.println("进入添加功能");
			session.setAttribute("advert_edit", null);
			session.setAttribute("style", "add");
		}
		return "shopping_advert_add.jsp";
	}

	// 广告后台页面显示
	@RequestMapping("/show")
	public ModelAndView show(int pagenum, String name, HttpSession session) {
		List<Shopping_advert> list = new ArrayList<>();
		if (name == null || name == "") {
			list = service.fenye(pagenum, session);
			session.setAttribute("FenYeFlg", "0");
		} else {
			session.setAttribute("FenYeFlg", "1");
			System.out.println("进入模糊查询");
			Shopping_advert sa = new Shopping_advert();
			sa.setAdTitle(name);
			list = service.search(pagenum, sa, session);
		}
		ModelAndView mv = new ModelAndView("shopping_advert_mv.jsp");
		mv.addObject("show", list);
		return mv;
	}

	// 返回翻页数据的单一
	@RequestMapping("/fenye1")
	public ModelAndView fanye(String pagenow, HttpSession session) {
		ModelAndView mv = new ModelAndView("goods_brand_fenyeMV.jsp");
		mv.addObject("pagenum", session.getAttribute("pagenum1"));
		System.out.println("controller的设置当前页="
				+ session.getAttribute("pagenow"));
		mv.addObject("currentPage", session.getAttribute("pagenow"));
		return mv;
	}

	// 翻页功能
	@RequestMapping("/fenye")
	public ModelAndView selectall(int pagenum, HttpSession session, String name) {
		System.out.println("进入分页controller方法名为fenye");
		List<Shopping_advert> list = new ArrayList<>();
		if ("0".equals(session.getAttribute("FenYeFlg"))) {
			System.out.println("pagenum======" + pagenum);
			list = service.fenye(pagenum, session);
		} else {
			Shopping_advert sa = new Shopping_advert();
			sa.setAdTitle(name);
			list = service.search(pagenum, sa, session);
		}
		ModelAndView mv = new ModelAndView("shopping_advert_mv.jsp");
		mv.addObject("show", list);
		return mv;
	}

	// 文件上传
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String updateAndfileupload(Integer id, String advername,
			HttpServletRequest req, String url, Integer seq, boolean yesnoshow1) {
		System.out.println("进入广告添加controller");
		System.out.println(yesnoshow1 + "#######");
		Shopping_advert sa = new Shopping_advert();
		System.out.println(advername + "+++++" + url + "+++++" + seq + "+++"
				+ yesnoshow1 + "++++" + id);

		MultipartHttpServletRequest mreq = (MultipartHttpServletRequest) req;
		MultipartFile file = mreq.getFile("file");
		String fname = file.getOriginalFilename();// 获取上传文件原名
		// System.out.println("原文件名" + fname);
		if (fname != null && fname != "") {
			System.out.println("###################################可判断");
			// sa.setBrandlogoImg(fname);
			sa.setAdImage(fname);
			ServletContext servletContext = req.getServletContext();// 得到上下文对象
			// 获取上传路径
			String realPath1 = servletContext.getRealPath("upload/advert");
			// System.out.println(realPath1);
			File filenew = new File(realPath1 + "/" + fname);
			// System.out.println(filenew);
			try {
				filenew.createNewFile();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				BufferedOutputStream bs = new BufferedOutputStream(
						new FileOutputStream(filenew));
				bs.write(file.getBytes());
				bs.flush();
				bs.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		sa.setAdTitle(advername);
		sa.setAdUrl(url);
		sa.setRecommend(yesnoshow1);
		sa.setSequence(seq);
		if (id == null) {
			service.insert(sa);

		} else {
			sa.setId(id);
			service.update(sa);
		}
		return "shopping_advert_list.jsp";
	}

	// 删除
	@RequestMapping("/delete")
	public String delete(Integer id) {
		System.out.println("controller进入广告删除");
		service.delete(id);
		return "shopping_advert_list.jsp";
	}
	//修改是否推荐
	@RequestMapping("/updateSep")
	public void updateSep(boolean flg,Integer id, String display,PrintWriter out){
		System.out.println("controller修改推荐");
		System.out.println("flg="+flg+"id="+id);
		Shopping_advert sa = new Shopping_advert();
		boolean rec= !flg;
		sa.setRecommend(rec);
		sa.setId(id);
		service.update(sa);		
	}
	// 表单验证
	@RequestMapping({ "/theform" })
	public void goods_brand_verify(HttpServletRequest request,
			HttpServletResponse response, String advername, String id) {
		System.out.println("表单验证controller");
		// System.out.println("name=" + name + "####" + "id=" + id);
		boolean ret = true;
		Shopping_advert sa = new Shopping_advert();
		Shopping_advert sa1 = new Shopping_advert();
		if (advername != null || advername != "") {
			sa.setAdTitle(advername);
			sa1 = service.selectforForm(sa);
		}
		if (id == null || id == "") {// 判断为添加功能
			if (sa1 != null) {
				System.out.println("判断添加功能时候品牌是否存在");
				ret = false;
			}
		} else {// 判断为编辑功能
			System.out.println("判断编辑功能时候品牌是否存在");
			String id1 = sa1.getId().toString();
			if (id.equals(id1)) {
				ret = true;
			} else {
				ret = false;
			}
		}
		try {
			PrintWriter writer = response.getWriter();
			writer.print(ret);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
