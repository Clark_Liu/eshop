package com.mall.controller;

import java.io.PrintWriter;
import java.util.List;
import java.util.jar.JarOutputStream;

import javax.servlet.http.HttpSession;

import org.apache.taglibs.standard.lang.jstl.test.beans.PublicInterface2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mall.entity.City;
import com.mall.service.CityService;
import com.mall.utils.IsNumberUtils;

import net.sf.json.JSONObject;

@Controller
@RequestMapping("/city")
public class CityController {
	@Autowired
	private CityService service;
	
	//下拉列表及城市信息
	@RequestMapping("/showSelectCity")
	public void showSelectCity(String pid,PrintWriter out,HttpSession session) {
		Long pidL = Long.parseLong(pid);
		List<City> list = service.showSelectCity(pidL,session);
		JSONObject js = new JSONObject();
		js.put("list", list);
		out.write(js.toString());
	}
	
	
	@RequestMapping("/updateCitySeqAndNameAndStatus")
	public void updateCitySeqAndNameAndStatus(Long id,String fieldName,String value,Integer level,PrintWriter out) {
		System.out.println(id);
		System.out.println(fieldName);
		System.out.println(value);
		switch(fieldName) {
		case "toseq":
			if("".equals(value)) {
				out.write("seqnull");
				return;
			}else {
				if(!IsNumberUtils.isNumber(value)) {
					out.write("geshi");
					return;
				}
			}
			break;
		case "toname":
			if("".equals(value)) {
				out.write("citynamenull");
				return;
			}else {
				Boolean b = service.selectByAreaname(value,level);
				if(b) {
					out.write("repeat");
					return;
				}
			}
			break;
		}
		
		int i = service.updateCitySeqAndNameAndStatus(id,fieldName,value);
		out.write(i);
	}
	
	
	@RequestMapping("/deleteCity")
	public void deleteCity(Long id,PrintWriter out) {
		int i = service.deleteCity(id);
		if(i>0) {
			out.write("1");
		}else {
			out.write("删除失败");
		}
	}
	
	@ResponseBody
	@RequestMapping("/isExistCityName")
	public void isExistCityName(String cityname,Integer level,PrintWriter out) {
		Boolean b = service.selectByAreaname(cityname,level);
		out.print(!b);
	}
	
	@RequestMapping("/addCity")
	public String addCity(String cityname,Integer level,Integer seq,Integer yesnouse,String parentId) {
		System.out.println(cityname);
		System.out.println(level);
		System.out.println(seq);
		System.out.println(yesnouse);
		System.out.println(parentId);
		if(seq == null) {
			seq = 0;
		}
		int i = service.addCity(cityname,level,seq,yesnouse,parentId);
		return "/city_addSeccess.html";
	}
	
	
	
}
