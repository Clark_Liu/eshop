package com.mall.entity;

import java.util.Date;
import java.util.List;

public class GoodsClass {
	
    @Override
	public String toString() {
		return "GoodsClass [id=" + id + ", addtime=" + addtime + ", deletestatus=" + deletestatus + ", classname="
				+ classname + ", display=" + display + ", level=" + level + ", recommend=" + recommend + ", sequence="
				+ sequence + ", goodstypeId=" + goodstypeId + ", typeName=" + typeName + ", parentId=" + parentId
				+ ", iconSys=" + iconSys + ", iconType=" + iconType + ", iconAccId=" + iconAccId + "]";
	}

	/**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column shopping_goodsclass.id
     *
     * @mbggenerated
     */
    private Long id;
    

	/**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column shopping_goodsclass.addTime
     *
     * @mbggenerated
     */
    private Date addtime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column shopping_goodsclass.deleteStatus
     *
     * @mbggenerated
     */
    private Boolean deletestatus;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column shopping_goodsclass.className
     *
     * @mbggenerated
     */
    private String classname;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column shopping_goodsclass.display
     *
     * @mbggenerated
     */
    private Boolean display;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column shopping_goodsclass.level
     *
     * @mbggenerated
     */
    private Integer level;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column shopping_goodsclass.recommend
     *
     * @mbggenerated
     */
    private Boolean recommend;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column shopping_goodsclass.sequence
     *
     * @mbggenerated
     */
    private Integer sequence;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column shopping_goodsclass.goodsType_id
     *
     * @mbggenerated
     */
    private Long goodstypeId;
    
    
    private String typeName;
    
    private String iconAccPath;

	public String getIconAccPath() {
		return iconAccPath;
	}

	public void setIconAccPath(String iconAccPath) {
		this.iconAccPath = iconAccPath;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	/**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column shopping_goodsclass.parent_id
     *
     * @mbggenerated
     */
    private Long parentId;
    
    

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column shopping_goodsclass.icon_sys
     *
     * @mbggenerated
     */
    private String iconSys;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column shopping_goodsclass.icon_type
     *
     * @mbggenerated
     */
    private Integer iconType;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column shopping_goodsclass.icon_acc_id
     *
     * @mbggenerated
     */
    private Long iconAccId;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column shopping_goodsclass.id
     *
     * @return the value of shopping_goodsclass.id
     *
     * @mbggenerated
     */
    public Long getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column shopping_goodsclass.id
     *
     * @param id the value for shopping_goodsclass.id
     *
     * @mbggenerated
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column shopping_goodsclass.addTime
     *
     * @return the value of shopping_goodsclass.addTime
     *
     * @mbggenerated
     */
    public Date getAddtime() {
        return addtime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column shopping_goodsclass.addTime
     *
     * @param addtime the value for shopping_goodsclass.addTime
     *
     * @mbggenerated
     */
    public void setAddtime(Date addtime) {
        this.addtime = addtime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column shopping_goodsclass.deleteStatus
     *
     * @return the value of shopping_goodsclass.deleteStatus
     *
     * @mbggenerated
     */
    public Boolean getDeletestatus() {
        return deletestatus;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column shopping_goodsclass.deleteStatus
     *
     * @param deletestatus the value for shopping_goodsclass.deleteStatus
     *
     * @mbggenerated
     */
    public void setDeletestatus(Boolean deletestatus) {
        this.deletestatus = deletestatus;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column shopping_goodsclass.className
     *
     * @return the value of shopping_goodsclass.className
     *
     * @mbggenerated
     */
    public String getClassname() {
        return classname;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column shopping_goodsclass.className
     *
     * @param classname the value for shopping_goodsclass.className
     *
     * @mbggenerated
     */
    public void setClassname(String classname) {
        this.classname = classname == null ? null : classname.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column shopping_goodsclass.display
     *
     * @return the value of shopping_goodsclass.display
     *
     * @mbggenerated
     */
    public Boolean getDisplay() {
        return display;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column shopping_goodsclass.display
     *
     * @param display the value for shopping_goodsclass.display
     *
     * @mbggenerated
     */
    public void setDisplay(Boolean display) {
        this.display = display;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column shopping_goodsclass.level
     *
     * @return the value of shopping_goodsclass.level
     *
     * @mbggenerated
     */
    public Integer getLevel() {
        return level;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column shopping_goodsclass.level
     *
     * @param level the value for shopping_goodsclass.level
     *
     * @mbggenerated
     */
    public void setLevel(Integer level) {
        this.level = level;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column shopping_goodsclass.recommend
     *
     * @return the value of shopping_goodsclass.recommend
     *
     * @mbggenerated
     */
    public Boolean getRecommend() {
        return recommend;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column shopping_goodsclass.recommend
     *
     * @param recommend the value for shopping_goodsclass.recommend
     *
     * @mbggenerated
     */
    public void setRecommend(Boolean recommend) {
        this.recommend = recommend;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column shopping_goodsclass.sequence
     *
     * @return the value of shopping_goodsclass.sequence
     *
     * @mbggenerated
     */
    public Integer getSequence() {
        return sequence;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column shopping_goodsclass.sequence
     *
     * @param sequence the value for shopping_goodsclass.sequence
     *
     * @mbggenerated
     */
    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column shopping_goodsclass.goodsType_id
     *
     * @return the value of shopping_goodsclass.goodsType_id
     *
     * @mbggenerated
     */
    public Long getGoodstypeId() {
        return goodstypeId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column shopping_goodsclass.goodsType_id
     *
     * @param goodstypeId the value for shopping_goodsclass.goodsType_id
     *
     * @mbggenerated
     */
    public void setGoodstypeId(Long goodstypeId) {
        this.goodstypeId = goodstypeId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column shopping_goodsclass.parent_id
     *
     * @return the value of shopping_goodsclass.parent_id
     *
     * @mbggenerated
     */
    public Long getParentId() {
        return parentId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column shopping_goodsclass.parent_id
     *
     * @param parentId the value for shopping_goodsclass.parent_id
     *
     * @mbggenerated
     */
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column shopping_goodsclass.icon_sys
     *
     * @return the value of shopping_goodsclass.icon_sys
     *
     * @mbggenerated
     */
    public String getIconSys() {
        return iconSys;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column shopping_goodsclass.icon_sys
     *
     * @param iconSys the value for shopping_goodsclass.icon_sys
     *
     * @mbggenerated
     */
    public void setIconSys(String iconSys) {
        this.iconSys = iconSys == null ? null : iconSys.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column shopping_goodsclass.icon_type
     *
     * @return the value of shopping_goodsclass.icon_type
     *
     * @mbggenerated
     */
    public Integer getIconType() {
        return iconType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column shopping_goodsclass.icon_type
     *
     * @param iconType the value for shopping_goodsclass.icon_type
     *
     * @mbggenerated
     */
    public void setIconType(Integer iconType) {
        this.iconType = iconType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column shopping_goodsclass.icon_acc_id
     *
     * @return the value of shopping_goodsclass.icon_acc_id
     *
     * @mbggenerated
     */
    public Long getIconAccId() {
        return iconAccId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column shopping_goodsclass.icon_acc_id
     *
     * @param iconAccId the value for shopping_goodsclass.icon_acc_id
     *
     * @mbggenerated
     */
    public void setIconAccId(Long iconAccId) {
        this.iconAccId = iconAccId;
    }
}