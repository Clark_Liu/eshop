package com.mall.entity;

import java.util.Date;

public class Goods_Fjs_GoodsTypeProperty {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column shopping_goodstypeproperty.id
     *
     * @mbggenerated
     */
    private Long id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column shopping_goodstypeproperty.addTime
     *
     * @mbggenerated
     */
    private Date addtime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column shopping_goodstypeproperty.deleteStatus
     *
     * @mbggenerated
     */
    private Boolean deletestatus;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column shopping_goodstypeproperty.display
     *
     * @mbggenerated
     */
    private Boolean display;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column shopping_goodstypeproperty.name
     *
     * @mbggenerated
     */
    private String name;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column shopping_goodstypeproperty.sequence
     *
     * @mbggenerated
     */
    private Integer sequence;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column shopping_goodstypeproperty.value
     *
     * @mbggenerated
     */
    private String value;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column shopping_goodstypeproperty.goodsType_id
     *
     * @mbggenerated
     */
    private Long goodstypeId;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column shopping_goodstypeproperty.id
     *
     * @return the value of shopping_goodstypeproperty.id
     *
     * @mbggenerated
     */
    public Long getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column shopping_goodstypeproperty.id
     *
     * @param id the value for shopping_goodstypeproperty.id
     *
     * @mbggenerated
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column shopping_goodstypeproperty.addTime
     *
     * @return the value of shopping_goodstypeproperty.addTime
     *
     * @mbggenerated
     */
    public Date getAddtime() {
        return addtime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column shopping_goodstypeproperty.addTime
     *
     * @param addtime the value for shopping_goodstypeproperty.addTime
     *
     * @mbggenerated
     */
    public void setAddtime(Date addtime) {
        this.addtime = addtime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column shopping_goodstypeproperty.deleteStatus
     *
     * @return the value of shopping_goodstypeproperty.deleteStatus
     *
     * @mbggenerated
     */
    public Boolean getDeletestatus() {
        return deletestatus;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column shopping_goodstypeproperty.deleteStatus
     *
     * @param deletestatus the value for shopping_goodstypeproperty.deleteStatus
     *
     * @mbggenerated
     */
    public void setDeletestatus(Boolean deletestatus) {
        this.deletestatus = deletestatus;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column shopping_goodstypeproperty.display
     *
     * @return the value of shopping_goodstypeproperty.display
     *
     * @mbggenerated
     */
    public Boolean getDisplay() {
        return display;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column shopping_goodstypeproperty.display
     *
     * @param display the value for shopping_goodstypeproperty.display
     *
     * @mbggenerated
     */
    public void setDisplay(Boolean display) {
        this.display = display;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column shopping_goodstypeproperty.name
     *
     * @return the value of shopping_goodstypeproperty.name
     *
     * @mbggenerated
     */
    public String getName() {
        return name;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column shopping_goodstypeproperty.name
     *
     * @param name the value for shopping_goodstypeproperty.name
     *
     * @mbggenerated
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column shopping_goodstypeproperty.sequence
     *
     * @return the value of shopping_goodstypeproperty.sequence
     *
     * @mbggenerated
     */
    public Integer getSequence() {
        return sequence;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column shopping_goodstypeproperty.sequence
     *
     * @param sequence the value for shopping_goodstypeproperty.sequence
     *
     * @mbggenerated
     */
    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column shopping_goodstypeproperty.value
     *
     * @return the value of shopping_goodstypeproperty.value
     *
     * @mbggenerated
     */
    public String getValue() {
        return value;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column shopping_goodstypeproperty.value
     *
     * @param value the value for shopping_goodstypeproperty.value
     *
     * @mbggenerated
     */
    public void setValue(String value) {
        this.value = value == null ? null : value.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column shopping_goodstypeproperty.goodsType_id
     *
     * @return the value of shopping_goodstypeproperty.goodsType_id
     *
     * @mbggenerated
     */
    public Long getGoodstypeId() {
        return goodstypeId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column shopping_goodstypeproperty.goodsType_id
     *
     * @param goodstypeId the value for shopping_goodstypeproperty.goodsType_id
     *
     * @mbggenerated
     */
    public void setGoodstypeId(Long goodstypeId) {
        this.goodstypeId = goodstypeId;
    }
}