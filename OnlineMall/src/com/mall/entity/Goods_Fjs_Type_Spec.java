package com.mall.entity;

public class Goods_Fjs_Type_Spec {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column shopping_goodstype_spec.type_id
     *
     * @mbggenerated
     */
    private Long typeId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column shopping_goodstype_spec.spec_id
     *
     * @mbggenerated
     */
    private Long specId;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column shopping_goodstype_spec.type_id
     *
     * @return the value of shopping_goodstype_spec.type_id
     *
     * @mbggenerated
     */
    public Long getTypeId() {
        return typeId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column shopping_goodstype_spec.type_id
     *
     * @param typeId the value for shopping_goodstype_spec.type_id
     *
     * @mbggenerated
     */
    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column shopping_goodstype_spec.spec_id
     *
     * @return the value of shopping_goodstype_spec.spec_id
     *
     * @mbggenerated
     */
    public Long getSpecId() {
        return specId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column shopping_goodstype_spec.spec_id
     *
     * @param specId the value for shopping_goodstype_spec.spec_id
     *
     * @mbggenerated
     */
    public void setSpecId(Long specId) {
        this.specId = specId;
    }
}