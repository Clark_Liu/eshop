package com.mall.entity;

public class Goods_Fjs_Type_Brand {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column shopping_goodstype_brand.type_id
     *
     * @mbggenerated
     */
    private Long typeId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column shopping_goodstype_brand.brand_id
     *
     * @mbggenerated
     */
    private Long brandId;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column shopping_goodstype_brand.type_id
     *
     * @return the value of shopping_goodstype_brand.type_id
     *
     * @mbggenerated
     */
    public Long getTypeId() {
        return typeId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column shopping_goodstype_brand.type_id
     *
     * @param typeId the value for shopping_goodstype_brand.type_id
     *
     * @mbggenerated
     */
    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column shopping_goodstype_brand.brand_id
     *
     * @return the value of shopping_goodstype_brand.brand_id
     *
     * @mbggenerated
     */
    public Long getBrandId() {
        return brandId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column shopping_goodstype_brand.brand_id
     *
     * @param brandId the value for shopping_goodstype_brand.brand_id
     *
     * @mbggenerated
     */
    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }
}